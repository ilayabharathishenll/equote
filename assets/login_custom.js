$( function() {
	$("#dob").datepicker({ todayHighlight: true,dateFormat: 'dd/mm/yy',autoclose: true });
});
$("#frm_register").validate({
	rules: {
		first_name:{required: {
                        depends:function(){
                            $(this).val($.trim($(this).val()));
                            return true;
                        }
                    }},
		last_name:{required: {
                        depends:function(){
                            $(this).val($.trim($(this).val()));
                            return true;
                        }
                    }},
		password:{
			required: {
                        depends:function(){
                            $(this).val($.trim($(this).val()));
                            return true;
                        }
                    },
		},
		rpassword:{required: {
                        depends:function(){
                            $(this).val($.trim($(this).val()));
                            return true;
                        }
                    },equalTo: "#register_password"},
		email:{required: {
                        depends:function(){
                            $(this).val($.trim($(this).val()));
                            return true;
                        }
                    },email:true,remote: 
			{
				url: base_url+"supplier/login/register_email_exists",
				type: "post",
		    } 
		},
		gender: {
			required: {
                        depends:function(){
                            $(this).val($.trim($(this).val()));
                            return true;
                        }
                    },
                 },
		dob: {
			required: {
                        depends:function(){
                            $(this).val($.trim($(this).val()));
                            return true;
                        }
                    },
            },
		address: {
			required: {
                        depends:function(){
                            $(this).val($.trim($(this).val()));
                            return true;
                        }
                    },
               },
		permanent_address: {
			required: {
                        depends:function(){
                            $(this).val($.trim($(this).val()));
                            return true;
                        }
                    },
                 },
		state: {
			required: {
                        depends:function(){
                            $(this).val($.trim($(this).val()));
                            return true;
                        }
                    },
                  },
		// post_code:{required:true,number:true}, 
		phone:{required: {
                        depends:function(){
                            $(this).val($.trim($(this).val()));
                            return true;
                        }
                    },number:true},
        alt_phone:{number:true},
        landline_no:{number:true},
		country: {
			required: {
                        depends:function(){
                            $(this).val($.trim($(this).val()));
                            return true;
                        }
                    },
               },
		company_name:{required:true},
		company_regno: {
			required: {
                        depends:function(){
                            $(this).val($.trim($(this).val()));
                            return true;
                        }
                    },
             },
		"catgory[]":{
			required: true
                 },
	},
	messages: {
		first_name:
		{
			required:"Please enter first name",
			lettersonly:"Please enter alphabet characters only"
		},
		last_name:
		{
			required:"Please enter last name",
			lettersonly:"Please enter alphabet characters only"
		},
	    position:"Please enter position",
		password:{required:"Please enter password"},
		rpassword:
		{
			required:"Please enter confirm password",
			equalTo: "Your password and confirmation password do not match"
		},
		email:{required:"Please enter email",email:"Please enter vaild email",remote:'Email already used.'},
		gender:"Please select gender",
		dob:"Please select date of birth",
		address:"Please enter temporary address",
		permanent_address:"Please enter permanent address",
		state:"Please enter state",
		phone:{required:"Please enter phone number",number:"Please enter valid phone number"},
		country:"Please select country",
		company_name:
		{
			required:"Please enter company name",
			lettersonly:"Please enter alphabet characters only"
		},
		company_regno:"Please enter company regno",
		"catgory[]":"Please select atleast one category",
		
	},
	errorPlacement: function(error, element) {
        if(element.parent('.input-icon').length) {
            error.insertAfter(element.parent());
        } else {
            error.insertAfter(element);
        }
    }
});

$("#frm_reset").validate({
	rules: {
		password:{
			required: {
                        depends:function(){
                            $(this).val($.trim($(this).val()));
                            return true;
                        }
                    },
		},
		conpassword:{required: {
                        depends:function(){
                            $(this).val($.trim($(this).val()));
                            return true;
                        }
                    },equalTo: "#password"},
	},
	messages: {
		password:{required:"Please enter password"},
		conpassword:
		{
			required:"Please enter confirm password",
			equalTo: "Your password and confirmation password do not match"
		},
	},
	errorPlacement: function(error, element) {
        if(element.parent('.input-icon').length) {
            error.insertAfter(element.parent());
        } else {
            error.insertAfter(element);
        }
    }
});

$(document).ready(function() {
	$('#supplier-quote').DataTable({
		"bPaginate": true,
		"bLengthChange": false,
		"bFilter": false,
		"bInfo": false,
		"iDisplayLength":5 ,
		"ordering": false
	});
	$('#applied-tenders').DataTable({
		"bPaginate": true,
		"bLengthChange": false,
		"bFilter": false,
		"bInfo": false,
		"iDisplayLength":5 ,
		"ordering": false
	});
	$('#award-tenders').DataTable({
		"bPaginate": true,
		"bLengthChange": false,
		"bFilter": false,
		"bInfo": false,
		"iDisplayLength":5 ,
		"ordering": false
	});
	$('#admin-list').DataTable({
		"bPaginate": true,
		"bLengthChange": false,
		"bFilter": false,
		"bInfo": false,
		"iDisplayLength":5 ,
		"ordering": false
	});
	$('#applied-tender').DataTable({
		"bPaginate": true,
		"bLengthChange": false,
		"bFilter": false,
		"bInfo": false,
		"iDisplayLength":5 ,
		"ordering": false
	});
	$('#award-list').DataTable({
		"bPaginate": true,
		"bLengthChange": false,
		"bFilter": false,
		"bInfo": false,
		"iDisplayLength":5 ,
		"ordering": false
	});
	$('#invitetbl').DataTable({
		"bPaginate": true,
		"bLengthChange": false,
		"bFilter": false,
		"bInfo": false,
		"iDisplayLength":5 ,
		"ordering": false
	});
	$('#send-invite').DataTable({
		"bPaginate": true,
		"bLengthChange": false,
		"bFilter": false,
		"bInfo": false,
		"iDisplayLength":5 ,
		"ordering": false
	});
	$('#tender-invite').DataTable({
		"bPaginate": true,
		"bLengthChange": false,
		"bFilter": false,
		"bInfo": false,
		"iDisplayLength":5 ,
		"ordering": false
	});
	$('#tendertbl').DataTable({
		"bPaginate": true,
		"bLengthChange": false,
		"bFilter": false,
		"bInfo": false,
		"iDisplayLength":5 ,
		"ordering": false
	});
	$('#vendor-list').DataTable({
		"bPaginate": true,
		"bLengthChange": false,
		"bFilter": false,
		"bInfo": false,
		"iDisplayLength":5 ,
		"ordering": false
	});

	var i = 0;
	$('#addmore').on('click', function(){
		var newfield ='<div class="col-md-12 col-sm-12 col-xs-12 margintop10 paddingleftright"><div class="form-group"><label class="control-label col-md-5" style="padding-right: 0px;"></label><div class="col-md-5"><input type="file" name="upload_terms['+i+']" id="upload_terms_'+i+'"></div><div class="col-md-2"><button type="button" class="btn btn-danger customaddmorebtn" name="remove" id="remove"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button></div></div></div>';
		$('#terms').append(newfield);
		i++;
	});
	$(document).on('click','#remove', function(){
		$(this).parent().parent().parent('div').remove();
	});
	i++;

	chelp=0;
	$('#helpmore').on('click', function(){
		var newfield ='<div class="col-md-12 col-sm-12 col-xs-12 margintop10 paddingleftright"><div class="form-group"><label class="control-label col-md-5"></label><div class="col-md-5"><input type="file" name="upload_helpdoc['+chelp+']" id="upload_helpdoc_'+chelp+'"></div><div class="col-md-2"><button type="button" class="btn btn-danger customaddmorebtn" name="remove" id="removehelp"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button></div></div></div>';
		$('#helpdoc').append(newfield);
		chelp++; 
	});
	$(document).on('click','#removehelp', function(){
		$(this).parent().parent().parent('div').remove();
	});
	chelp++;

	cdetail=0;
	$('#detailmore').on('click', function(){
		var newfield ='<div class="col-md-12 col-sm-12 col-xs-12 margintop10 paddingleftright"><div class="form-group"><label class="control-label col-md-5"></label><div class="col-md-5"><input type="file" name="upload_tenderdetail['+cdetail+']" id="upload_tenderdetail_'+cdetail+'"></div><div class="col-md-2"><button type="button" class="btn btn-danger customaddmorebtn" name="remove" id="removedetail"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button></div></div></div>';
		$('#detaildoc').append(newfield);
		cdetail++; 
	});
	$(document).on('click','#removedetail', function(){
		$(this).parent().parent().parent('div').remove();
	});
	cdetail++;

	ctender=0;
	$('#tenderdocmore').on('click', function(){
		var newfield ='<div class="col-md-12 col-sm-12 col-xs-12 margintop10 paddingleftright"><div class="form-group"><label class="control-label col-md-5" style="padding-right: 0px;"></label><div class="col-md-5"><input type="file" name="upload_tenderdoc['+ctender+']" id="upload_tenderdoc_'+ctender+'"></div><div class="col-md-2"><button type="button" class="btn btn-danger customaddmorebtn" name="remove" id="removetender"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button></div></div></div>';
		$('#tenderdoc').append(newfield);
		ctender++;
	});
	$(document).on('click','#removetender', function(){
		$(this).parent().parent().parent('div').remove();
	});
	ctender++;

	ctenderone=0;
	$('#tenderdoconemore').on('click', function(){
		var newfield ='<div class="col-md-12 col-sm-12 col-xs-12 margintop10 paddingleftright"><div class="form-group"><label class="control-label col-md-5"></label><div class="col-md-5"><input type="file" name="tender_docone['+ctenderone+']" id="tender_docone_'+ctenderone+'"></div><div class="col-md-2"><button type="button" class="btn btn-danger customaddmorebtn" name="remove" id="removetenderone"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button></div></div></div>';
		$('#tenderdocone').append(newfield);
		ctenderone++;
	});
	$(document).on('click','#removetenderone', function(){
		$(this).parent().parent().parent('div').remove();
	});
	ctenderone++;

	ctenderonetwo=0;
	$('#tenderdoctwomore').on('click', function(){
		var newfield ='<div class="col-md-12 col-sm-12 col-xs-12 margintop10 paddingleftright"><div class="form-group"><label class="control-label col-md-5"></label><div class="col-md-5"><input type="file" name="tender_doctwo['+ctenderonetwo+']" id="tender_doctwo_'+ctenderonetwo+'"></div><div class="col-md-2"><button type="button" class="btn btn-danger customaddmorebtn" name="remove" id="removetendertwo"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button></div></div></div>';
		$('#tenderdoctwo').append(newfield);
		ctenderonetwo++;
	});
	$(document).on('click','#removetendertwo', function(){
		$(this).parent().parent().parent('div').remove();
	});
	ctenderonetwo++;
});
$("#search_result_length").hide();

$( function() {
	$("#award_date").datepicker({ todayHighlight: true,dateFormat: 'dd/mm/yy',autoclose: true });
});
$( function() {
	$("#award_enddate").datepicker({ todayHighlight: true,dateFormat: 'dd/mm/yy',autoclose: true });
});
$( function() {
	
	$(".delivery_date").datepicker({ todayHighlight: true,dateFormat: 'dd/mm/yy',autoclose: true });
});
$( function() {
	$("#start_date").datepicker({ todayHighlight: true,dateFormat: 'dd/mm/yy H:i:S',autoclose: true });
});
$( function() {
	$("#end_date").datepicker({ todayHighlight: true,dateFormat: 'dd/mm/yy H:i:S',autoclose: true });
});
$( function() {
	$("#dob").datepicker({ todayHighlight: true,dateFormat: 'dd/mm/yy',autoclose: true });
});

$(document).on("click",".sendinvitepopup",function() {
	$("#sendPopup").modal("show");
});

$(document).on("click",".documentDownload",function() {
	$("#docPopup").modal("show");
});
<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admistrator
 *
 * @author Shenll
 */
class Dashboard extends Admin_Controller {

	public function index()
	{
		$data = array();
		$this->load->model("Dashboard_Model");
		$data['dashboarCount'] = $this->Dashboard_Model->DashboardCounts();
		$tender=$this->Tender_Model->tender_list();
		if(!empty($tender)){
			$this->tender_cron($tender);
		}
		$data['subview'] = $this->load->view('admin/main_content', $data, TRUE);
		$this->load->view('admin/_layout_main', $data);
	}

	public function tender_cron($data){
		$datas=array();
		//print_r($data);
		foreach ($data as $value) {
			if($value->status!='Trash' && $value->status!='Awarded' && $value->status!='Cancelled') {
				$startDate = $value->start_date . " " . date("H:i",strtotime($value->start_time));
				$endDate = $value->end_date . " " . date("H:i",strtotime($value->end_time));
				if(strtotime(date('Y-m-d H:i'))>=strtotime($startDate) && strtotime(date('Y-m-d H:i'))<=strtotime($endDate)) {
					$datas['status']="Open";
				} elseif(strtotime(date('Y-m-d H:i'))>strtotime($endDate) && strtotime(date('Y-m-d H:i'))>strtotime($startDate)) {
					$datas['status']="Closed";
				} elseif(strtotime(date('Y-m-d H:i'))<strtotime($startDate) && strtotime(date('Y-m-d H:i'))!=strtotime($startDate)){ 
					$datas['status']="Pending";
				} elseif(empty(strtotime($value->start_date)) && empty(strtotime($value->end_date))) {
					$datas['status']="Pending";
				}
				$this->Tender_Model->update($value->tender_id,$datas);
			}
		}
	}
}

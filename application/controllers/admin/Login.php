<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Login extends Admin_Controller {

	public function index()
	{  
		$this->load->database();
		$this->load->helper(array('form','url'));
		$this->load->library('session');
		$this->load->model('Admin_Model');
		$login_details=  $this->Admin_Model->AdminLogin();
		if (($login_details!=NULL) && !empty($login_details)) {
			$adminid=$this->session->userdata('admin_id');
			if(!empty($adminid)) {
				$this->session->set_flashdata('success', 'You have successfully logged in');
				redirect('admin/dashboard');
			} else {
				$this->session->set_flashdata('message_failure', '');
				$this->load->view('admin/login');
			}
		} else {
			$submit = $this->input->post("submit");
			if (!empty($submit)){
				$this->session->set_flashdata('message_failure', 'Invalid email and password');
				$submit="";
			} else {
				$this->session->set_flashdata('message_failure', '');
			}
				$this->load->view('admin/login');
		}
	}
}
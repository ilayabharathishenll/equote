<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admistrator
 *
 * @author Shenll
 */
class Add extends Admin_Controller {

	public function add_admin()
	{
		if($this->input->post('submit')){
			$key='12345';
			$password=$this->input->post('password');
			
			$data['password']=$this->encryption->encrypt($password,$key);
			$data['passwords']=$password;
			$data['name']=$this->input->post('name');
			$data['email']=$this->input->post('email');
			$data['phone']=$this->input->post('Phone');
			$data['alt_phone']=$this->input->post('alt_phone');
			$data['status']="Active";
			$data['created_on']=date('Y-m-d H:i:s');
			$data['updated_on']=date('Y-m-d H:i:s');

			$message = $this->encryption->decrypt(
				$data['password'],
				array(
					'cipher' => 'blowfish',
					'mode' => 'cbc',
					'key' => $key,
					'hmac_digest' => 'sha256',
					'hmac_key' => $hmac_key
				)
			);
		}
		else{
			$this->load->view('admin/add_admin');
		}
	}
} 
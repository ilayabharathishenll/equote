<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admistrator
 *
 * @author Shenll
 */
class Tender extends Admin_Controller {

	public function add_quote()
	{
		$user_id=$this->session->userdata('admin_id');
		$data = array();$rfq='';
		$category=$this->Category_Model->cat_list();
		if($this->input->post('submit')){
			$data['part_no']=$this->input->post("part_no");
			$data['part_name']=$this->input->post("part_name");
			$data['part_price']=$this->input->post("part_price");
			$data['quantity']=$this->input->post("quantity");
			$part_price = $this->input->post("part_price");
			$quantity = $this->input->post("quantity");
			$data['base_price'] = ($part_price * $quantity);
			$data['price_edit']=$this->input->post("period")==1?"Yes":"No";
			$data['drawing_no']=$this->input->post("drawing_no");
			$data['commodity_id']=$this->input->post("category");
			$data['created_by']=$user_id;
			$data['modified_by']=$user_id;
			$data['created_on']=date('Y-m-d H:i:s');
			$data['modified_on']=date('Y-m-d H:i:s');
			//print_r($data);exit;
			$inserted_id=$this->Tender_Model->add($data);
			if($inserted_id){
				// $this->recent_activity($inserted_id,"Add");
				foreach($category as $categorys) {
					if($categorys->category_id==$data['commodity_id']){
						$rfq=$categorys->category_name;
					}
				}
				$data["ref_no"]=$rfq.'/'.date('Y').'/'.sprintf("%03d",($inserted_id));
				$this->Tender_Model->update($inserted_id,$data);
			}
			$url_id=$this->encrypt->encode($inserted_id);
			$url_id=$inserted_id;
			$url="?id=".$url_id;
			redirect('admin/tender_add'.$url);
		}
		else{
			$data['subview'] = $this->load->view('admin/add_quote',array('data'=>$category), TRUE);
			$this->load->view('admin/_layout_main', $data);
		}
	}
	public function recent_activity($id,$activity){
		$user_id = $this->session->userdata('admin_id');
		$admin_name = $this->session->userdata('admin_name');
		$data=array("tender_id"=>$id,"admin_id"=>$user_id,"created_by"=>$admin_name,"status"=>$activity,"created_on"=>date('Y-m-d H:i:s'));
		$this->Dashboard_Model->add_activity($data);
	}

	public function add_rfq()
	{
		date_default_timezone_set('Asia/Kolkata');
		$user_id=$this->session->userdata('admin_id');
		$data = array();
		$tender_id='';
		if($this->input->get('id')!=''){
			$id=$this->input->get("id");
			$result['tender']=$this->Tender_Model->list_tender($id);
			if($this->input->post('submit')!=''){
				$data["delivery_duration"]=$this->input->post("delivery_duration");
				$data["delivery_date"]=DateTime::createFromFormat('d/m/Y', $this->input->post("delivery_date"))->format('Y-m-d');
				$data["quantity"]=$this->input->post("sel_quantity");
				$data["base_price"]=$this->input->post("base_price");
				$data["tender_title"]=$this->input->post("tender_title");
				$data["norm"]=$this->input->post("norm");
				$data["rm_price"]=$this->input->post("rm_price");
				$data["tender_detail"]=$this->input->post("tender_detail");
				$data["start_date"]=DateTime::createFromFormat('d/m/Y', $this->input->post("start_date"))->format('Y-m-d');
				$data["end_date"]=DateTime::createFromFormat('d/m/Y', $this->input->post("end_date"))->format('Y-m-d');
				$data["start_time"]=$this->input->post("start_time");
				$data["end_time"]=$this->input->post("end_time");
				$data['notification_setting']=$this->input->post("notification_setting");
				if ($data['notification_setting'] == 2)
					$data['suppliers']=implode("##",$this->input->post("suppliers"));
				$data['modified_by']=$user_id;
				$data['modified_on']=date('Y-m-d H:i:s');
				if($_FILES){
					$this->do_upload($_FILES,$id);
				}
				if (!empty(strtotime( $data["start_date"])) && !empty(strtotime($data["end_date"]))){
					$startDate = $data["start_date"] . " " . $data["start_time"];
					$endDate = $data["end_date"] . " " . $data["end_time"];
					if(strtotime(date('Y-m-d H:i')) >= strtotime($startDate) && strtotime(date('Y-m-d H:i'))<=strtotime($endDate)){ 
						$data['status']="Open";
					} 
					elseif(strtotime(date('Y-m-d H:i')) > strtotime($endDate) && strtotime(date('Y-m-d H:i')) > strtotime($startDate)) {
						$data['status']="Closed";
					} elseif(strtotime(date('Y-m-d H:i'))<strtotime($startDate) && strtotime(date('Y-m-d H:i'))!=strtotime($startDate)){ 
						$data['status']="Pending";
					}elseif(empty(strtotime($startDate)) && empty(strtotime($endDate))){
						$data['status']="Pending";
					}
				}
				$output=$this->Tender_Model->update($id,$data);

				if(!empty($output)){
					$this->recent_activity($id,"Add");
					$this->session->set_flashdata('success', 'Tender added successfully');
					redirect('admin/tender/tender_list');
				}
			}
			else {
				$tender_category = $result['tender'][0]->commodity_id;
				$result['category']=$this->Category_Model->cat_list();
				$result['suppliers']=$this->Tender_Model->supplier_list($tender_category);
				$data['subview'] = $this->load->view('admin/add_rfq',array('data'=>$result), TRUE);
				$this->load->view('admin/_layout_main', $data);
			}
		}
		else{
			redirect('admin/dashboard');
		}
	}

	public function edit_rfq()
	{	
		date_default_timezone_set('Asia/Kolkata');
		$user_id=$this->session->userdata('admin_id');
		$data = array();
		if($this->input->get('id')){
			$data=$this->Tender_Model->view_list($this->input->get('id'));
		}
		if($this->input->post('submit')!=''){
			$id=$this->input->get('id');
			$datas["norm"]=$this->input->post("norm");
			$datas["rm_price"]=$this->input->post("rm_price");
			$datas["tender_detail"]=$this->input->post("tender_detail");
			$datas['modified_by']=$user_id;
			$datas['modified_on']=date('Y-m-d H:i:s');
            $upload['terms']    = (empty($_POST["upload_terms"]))?"":$_POST["upload_terms"];
            $upload['helpdoc']  = (empty($_POST["upload_helpdoc"]))?"":$_POST["upload_helpdoc"];
            $upload['tenderdetail'] = (empty($_POST["upload_tenderdetail"]))?"":$_POST["upload_tenderdetail"];
            $upload['tenderdoc'] = (empty($_POST["upload_tenderdoc"]))?"":$_POST["upload_tenderdoc"];
            $upload['docone'] = (empty($_POST["tender_docone"]))?"":$_POST["tender_docone"];
            $upload['doctwo'] = (empty($_POST["tender_doctwo"]))?"":$_POST["tender_doctwo"];
            $upload['id'] = $id;
            $this->Tender_Model->update_docs($upload);
			if($_FILES) {
				$this->do_upload($_FILES,$id);
			} 
			$output=$this->Tender_Model->update($id,$datas);
			$this->recent_activity($id,"Update");
			if(!empty($output)){
				$this->session->set_flashdata('success', 'Tender updated successfully');
				redirect('admin/tender/tender_list');
			}
		}
		$data['category']=$this->Category_Model->cat_list();
		$data['subview'] = $this->load->view('admin/edit_rfq', array('data'=>$data), TRUE);
		$this->load->view('admin/_layout_main', $data);
	}


	public function view_tender()
	{
		$data = array();
		if($this->input->get('id')){
			$data=$this->Tender_Model->view_list($this->input->get('id'));
		}
		$data['subview'] = $this->load->view('admin/view_tender', array('data'=>$data), TRUE);
		$this->load->view('admin/_layout_main', $data);
	}

	public function tender_list()
	{
		$data=array();
		if($this->input->post('search')!=''){
			$ref_no=$this->input->post("rfq_no");
			$tender_title=$this->input->post("tender_name"); 
			$partno=$this->input->post("partno");
			$status=$this->input->post("status");
			$data =$this->Tender_Model->tender_list();
			$url="?no=".$ref_no."&title=".$tender_title."&partno=".$partno."&status=".$status."&search=1";
			redirect('admin/tender/tender_list'.$url);
		}
		if($this->input->get('search')==1){
			$datas["ref_no"]=$this->input->get("no");
			$datas["tender_title"]=$this->input->get("title");
			$datas["part_no"]=$this->input->get("partno");
			$datas["status"]=$this->input->get("status");
			$data['tender'] =$this->Tender_Model->tender_list($datas);
		}else{
			$data['tender'] =$this->Tender_Model->order_tender_list();
		}
		$data['category']=$this->Category_Model->cat_list();

		$data['subview'] = $this->load->view('admin/tender_list', array('data'=>$data), TRUE);
		$this->load->view('admin/_layout_main', $data);
	}
	public function applied_list()
	{
		$data = array();
		if (!empty($_POST['appliedvendor'])) {
			$data['search'] = $_POST['appliedvendor'];
		}
		$this->load->model("Applytender_Model");
		$tenderID =$this->uri->segment(4);
		$application_id =$this->uri->segment(5);
		if (!empty($tenderID) && !empty($application_id)) {
			$data = $this->Applytender_Model->awardsAdded($tenderID,$application_id);
			if (!empty($data)) {
				$this->session->set_flashdata('message', 'Award added successfully.');
				redirect("admin/awards/award_list");
			} else {
				redirect("admin/tender/applied_list/".$tenderID);
			}
		} else {
			$data['getvendorList'] = $this->Applytender_Model->appliedvendor_list($tenderID);
			$data['subview'] = $this->load->view('admin/applied_list', $data, TRUE);
			$this->load->view('admin/_layout_main', $data);
		}
	}

	public function applied_tender()
	{
		$data = array();
		if (!empty($_POST['applied'])) {
			$data['search'] = $_POST['applied'];
		}
		$this->load->model("Applytender_Model");
		$data['getTenderList'] = $this->Applytender_Model->appliedtender_list();
		$data['subview'] = $this->load->view('admin/applied_tender', $data, TRUE);
		$this->load->view('admin/_layout_main', $data);
	}

	public function do_upload($files,$id){
		$key=array_keys($files);
		$config['upload_path']  = './uploads/';
		$config['allowed_types']= '*';
		$this->upload->initialize($config);
		foreach ($key as $value) {
			// if((is_array($_FILES[$value]['size'])) && ($_FILES[$value]['size'][0]>0)){
			if((is_array($_FILES[$value]['size']))){
				for($i=0; $i< count($_FILES[$value]['name']); $i++) {           
					$_FILES['new']['name']= $files[$value]['name'][$i];
					$_FILES['new']['type']= $files[$value]['type'][$i];
					$_FILES['new']['tmp_name']= $files[$value]['tmp_name'][$i];
					$_FILES['new']['error']= $files[$value]['error'][$i];
					$_FILES['new']['size']= $files[$value]['size'][$i];	
					if ( ! $this->upload->do_upload('new')) {
						$error = array('error' => $this->upload->display_errors());
						$data['subview'] = $this->load->view('admin/add_rfq', $error, TRUE);
						$this->load->view('admin/_layout_main', $data);
					} else {
						$datas = array('upload_data' => $this->upload->data());
						$details["tender_id"]=$id;
						$details["t_doc_type"]=$value;
						$details["t_doc_name"]=$datas['upload_data']['file_name'];
						$details["t_doc_file_name"]=$datas['upload_data']['file_name']."-".date('Y-m-d');
						$details["status"]="Active";
						$details['created_on']=date('Y-m-d H:i:s');
						$details['modified_date']=date('Y-m-d H:i:s');
						$details['created_by']=$id;
						$this->Tender_Model->add_details($details);
					}
				}
			} elseif($_FILES[$value]['size']>0 && $value=='upload_drawing') {
				if ( ! $this->upload->do_upload($value))  {
					$error = array('error' => $this->upload->display_errors());
					$data['subview'] = $this->load->view('admin/add_rfq', $error, TRUE);
					$this->load->view('admin/_layout_main', $data);
				} else {
					$datas = array('upload_data' => $this->upload->data());
					$data['drawing_doc']= $datas['upload_data']['file_name'];
					$this->Tender_Model->update($id,$data);
				}
			}
		}
	}

	public function download($name=null){
		force_download('./uploads/'.$name, NULL);
	}

	public function export_excel(){
		$this->load->library("excel");
		if($this->input->post('rfq_no')!=''){
			$object = new PHPExcel();
			$object->setActiveSheetIndex(0);
			$table_columns = array("S.No", "Ref No","Tender Title","Part No","Part Name","Part Price", "Drawing No","Delivery Duration","Delivery Date","Quantity", "Base Price","Norm", "Start Date", "End Date", "Price", "RM Price");
			$column = 0;
			$object->getActiveSheet()->setTitle('RFQ worksheet');  
			$datas['ref_no']=$this->input->post('rfq_no');
			if(!empty($datas)){
				$data = $this->Tender_Model->tender_list($datas);
				foreach($table_columns as $field) {
					$object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
					$column++;
				}

				$excel_row = 2;
				$i=1;
				foreach($data as $row) {
					$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $i);
					$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->ref_no);
					$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->tender_title);
					$object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->part_no);
					$object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->part_name);
					$object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row->part_price);
					$object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row->drawing_no);
					$object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, date('d-m-Y',strtotime($row->delivery_duration)));
					$object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, date('d-m-Y',strtotime($row->delivery_date)));
					$object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row, $row->quantity);
					$object->getActiveSheet()->setCellValueByColumnAndRow(10, $excel_row,$row->base_price);
					$object->getActiveSheet()->setCellValueByColumnAndRow(11, $excel_row,$row->norm);
					$object->getActiveSheet()->setCellValueByColumnAndRow(12, $excel_row, date('d-m-Y',strtotime($row->start_date)));
					$object->getActiveSheet()->setCellValueByColumnAndRow(13, $excel_row,date('d-m-Y',strtotime($row->end_date)));
					$object->getActiveSheet()->setCellValueByColumnAndRow(14, $excel_row,!empty($row->price)?$row->price:"0");
					$object->getActiveSheet()->setCellValueByColumnAndRow(15, $excel_row, $row->rm_price);
					$i++;
					$excel_row++;
				}
				$object->getActiveSheet()
					->getStyle('A1:P1')
					->applyFromArray(
						array(
							'fill' => array(
								'type' => PHPExcel_Style_Fill::FILL_SOLID,
								'color' => array('rgb' => 'D9D9D9')
							)
						)
					);
				$object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
				header('Content-Type: application/vnd.ms-excel');
				header('Content-Disposition: attachment;filename="Tender.xls"');
				$object_writer->save('php://output');
			}
		} else {
			redirect('admin/tender/tender_list');
		}
	}
	public function delete(){
		$id=$this->input->get('id');
		$datas['status']='Trash';
		$datas['modified_on']=date('Y-m-d H:i:s');
		$output=$this->Tender_Model->update($id,$datas);
		if(!empty($output)){
			$this->recent_activity($id,"Delete");
			$this->session->set_flashdata('success', 'Tender deleted successfully');
			redirect('admin/tender/tender_list');
		}
	}
	public function delete_file($path){
		echo "path=>".$path;
	}

	public function automaticTenderUpdate() {
		$post_type = $_POST["post-type"];
		if (strtolower($post_type) == "automaticupdate") {

			$open=$this->Tender_Model->tenderAutoUpdate("Pending");
			$close=$this->Tender_Model->tenderAutoUpdate("Open");
			if(!empty($open) || !empty($close)){
               echo "success";
			}
		}
	}
	/*public function get_delivery(){
		$delivery_date = $_POST["delivery_date"];
		if (!empty($delivery_date)) {
			$currenetDate=date("Y-m-d");
			list($day,$month,$year)=explode("/",$_POST["delivery_date"]);
            $deliveryDate=$year."-".$month."-".$day;
			$stmt = $this->db->query("select DATEDIFF('".$deliveryDate."', '".$currenetDate."') as count"); 
			foreach ($stmt->result() as $row) {
				$days=$row->count;
			}
			echo "success##^^##".$days;
		}
	}*/

	public function get_delivery(){
		$delivery_date = $_POST["delivery_date"];
		$end_date = $_POST["end_date"];
		if (!empty($delivery_date) && !empty($end_date)) {
			//$currenetDate=date("Y-m-d");
			list($eday,$emonth,$eyear)=explode("/",$_POST["end_date"]);
			list($day,$month,$year)=explode("/",$_POST["delivery_date"]);
			$currenetDate=$eyear."-".$emonth."-".$eday;
            $deliveryDate=$year."-".$month."-".$day;
			$stmt = $this->db->query("select DATEDIFF('".$deliveryDate."', '".$currenetDate."') as count"); 
			foreach ($stmt->result() as $row) {
				$days=$row->count;
			}
			echo "success##^^##".$days;
		}
	}

	public function CancelTenderUpdate() {
		$tenderid = $_POST["tenderid"];
		$reason = $_POST["reason"];
		if (!empty($tenderid) && !empty($reason)) {
			$cancel = $this->Tender_Model->tenderCancelUpdate($tenderid,$reason);
			if(!empty($cancel)){
			   $this->recent_activity($tenderid,"Cancelled");
			   $this->session->set_flashdata('success', 'Tender cancellation successfully');
               echo "success";
			}
		}
	}
	public function getCancelReasonTender(){
		$tenderid = $_POST["tenderid"];
		if (!empty($tenderid)) {
			$this->db->select('reason');
			$this->db->from('tbl_tender');
			$this->db->where('tender_id',$tenderid);
			$query=$this->db->get()->result();
			foreach ($query as $value) {
				$reason= $value->reason;
			}
			echo "success#^^#".$reason;
		}
	}

	public function part_number_exists() {
		$partno = $_POST["part_no"];
		if (!empty($partno)) {
			$this->db->where('part_no', $partno);
			$this->db->where("status != 'Awarded'");
			$query = $this->db->get('tbl_tender');
			if ( $query->num_rows() > 0 ) {
				echo 'exists';
			} else {
				echo 'not-exists';
			}
		} else {
			echo 'not-exists';
		}
	}

	public function logAdminTender() {
		$tenderid = $_POST["tenderid"];
		$post_type = $_POST["post_type"];
		if ($tenderid !="") {
			$this->db->select('*');
			$this->db->from('tbl_tender_activity');
			$this->db->where('tender_id',$tenderid);
			$query=$this->db->get();
			//$str = $this->db->last_query(); exit;
			$table='';
			if ( $query->num_rows() > 0 ) {
				$no=1;
				foreach ($query->result() as $value) {
				$adminname  = $value->created_by;
				if($adminname != $logname){
					$username = $adminname;
				}
				$status     = $value->status;
				if(strtolower($status)=="add"){
					$logstatus="E-Quote Added";
				}
				if(strtolower($status)=="update"){
					$logstatus="E-Quote Updated";
				}
				if(strtolower($status)=="cancelled"){
					$logstatus="E-Quote Cancelled";
				}
				if(strtolower($status)=="awarded"){
					$logstatus="E-Quote Awarded";
				}
				$created_on = date("d/m/Y",strtotime($value->created_on));
				$table.='<tr><td>'.$no.'</td><td>'.$username.'</td><td>'.$logstatus.'</td><td>'.$created_on.'</td></tr>';	
				$logname = $username;
				$no++;
			}
			echo "success##**##".$table;
			} else {
              // $table.="<tr colspan='4' style='text-align:center'><td></td><td>No record(s) Found.</td><td></td><td></td></tr>";
              echo "success##**##".$table;
			}
		}

	}
}

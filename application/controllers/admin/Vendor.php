<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admistrator
 *
 * @author Shenll
 */
class Vendor extends Admin_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('Vendor_Model');
		$this->load->library('email');
		$config = array (
			'mailtype'=>'html'
		);
	}

	public function add_vendor()
	{
		$data = array();
		$data['subview'] = $this->load->view('admin/add_vendor', $data, TRUE);
		$this->load->view('admin/_layout_main', $data);
	}

	public function edit_vendor()
	{
		$data = array();
		$data['subview'] = $this->load->view('admin/edit_vendor', $data, TRUE);
		$this->load->view('admin/_layout_main', $data);
	}

	public function vendor_list()
	{
		$data = array();
	    if (!empty($_POST['vendor'])) {
			$data['search'] = $_POST['vendor'];
		}
		$data['vendor_list'] = $this->Vendor_Model->get_vendor_list();
		$data['subview'] = $this->load->view('admin/vendor_list',array('data'=>$data), TRUE);
		$this->load->view('admin/_layout_main', $data);
	}

	public function get_vendor() {
		$vendor_id = $_POST["vendor_id"];
		$query = $this->db->select('*')
					->from('tbl_vendors')
					->where('vendor_id',$vendor_id)
					->get();
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$status = $row->status;
				$rating = $row->rating;

			}
			echo "success#^^#".$status."#^^#".$rating; 
		}
	}

	// public function get_vendor() {
	// 	if($this->input->post('submit')=='submit'){
	// 		$id=$this->input->post('id');
	// 		$status=$this->input->post('status');
	// 		$data=array("status"=>$status,"modified_date"=>date('Y-m-d H:i:s'));
	// 		$vendor_status = $this->Vendor_Model->update_vendor($id,$data);
	// 		$this->session->set_flashdata('message', 'Vendor status updated successfully');
	// 		$data['vendor_list'] = $this->Vendor_Model->get_vendor_list();
	// 		$data['subview'] = $this->load->view('admin/vendor_list',array('data'=>$data), TRUE);
	// 		$this->load->view('admin/_layout_main', $data);
	// 	}
	// }

	public function update_status() {
		if (isset($_POST['vendor_data'])) {
			$decrpted = $this->tenderapi_decrypt_data($_POST['vendor_data']);
			parse_str($decrpted, $parse_data);
			$vendor = $this->Vendor_Model->update_vendor_status($parse_data);
			if ($vendor) {
				$this->session->set_flashdata('message', 'Vendor status updated successfully');
				echo 'success';
			}
			else
				echo 'failure';
		}
	}

	public function invite_vendor() {
		if (isset($_POST['vendor_data'])) {
			$decrpted = $this->tenderapi_decrypt_data($_POST['vendor_data']);
			parse_str($decrpted, $parse_data);
			$invited = $this->Vendor_Model->add_vendor_invite($parse_data);
			if ($invited) {
				$mail_sent = $this->vendor_invite_email($parse_data);
			}
			else
				echo 'Please try again.';
		}
	}

	public function vendor_invite_email($vendor_details) {
		$this->email->from('ilayabharathi.shenll@gmail.com', 'E-Quote');
		$this->email->to($vendor_details['email']);
		$this->email->subject('Invitation for E Quote');
		$this->email->set_mailtype("html");
		$this->email->message(
			'<div>
				<p>Hi '.$vendor_details['email'].',</p><br>
				<p>We are very happy for working with you. Please click <a href="'.base_url().'/supplier/login/register">here</a> to register as our supplier. Thankyou.</p><br><br>
				<p>Best Regards,<br>Support Team<br>E-Quote Management<br></p>
			</div>'
		);

		if ($this->email->send()) {
			echo 'success';
			$this->session->set_flashdata('message_inivte', 'Vendor Invited successfully');
		} else {
			echo $this->email->print_debugger();
		}
	}

	public function tenderapi_decrypt_data($data) {
		return base64_decode($data);
	}
	public function restoreajax() {

		$vendorid=$_POST["vendorid"];
		$datavalue=$_POST["datavalue"];
		if (!empty($vendorid)) {
			if (strtolower($datavalue) =="trash") {
				$status="Trash";
				$msg="Vendor updated successfully";
			} else {
				$status="Inactive";
				$msg='Vendor updated successfully';
			}
			$data=array("status"=>$status,"modified_date"=>date('Y-m-d H:i:s'));
			$vendor_status = $this->Vendor_Model->update_vendor($vendorid,$data);
			$this->session->set_flashdata('message', $msg);
			echo 'success';
			
		} else {
			echo 'failure';
		}

	}
	public function delete($id){
		$data=array("status"=>"Trash","modified_date"=>date('Y-m-d H:i:s'));
		$vendor_status = $this->Vendor_Model->update_vendor($id,$data);
		$this->session->set_flashdata('message', 'Vendor status deleted successfully');
		$data['vendor_list'] = $this->Vendor_Model->get_vendor_list();
		$data['subview'] = $this->load->view('admin/vendor_list',array('data'=>$data), TRUE);
		$this->load->view('admin/_layout_main', $data);
	}
}

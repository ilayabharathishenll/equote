<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admistrator
 *
 * @author Shenll
 */
class Awards extends Admin_Controller {

	public function award_list()
	{
		$data = array();
		if (!empty($_POST['award'])) {
			$data['search'] = $_POST['award'];
		}
		$this->load->model("Award_Model");
		$data['getAwardList'] = $this->Award_Model->getAwardList();
		$data['subview'] = $this->load->view('admin/award_list', $data, TRUE);
		$this->load->view('admin/_layout_main', $data);
	}

}

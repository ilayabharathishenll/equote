<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admistrator
 *
 * @author Shenll
 */
class Dashboard extends Supplier_Controller {

	public function index()
	{
		$data = array();
		$this->load->model('Dashboard_Model');
		if(!empty($tender)){
			$this->tender_cron($tender);
		}
		$data['success'] = $this->Dashboard_Model->DashboardFileUpload();
		$data['dashboarCount'] = $this->Dashboard_Model->DashboardCounts();
		$data['subview'] = $this->load->view('supplier/dashboard', $data, TRUE);
		if ($data['success']=="success") {
            $this->session->set_flashdata("success","Supplier document upload successfully.");
			redirect("supplier/dashboard");
		} else {
			$this->load->view('supplier/_layout_main', $data);
		}
		
	}
	public function download($name=null){
    	force_download('./uploads/'.$name, NULL);
    }
}
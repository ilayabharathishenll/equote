<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Login extends MY_Controller {
    public function index()
	{  
		$this->load->database();
		$this->load->helper(array('form','url'));
		$this->load->library('session');
	    $this->load->model('Supplier_Model'); 
        $logindetails=  $this->Supplier_Model->SupplierLogin();
		if(!empty($logindetails)) {
			$vendor_id=$this->session->userdata('vendor_id');
			if(!empty($vendor_id)) {
				$this->session->set_flashdata('success', 'You have successfully logged in');
				redirect('supplier/dashboard');
			} else {
				$this->session->set_flashdata('message_login', '');
				$this->load->view('supplier/login');
			}
			
		} else {
			$submit = $this->input->post("submit");
			if (!empty($submit)) {
				$this->session->set_flashdata('message_login', 'display:block');
			} else {
				$this->session->set_flashdata('message_login', 'display:none');
			}
			$this->load->view('supplier/login');
		}
	}
	public function forgot_password()
	{    
		$this->load->model('Supplier_Model'); 
        $ForgotDetails=  $this->Supplier_Model->ForgotPassword();
        if($ForgotDetails=="Success") {
        	$this->session->set_flashdata('message', 'An email has been sent with password re-set instructions.');
        }
        if($ForgotDetails=="Invaild") {
        	$this->session->set_flashdata('failure', 'Invaild register email address.');
        }
        if($ForgotDetails=="Notsend") {
        	$this->session->set_flashdata('failure', 'Your Mail is not Sent');
        }
		$this->load->view('supplier/forgot_password');
	}

	public function reset_password()
	{    
		$vendorId=$this->uri->segment(4);
		$this->load->model('Supplier_Model'); 
        $VendorDetails=  $this->Supplier_Model->ResetPassword($vendorId);
       
        if(!empty($VendorDetails)) {
          $this->session->set_flashdata('message', 'New password updated successfully.');
          redirect('supplier/login');
        } else {
		   $this->load->view('supplier/reset_password');
        }
		
	}
	public function register()
	{
		$this->load->model('Supplier_Register');
        $registerData=  $this->Supplier_Register->Supplierreg();
        if(!empty($registerData) &&  $registerData != NULL) {
        	$this->session->set_flashdata('message', 'Supplier register successfully.');
            redirect('supplier/login');
        } else {
        	$data["catgoryData"]=  $this->Supplier_Register->Suppliercategory();
			$this->load->view('supplier/register',$data);
        }
	}

	public function register_email_exists()
	{
		$email=$_POST["email"];
		$this->db->where('email', $email);
		$query = $this->db->get('tbl_vendors');
		if ( $query->num_rows() > 0 ) { 
			echo 'false';
		} else { 
			echo 'true';
		}
	}

	public function cronvendornotification()
	{    
		$this->load->model('Supplier_Model'); 
        $VendorCronDetails=  $this->Supplier_Model->cronVendorMail();
	}
}
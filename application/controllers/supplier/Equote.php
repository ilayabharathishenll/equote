<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admistrator
 *
 * @author Shenll
 */
class Equote extends Supplier_Controller {

	public function index()
	{
		$data = array();
		if (!empty($_POST['equote'])) {
			$data['search'] = $_POST['equote'];
		}
		$user_data=$this->session->userdata('user_data');
		$data['tender_list']=$this->Supplier_Model->tender_list();
		$data['subview'] = $this->load->view('supplier/supplier_quote', array('data'=>$data), TRUE);
		$this->load->view('supplier/_layout_main', $data);
	}
	public function change_tender_price($tender_id)
	{
		$user_id=$this->session->userdata('vendor_id');
		//print_r($user_id);
		$data=$status= array();
		$vendor_id=$user_id;
		//$tender_id='';
		$data=$this->Supplier_Model->view_list($tender_id);
		//
		if($this->input->post('submit') !=''){
			if(!empty($data['tender_list'])){
				$tender_id=$data['tender_list'][0]->tender_id;
				$datas['price']=$this->input->post("price");
				$datas['created_by']=$user_id;
				$datas['modified_by']=$user_id;
				$datas['vendor_id']=$vendor_id;
				$datas['tender_id']=$tender_id;
				$status['modified_on']=date('Y-m-d H:i:s');
				$datas['status']='Active';
				$check_application=$this->Supplier_Model->check($tender_id,$vendor_id);
				if(!empty($check_application)){
					$datas['modified_date']=date('Y-m-d H:i:s');
					$inserted_id=$check_application;
					$this->Supplier_Model->update_application($check_application,$datas);
				}else{
					$datas['created_date']=date('Y-m-d H:i:s');
					$datas['modified_date']=date('Y-m-d H:i:s');
					$inserted_id=$this->Supplier_Model->add($datas);
				}
				if(!empty($inserted_id)){
					$application["apply_id"]=$inserted_id;
					$application["tender_id"]=$tender_id;
					$application["vendor_id"]=$vendor_id;
					$application["price"]= $this->input->post('price'); 
					$application["created_by"]=$user_id;
					$application["created_on"]=date('Y-m-d H:i:s');
					$application_id=$this->Supplier_Model->add_application($application);
					$audit['tender_id']=$tender_id; 
					$audit['audit_details']=$application_id;
					$audit['audit_date']=date('Y-m-d H:i:s');
					$audit_id=$this->Supplier_Model->add_tender_audit($audit);
					$this->Supplier_Model->update_invitation($tender_id,$status);
				}
				if($data['tender_list'][0]->price_edit=='Yes'){
					redirect('supplier/bidding/bidding_applied_tenders_list');
				}				
				redirect('supplier/equote/applied_tenders_list');
			}
			else{
				redirect('supplier/equote/applied_tenders_list');
			}
		}
		$data['subview'] = $this->load->view('supplier/change_tender_price', array('data'=>$data), TRUE);
		$this->load->view('supplier/_layout_main', $data);
		
	}
	public function applied_tenders_list()
	{
		$vendorId=$this->session->userdata('vendor_id');
		$data = array();
		if (!empty($_POST['equotes'])) {
			$data['search']= $_POST['equotes'];
		}
		$data['tender_list']=$this->Supplier_Model->applied_vendortotender_list($vendorId);
		$data['subview'] = $this->load->view('supplier/applied_tenders_list', array('data'=>$data), TRUE);
		$this->load->view('supplier/_layout_main', $data);
	}
	public function download($name=null){
    	force_download('./uploads/'.$name, NULL);
    }

    public function view_tender_application(){
    	$data = array();
    	$tenderID = $this->uri->segment(4);
		$vendorID=$this->session->userdata('vendor_id');
		$data["singlebiddingList"]=$this->Bidding_Model->change_bid_tenders($tenderID,$vendorID);
		$data['subview'] = $this->load->view('supplier/view_tender_price', array('data'=>$data), TRUE);
		$this->load->view('supplier/_layout_main', $data);
    }

    public function individual_quote() {
    	$tenderID = $this->uri->segment(4);
		$vendorID=$this->session->userdata('vendor_id');
        $vendor_id=$this->input->post("hnd_vendor_id");
        $data = array();
		if(!empty($vendor_id)) {
			$data= $this->Bidding_Model->upadtebidding($vendor_id,$tenderID);
			if ($data=="success") {
				$this->session->set_flashdata('message', 'Applied tender has been added successfully.');
                redirect("supplier/equote/applied_tenders_list");
			}
		} else {
			$data["singleList"]=$this->Bidding_Model->change_bid_tenders($tenderID,$vendorID);
			$data['subview'] = $this->load->view('supplier/individual_quote', array('data'=>$data), TRUE);
			$this->load->view('supplier/_layout_main', $data); 
		}
    }
}
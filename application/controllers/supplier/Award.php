<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admistrator
 *
 * @author Shenll
 */
class Award extends Supplier_Controller {

	public function index()
	{
		$data = array();
		if (!empty($_POST['award'])) {
			$data['search'] = $_POST['award'];
			//print_r($data['search']);
		}
		$this->load->model("Award_Model");
		$data['supplierAwardList'] = $this->Award_Model->supplierAwardList();
		$data['subview'] = $this->load->view('supplier/supplier_award',array('data'=>$data), TRUE);
		$this->load->view('supplier/_layout_main', $data);
	}
}
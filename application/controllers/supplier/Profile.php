<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admistrator
 *
 * @author Shenll
 */
class Profile extends Supplier_Controller {
    public function index()
	{   
		$data = array();
		$vendor_id=$this->session->userdata('vendor_id');

		if($this->input->post("first_name")) {
            $this->load->model('Supplier_Register');
            $data=  $this->Supplier_Register->Updateprofile($vendor_id);
            $this->session->set_flashdata('message', 'Profile updated Successfully.');
            redirect('supplier/profile');
		} else {
		   $this->load->model('Supplier_Register');
		   $data["catgoryData"]=  $this->Supplier_Register->Suppliercategory();
           $data['vendordetails']=  $this->Supplier_Register->Getprofile($vendor_id);
           $data['subview'] = $this->load->view('supplier/profile_setting', $data, TRUE);
		   $this->load->view('supplier/_layout_main', $data);
		}
		
	}
}
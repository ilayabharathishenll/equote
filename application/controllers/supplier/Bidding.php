<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admistrator
 *
 * @author Shenll
 */
class Bidding extends Supplier_Controller {
	public function bidding_applied_tenders_list()
	{
		$data = array();
		if (!empty($_POST['bidding'])) {
			$data['search'] = $_POST['bidding'];
		}
		$this->load->model("Bidding_Model");
		$data['biddingGetList']= $this->Bidding_Model->getbiddingList();
		$data['subview'] = $this->load->view('supplier/bidding_applied_tenders_list', $data, TRUE);
		$this->load->view('supplier/_layout_main', $data);
	}
	// public function bidding_applied_tenders_list()
	// {
	// 	$data = array();
	// 	$user_id=$this->session->userdata('vendor_id');
	// 	$data=$this->Bidding_Model->applied_tenders_list($user_id);
	// 	$data['subview'] = $this->load->view('supplier/bidding_applied_tenders_list', array('data'=>$data), TRUE);
	// 	$this->load->view('supplier/_layout_main', $data);
	// }
	public function change_tender_price()
	{
		$tenderID = $this->uri->segment(4);
		$vendorID=$this->session->userdata('vendor_id');
        $vendor_id=$this->input->post("hnd_vendor_id");
		if(!empty($vendor_id)) {
			$data= $this->Bidding_Model->upadtebidding($vendor_id,$tenderID);
			if ($data=="success") {
				$this->session->set_flashdata('message', 'Bidding updated successfully.');
                redirect("supplier/bidding/bidding_applied_tenders_list");
			}
		} else {
			$data["singlebiddingList"]=$this->Bidding_Model->change_bid_tenders($tenderID,$vendorID);
			$data['subview'] = $this->load->view('supplier/change_tender_price', array('data'=>$data), TRUE);
			$this->load->view('supplier/_layout_main', $data);
		}
		
	}
}
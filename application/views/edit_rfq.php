<?php 
//print_r($data);exit;
function icon($value){
    $outputs=explode(".",$value);
    $icons=$outputs[1];
    if($icons=='jpg' || $icons=='jpeg' || $icons=='png' || $icons=='gif' || $icons=='svg'){
        return "image";
    }elseif($icons=='doc' || $icons=='docx'){
         return "word";
    }elseif($icons=='xls' || $icons=='xlsx'){
         return "excel";    
    }elseif($icons=='PDF'){
         return "pdf";  
    }elseif($icons=='ppt' || $icons=='pptx'){
         return "powerpoint";  
    }
}
foreach($data['tender_list'] as $tender){ ?>
<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/tenders.png" class="imgbasline"> RFQ Details
            </div>
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <form  name="frms_rfq" id="frms_rfq" class="horizontal-form" method="POST" enctype="multipart/form-data">
                <div class="form-body">
                    <h3 class="form-section formheading">Tender Basic Detail</h3>
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">RFQ Number</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="rfq_no" id="rfq_no" placeholder="" value="<?php echo $tender->ref_no;?>" readonly>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Part Name</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="sel_partname" id="sel_partname" value="<?php echo !empty($tender->part_name)?$tender->part_name:"";?>">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->

                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Delivery Duration</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="delivery_duration" id="delivery_duration" placeholder="Delivery Duration in Days" value="<?php echo !empty($tender->delivery_duration)?$tender->delivery_duration:"";?>">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Delivery Date</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="delivery_date" id="delivery_date" autocomplete="off" data-date-format="dd/mm/yyyy" value="<?php echo date('d/m/Y',strtotime($tender->delivery_date));?>">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->

                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Quantity</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="sel_quantity" id="sel_quantity" value="<?php echo !empty($tender->quantity)?$tender->quantity:"0";?>">
                                  
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Tender Base Price</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="base_price" id="base_price" placeholder="" value="<?php echo !empty($tender->base_price)?$tender->base_price:"0";?>" >
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->

                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Tender Title</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="tender_title" id="tender_title" placeholder="" value="<?php echo !empty($tender->tender_title)?$tender->tender_title:"-";?>">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Upload drawing & Technical Spesc</label>
                                <div class="col-md-8">
                                    <div class="col-md-12" style="padding: 0;margin-bottom: 15px;font-size: 12px;">
                                                <input type="file" name="upload_drawing" id="upload_drawing">
                                        </div>
                                    
                                     <div class="col-md-10" style="padding: 0;">
                                        <a style="display: inline-block;margin-bottom: 5px;font-size: 12px;text-decoration: none;" href="<?php echo base_url('admin/tender/download');?>/<?php echo !empty($tender->drawing_doc)?$tender->drawing_doc:"#"; ?>"><?php echo !empty($tender->drawing_doc)?$tender->drawing_doc:"#"; ?></a>
                                    </div>
                                    <div class="col-md-2">
                                        <button type="button" class="btn btn-danger customaddmorebtn" name="remove" id="remove" style="font-size: 10px;border-radius: 50% !important;">
                                            <span class="fa fa-remove" aria-hidden="true"></span>
                                        </button>
                                    </div>
                                </div>
                                <!--  <div class="col-md-1">
                                    <input type="file" name="upload_drawing" id="upload_drawing">

                                </div> -->
                               
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->

                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Norm</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="norm" id="norm" placeholder="" value="<?php echo !empty($tender->norm)?$tender->norm:"-";?>">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">RM Price</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="rm_price" id="rm_price" placeholder="" value="<?php echo !empty($tender->rm_price)?$tender->rm_price:"-";?>">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-12 paddingbottom">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <textarea class="form-control" name="tender_detail" id="tender_detail" rows="5" cols="50" placeholder="Enter the Tender Details...."><?php echo !empty($tender->tender_detail)?$tender->tender_detail:"-";?></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/row-->

                    <h3 class="form-section formheading">Upload Tender Documents</h3>
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="col-md-10" style="padding-right: 0px;">
                                <span class="help-block pull-right">(Upload Multiple Document's)</span>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-5" style="padding-right: 0px;">Tender Terms and Condition</label>
                                <div class="col-md-5">
                                    <input type="file" name="upload_terms[]" id="upload_terms">
                                </div>
                                <div class="col-md-2">
                                     <button type="button" class="btn btn-primary customaddmorebtn" name="addmore" id="addmore"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
                                </div>
                            </div>
                            <div id="terms">
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="col-md-10" style="padding-right: 0px;">
                                <span class="help-block pull-right">(Upload Multiple Document's)</span>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-5">Help Document</label>
                                <div class="col-md-5">
                                   
                                    <input type="file" name="upload_helpdoc[]" id="upload_helpdoc">
                                </div>
                                <div class="col-md-2">
                                     <button type="button" class="btn btn-primary customaddmorebtn" name="helpmore" id="helpmore"><span class="glyphicon glyphicon-plus" aria-hidden="true" ></span></button>
                                </div>
                            </div>
                            <div id="helpdoc">
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-5">Tender Detail</label>
                                <div class="col-md-5">
                                    <input type="file" name="upload_tenderdetail[]" id="upload_tenderdetail">
                                </div>
                                <div class="col-md-2">
                                     <button type="button" class="btn btn-primary customaddmorebtn" name="detailmore" id="detailmore"><span class="glyphicon glyphicon-plus" aria-hidden="true" ></span></button>
                                </div>
                            </div>
                            <div id="detaildoc">
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-5" style="padding-right: 0px;">Tender Documents Required</label>
                                <div class="col-md-5">

                                    <input type="file" name="upload_tenderdoc[]" id="upload_tenderdoc">
                                </div>
                                <div class="col-md-2">
                                     <button type="button" class="btn btn-primary customaddmorebtn" name="tenderdocmore" id="tenderdocmore"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
                                </div>
                            </div>
                            <div id="tenderdoc">
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-5">Tender Document</label>
                                <div class="col-md-5">
                                    <input type="file"  name="tender_docone[]" id="tender_docone">
                                </div>
                                <div class="col-md-2">
                                     <button type="button" class="btn btn-primary customaddmorebtn" name="tenderdoconemore" id="tenderdoconemore"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
                                </div>
                            </div>
                            <div id="tenderdocone">
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-5">Tender Document</label>
                                <div class="col-md-5">
                                    <input type="file" name="tender_doctwo[]" id="tender_doctwo">
                                </div>
                                <div class="col-md-2">
                                     <button type="button" class="btn btn-primary customaddmorebtn" name="tenderdoctwomore" id="tenderdoctwomore"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
                                </div>
                            </div>
                             <div id="tenderdoctwo">
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->

                    <h3 class="form-section formheading">Tender Date Detail</h3>
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Start Date & Time</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="start_date" id="start_date" autocomplete="off" data-date-format="dd/mm/yyyy" value="<?php echo date('d/m/Y',strtotime($tender->start_date));?>">

                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">End Date & Time</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="end_date" id="end_date" autocomplete="off" data-date-format="dd/mm/yyyy" value="<?php echo date('d/m/Y',strtotime($tender->end_date));?>">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                </div>
                <div class="form-actions formbtncenter">
                    <button type="submit" name="submit" value="submit" class="btn green customsavebtn">
                        <i class="fa fa-check"></i> Save Data
                    </button>
                    <button type="button" class="btn red customrestbtn" id="resetEmpty"> <i class="fa fa-refresh"></i> Reset Data</button>
                </div>
            </form>
            <!-- END FORM-->
        </div>
    </div>
</div>
<?php } ?>
<!-- END CONTENT BODY -->
<?php
error_reporting(0);
$user_data=$this->session->userdata();
if (strtolower($user_data['status']) !='active') {
    redirect("supplier/dashboard");
}
$part_no= $part_name= $ref_no= $base_price= $quantity= $delivery_duration=$start_date= $end_date= $description= $applyprice= $vendor_id=  $drawingspes= $part_price="";
    $start_time =  $end_time="";
if(count($data["singlebiddingList"])>0) {
   $part_no = $data["singlebiddingList"]["part_no"];
   $part_name = $data["singlebiddingList"]["part_name"];
   $ref_no = $data["singlebiddingList"]["ref_no"];
   $base_price =$data["singlebiddingList"]["base_price"];
   $part_price =$data["singlebiddingList"]["part_price"];
   $quantity =$data["singlebiddingList"]["quantity"];
   $delivery_duration = $data["singlebiddingList"]["delivery_duration"];
   $start_date = date("d/m/Y",strtotime($data["singlebiddingList"]["start_date"]));
   $end_date = date("d/m/Y",strtotime($data["singlebiddingList"]["end_date"]));
   $start_time = date("H:i A",strtotime($data["singlebiddingList"]["start_time"]));
   $end_time = date("H:i A",strtotime($data["singlebiddingList"]["end_time"]));
   $description =$data["singlebiddingList"]["description"];
   $applyprice= $data["singlebiddingList"]["applyprice"];
   $vendor_id= $data["singlebiddingList"]["vendor_id"];
   $drawingspes = $data["singlebiddingList"]["drawing_doc"];
}
?>
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                 <img src="<?php echo base_url()?>assets/layouts/layout/img/de-active/bitting.png" class="imgbasline">Supplier Can Change the E Quote Price</div>
            <div class="tools">
                <!-- <a href="javascript:;" class="collapse"> </a>
                <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                <a href="javascript:;" class="reload"> </a>
                <a href="javascript:;" class="remove"> </a> -->
            </div>
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <form name="frm_rfq" id="frm_rfq" action="" class="ind-quote horizontal-form" method="POST">
                <input type="hidden" name="hnd_vendor_id" id="hnd_vendor_id" value="<?php echo $vendor_id ?>">
                <div class="form-body">
                    <h3 class="form-section formheading">E Quote Basic Detail</h3>
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Part No</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="part_no" id="part_no" placeholder="" value="<?php echo $part_no ?>" disabled>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Part Name</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="part_name" id="part_name" placeholder="" value="<?php echo $part_name ?>" disabled>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">RFQ Number</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="rfq_no" id="rfq_no" placeholder="" value="<?php echo $ref_no ?>" disabled>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Part Price</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="base_price" id="base_price" placeholder="" value="<?php echo $part_price ?>" disabled>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Qunatity</label>
                                <div class="col-md-8"> 
                                     <input type="text" class="form-control" name="quantity" id="quantity" placeholder="" value="<?php echo $quantity ?>" disabled>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Delivery Duration</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="delivery_duration" id="delivery_duration" placeholder="Delivery Duration in Days" value="<?php echo $delivery_duration ?>" disabled>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                     
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Start Date & Time</label>
                                <div class="col-md-5">
                                    <input type="text" class="form-control" name="start_date" id="start_date" autocomplete="off" value="<?php echo $start_date?>" disabled>
                                </div>
                                <div class="col-md-3">
                                    <input type="text" class="form-control" name="start_time" id="start_time" autocomplete="off" value="<?php echo $start_time?>" disabled>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">End Date & Time</label>
                                <div class="col-md-5">
                                    <input type="text" class="form-control" name="end_date" id="end_date" autocomplete="off" value="<?php echo $end_date?>" disabled> 
                                </div>
                                <div class="col-md-3">
                                    <input type="text" class="form-control" name="end_time" id="end_time" autocomplete="off" value="<?php echo $end_time?>" disabled>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Upload drawing & Technical Spes</label>
                                <div class="col-md-8"> 
                                    <?php
                                    if(!empty($drawingspes)){
                                    ?>
                                    <a class="download_doc" href="<?php echo base_url()."uploads/".$drawingspes?>" target="_blank"><img src="<?php echo base_url()."assets/layouts/layout/img/dow.png" ?>" style="margin-bottom: 10px;">Click here to download</a>
                                    <?php
                                    } else {
                                        echo ":-";
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <h3 class="form-section formheading">E Quote Detailed Description or Information</h3>
                    <div class="row">
                        <div class="col-md-12 paddingbottom">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <textarea class="form-control" name="tender_detail" id="tender_detail" rows="5" cols="50" placeholder="Enter the E Quote Details...." disabled><?php echo  $description ?></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/row-->

                    <h3 class="form-section formheading">E Quote Documents</h3>
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <?php
                                //$data["singlebiddingList"]["upload_terms"]="";
                                if(count($data["singlebiddingList"]["upload_terms"],COUNT_RECURSIVE)>1) {
                                    $termcnt=1;
                                    foreach ($data["singlebiddingList"]["upload_terms"] as $termskey) {
                                        foreach ($termskey as $keys => $DocTermsvalue) {
                                            if ($termcnt==1) {
                                                echo '<label class="control-label col-md-5" style="padding-right: 0px;">E Quote Terms and Condition :</label>';
                                            } else {
                                                echo '<label class="control-label col-md-5" style="padding-right: 0px;"></label>';
                                            }
                                        ?>
                                            <div class="col-md-5">
                                                <a class="download_doc" href="<?php echo base_url()."uploads/".$termskey["doc_name"]?>" target="_blank"><img src="<?php echo base_url()."assets/layouts/layout/img/dow.png" ?>" style="margin-bottom: 10px;">Click here to download</a>
                                            </div>
                                            <div class="col-md-2">
                                            </div>
                                        <?php
                                        }
                                        $termcnt++;
                                    }
                                } else {
                                    echo '<label class="control-label col-md-5" style="padding-right: 0px;">E Quote Terms and Condition </label><div class="col-md-5">
                                        : <a href="">-</i></a>
                                        </div>
                                        <div class="col-md-2">
                                        </div>';
                                }
                                ?>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <?php
                                if(count($data["singlebiddingList"]["upload_helpdoc"],COUNT_RECURSIVE)>1) {
                                    $helpcnt=1;
                                    foreach ($data["singlebiddingList"]["upload_helpdoc"] as $helpkey) {
                                        foreach ($helpkey as $keys => $Dochelpvalue) {
                                            if ($helpcnt==1) {
                                                echo '<label class="control-label col-md-5" style="padding-right: 0px;">Help Document :</label>';
                                            } else {
                                                echo '<label class="control-label col-md-5" style="padding-right: 0px;"></label>';
                                            }
                                        ?>
                                            <div class="col-md-5">
                                                <a class="download_doc" href="<?php echo base_url()."uploads/".$helpkey["doc_name"]?>" target="_blank"><img src="<?php echo base_url()."assets/layouts/layout/img/dow.png" ?>" style="margin-bottom: 10px;">Click here to download</a>
                                            </div>
                                            <div class="col-md-2">
                                            </div>
                                        <?php
                                        }
                                        $helpcnt++;
                                    }
                                } else {
                                    echo '<label class="control-label col-md-5" style="padding-right: 0px;">Help Document </label><div class="col-md-5">
                                        : <a href="">-</i></a>
                                        </div>
                                        <div class="col-md-2">
                                        </div>';
                                }
                                ?>
                                
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <?php
                                if(count($data["singlebiddingList"]["upload_tenderdetail"],COUNT_RECURSIVE)>1) {
                                    $tenderdetail=1;
                                    foreach ($data["singlebiddingList"]["upload_tenderdetail"] as $tenderdetailkey) {
                                        foreach ($tenderdetailkey as $keys => $Doctenderdetailvalue) {
                                            if ($tenderdetail==1) {
                                                echo '<label class="control-label col-md-5" style="padding-right: 0px;">E Quote Detail :</label>';
                                            } else {
                                                echo '<label class="control-label col-md-5" style="padding-right: 0px;"></label>';
                                            }
                                        ?>
                                            <div class="col-md-5">
                                                <a class="download_doc" href="<?php echo base_url()."uploads/".$tenderdetailkey["doc_name"]?>" target="_blank"><img src="<?php echo base_url()."assets/layouts/layout/img/dow.png" ?>" style="margin-bottom: 10px;">Click here to download</a>
                                            </div>
                                            <div class="col-md-2">
                                            </div>
                                        <?php
                                        }
                                        $tenderdetail++;
                                    }
                                } else {
                                    echo '<label class="control-label col-md-5" style="padding-right: 0px;">E Quote Detail </label><div class="col-md-5">
                                        : <a href="">-</i></a>
                                        </div>
                                        <div class="col-md-2">
                                        </div>';
                                }
                                ?>
                                
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <?php
                                if(count($data["singlebiddingList"]["upload_tenderdoc"],COUNT_RECURSIVE)>1) {
                                    $tenderdoccnt=1;
                                    foreach ($data["singlebiddingList"]["upload_tenderdoc"] as $tenderdockey) {
                                        foreach ($tenderdockey as $keys => $Doctenderdockeyvalue) {
                                            if ($tenderdoccnt==1) {
                                                echo '<label class="control-label col-md-5" style="padding-right: 0px;">E Quote Documents Required :</label>';
                                            } else {
                                                echo '<label class="control-label col-md-5" style="padding-right: 0px;"></label>';
                                            }
                                        ?>
                                            <div class="col-md-5">
                                                <a class="download_doc" href="<?php echo base_url()."uploads/".$tenderdockey["doc_name"]?>" target="_blank"><img src="<?php echo base_url()."assets/layouts/layout/img/dow.png" ?>" style="margin-bottom: 10px;">Click here to download</a>
                                            </div>
                                            <div class="col-md-2">
                                            </div>
                                        <?php
                                        }
                                        $tenderdoccnt++;
                                    }
                                } else {
                                    echo '<label class="control-label col-md-5" style="padding-right: 0px;">E Quote Documents Required </label><div class="col-md-5">
                                        : <a href="">-</i></a>
                                        </div>
                                        <div class="col-md-2">
                                        </div>';
                                }
                                ?>
                                
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <?php
                                if(count($data["singlebiddingList"]["tender_docone"],COUNT_RECURSIVE)>1) {
                                    $tenderdoconecnt=1;
                                    foreach ($data["singlebiddingList"]["tender_docone"] as $tenderdoconekey) {
                                        foreach ($tenderdoconekey as $keys => $Doctenderdockeyvalue) {
                                            if ($tenderdoconecnt==1) {
                                                echo '<label class="control-label col-md-5" style="padding-right: 0px;">E Quote Document :</label>';
                                            } else {
                                                echo '<label class="control-label col-md-5" style="padding-right: 0px;"></label>';
                                            }
                                        ?>
                                            <div class="col-md-5">
                                                <a class="download_doc" href="<?php echo base_url()."uploads/".$tenderdoconekey["doc_name"]?>" target="_blank"><img src="<?php echo base_url()."assets/layouts/layout/img/dow.png" ?>" style="margin-bottom: 10px;">Click here to download</a>
                                            </div>
                                            <div class="col-md-2">
                                            </div>
                                        <?php
                                        }
                                        $tenderdoconecnt++;
                                    }
                                } else {
                                    echo '<label class="control-label col-md-5" style="padding-right: 0px;">E Quote Document </label><div class="col-md-5">
                                        : <a href="">-</i></a>
                                        </div>
                                        <div class="col-md-2">
                                        </div>';
                                }
                                ?>
                                
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <?php
                                if(count($data["singlebiddingList"]["tender_doctwo"],COUNT_RECURSIVE)>1) {
                                    $tenderdoctwocnt=1;
                                    foreach ($data["singlebiddingList"]["tender_doctwo"] as $tenderdoctwokey) {
                                        foreach ($tenderdoctwokey as $keys => $Doctenderdoctwovalue) {
                                            if ($tenderdoctwocnt==1) {
                                                echo '<label class="control-label col-md-5" style="padding-right: 0px;">E Quote Document :</label>';
                                            } else {
                                                echo '<label class="control-label col-md-5" style="padding-right: 0px;"></label>';
                                            }
                                        ?>
                                            <div class="col-md-5">
                                                <a class="download_doc" href="<?php echo base_url()."uploads/".$tenderdoctwokey["doc_name"]?>" target="_blank"><img src="<?php echo base_url()."assets/layouts/layout/img/dow.png" ?>" style="margin-bottom: 10px;">Click here to download</a>
                                            </div>
                                            <div class="col-md-2">
                                            </div>
                                        <?php
                                        }
                                        $tenderdoctwocnt++;
                                    }
                                } else {
                                    echo '<label class="control-label col-md-5" style="padding-right: 0px;">E Quote Document </label><div class="col-md-5">
                                        : <a href="">-</i></a>
                                        </div>
                                        <div class="col-md-2">
                                        </div>';
                                }
                                ?>
                                
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <h3 class="form-section formheading">E Quote Price</h3>
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">E Quote Price</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="tender_price" id="tender_price" autocomplete="off" value="<?php echo $base_price ?>" disabled>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">No of Qunatity</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="qunatity_no" id="qunatity_no" autocomplete="off" value="<?php echo $quantity ?>" disabled>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Enter Your Price</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="your_price" id="your_price" autocomplete="off" value="<?php echo $applyprice ?>">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                </div>
                <div class="form-actions formbtncenter">
                    <button type="submit" class="btn green customsavebtn">
                        <i class="fa fa-check"></i> Apply E Quote
                    </button>
                    <!-- <a href="previewbidding" type="button" class="btn red customrestbtn" > <i class="fa fa-eye"></i> Preview Tender</a> -->
                </div>
            </form>
            <!-- END FORM-->
        </div>
    </div>
</div>
<?php
  //print_r($data);
  $user_data=$this->session->userdata();
  if (strtolower($user_data['status']) !='active') {
      redirect("supplier/dashboard");
  }
  $search_rfq =  $search_name = $search_partno='';
  if (!empty($data["search"])) {
    $search_rfq = $data["search"]['rfq_no'];
    $search_partno = $data["search"]['part_no'];
    $search_name = $data["search"]['tender_title'];
  }
?>
<!-- BEGIN CONTENT BODY -->
<?php //print_r($data);exit; ?>
<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="<?php echo base_url();?>assets/layouts/layout/img/de-active/tenders.png" class="imgbasline"> E Quote Details</div>
            <div class="actions">
            </div>
        </div>
        <div class="portlet-body">
          <form name="frm_equote" id="frm_equote" method="POST">
            <div class="row">
              <div class="col-md-12 paddingleftright">
                <div class="col-md-3 paddingbottom">
                  <div class="col-md-12 paddingleftright">
                     <input type="text" class="form-control" name="equote[rfq_no]" id="rfq_no" placeholder="RFQ No" value="<?php echo $search_rfq ?>">
                  </div>
                </div>
                <div class="col-md-3 paddingbottom">
                  <div class="col-md-12 paddingleftright">
                       <input type="text" class="form-control" name="equote[part_no]" id="part_no" placeholder="Part Number" value="<?php echo $search_partno ?>">
                    </div>
                </div>
                <div class="col-md-3 paddingbottom">
                  <div class="col-md-12 paddingleftright">
                      <input type="text" class="form-control" name="equote[tender_title]" id="tender_title" placeholder="E Quote Title" value="<?php echo $search_name ?>">
                  </div>
                </div>
                
                <div class="col-md-3">
                  <div class="col-md-12 paddingleftright">
                    <button type="submit" class="btn btn-warning customsearchtbtn"> <i class="fa fa-search"></i> Search</button>
                    <a href="<?php echo base_url()."supplier/equote"?>" type="button" class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Reset</a>
                  </div>
                </div>
              </div>
            </div>
          	<div class="table-responsive" style="overflow-x: inherit;margin-top:0px;">
  	            <table class="table table-striped table-bordered table-hover suppliertbl" id="supplier-quote">
  	            	<thead>
  	                    <tr>
  	                    	<th> SI.No </th>
  	                        <th> RFQ No </th>
  	                        <th> E Quote Title</th>
  	                        <th> Duration in Days </th>
  	                        <th> E Quote End Date </th>
  	                        <th> Action </th>
  	                    </tr>
  	                </thead>
  	                 </tbody>
                   <?php
                    $i=1;
                    if(!empty($data['tender_list'])){
                    foreach ($data['tender_list'] as $value) { ?>
                     <tr>
                           <td><?php echo $i; ?></td>
                           <td><?php echo $value->ref_no;?></td>
                           <td><?php echo $value->tender_title;?></td>
                           <td><?php echo $value->delivery_duration;?></td>
                           <td><?php echo date("d/m/Y",strtotime($value->end_date));?></td>
                           <td><a href="<?php echo base_url('supplier/equote/individual_quote');?>/<?php echo $value->tender_id;?>" type="button" class="btn green btn-xs customactionbtn"><i class="fa fa-edit"></i> View</a></td>
                        </tr>
                        <?php $i++;
                          }
                         }
                        ?>
                   </tbody>
  	            </table>
  	        </div>
          </form>
        </div>
    </div>
</div>
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                 <img src="<?php echo base_url()?>assets/layouts/layout/img/de-active/bitting.png" class="imgbasline"> Supplier Bidding </div>
            <div class="actions">
                <!-- <a href="javascript:;" class="btn btn-default btn-sm">
                    <i class="fa fa-plus"></i> Add </a>
                <a href="javascript:;" class="btn btn-default btn-sm">
                    <i class="fa fa-print"></i> Print </a> -->
            </div>
        </div>
        <div class="portlet-body">
	        <div class="row">
	        	<div class="col-md-12 paddingleftright">
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                        	<input type="text" class="form-control" name="start_date" id="start_date" autocomplete="off" Placeholder="Tender Start Date">
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                            <input type="text" class="form-control" name="end_date" id="end_date" autocomplete="off" Placeholder="Tender End Date">
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                            <input type="text" class="form-control" name="search_tender" id="search_tender" placeholder="Search Tender">
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                           <input type="text" class="form-control" name="rfq_no" id="rfq_no" placeholder="RFQ No">
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                            <input type="text" class="form-control" name="part_no" id="part_no" placeholder="Part No">
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                           <input type="text" class="form-control" name="tender_title" id="tender_title" placeholder="Tender Title">
                        </div>
	        		</div>
	        		<div class="col-md-3">
	        			<div class="col-md-12 paddingleftright">
	        				<button type="button" class="btn btn-warning customsearchtbtn"> <i class="fa fa-search"></i> Search</button>
	        				<button type="button" class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Reset</button>
	        			</div>
	        		</div>
	        	</div>
	        </div>
        	<div class="table-responsive" style="overflow-x: inherit;margin-top:0px;">
	            <table class="table table-striped table-bordered table-hover" id="tendertbl">
	            	<thead>
	                    <tr>
	                    	<th> SI.NO </th>
	                        <th> RFQ No</th>
	                        <th> E Quote Title</th>
	                        <th> Tender Base Price</th>
	                        <th> Quote Price</th>
	                        <th> Current Price</th>
	                        <th> Action </th>
	                    </tr>
	                </thead>
	                </tbody>
	                    <tr>
	                    	<td>1</td>
	                        <td>TENDER001</td>
	                        <td>Spare Part4</td>
	                        <td>100000</td>
	                        <td>95000</td>
	                        <td>94000</td>
	                        <td><a href="bidding/changebidding" type="button" class="btn green btn-xs customactionbtn"><i class="fa fa-eye"></i> View </a> </td>
	                    </tr>
	                    <!-- <tr>
	                    	<td>2</td>
	                        <td>Vendor-02</td>
	                        <td>100000</td>
	                        <td>96000</td>
	                        <td>94000</td>
	                        <td>iMax Technologies</td>
	                        <td>-</td>
	                    </tr>
	                    <tr>
	                    	<td>3</td>
	                        <td>Vendor-03</td>
	                        <td>100000</td>
	                        <td>97000</td>
	                        <td>94000</td>
	                        <td>SAP Technologies</td>
	                        <td>-</td>
	                    </tr>
	                    <tr>
	                    	<td>4</td>
	                    	<td>Vendor-12</td>
	                        <td>100000</td>
	                        <td>98000</td>
	                        <td>95000</td>
	                        <td>Swap Technologies</td>
	                        <td>-</td>
	                    </tr>
	                    <tr>
	                    	<td>5</td>
	                        <td>Vendor-05</td>
	                        <td>100000</td>
	                        <td>95000</td>
	                        <td>94000</td>
	                        <td>iStudio Technologies</td>
	                        <td><a href="change_bid.php" type="button" class="btn green btn-xs customactionbtn"><i class="fa fa-eye"></i> View </a> </td>
	                    </tr>
	                    <tr>
	                    	<td>6</td>
	                        <td>Vendor-11</td>
	                        <td>100000</td>
	                        <td>96000</td>
	                        <td>94000</td>
	                        <td>iMax Technologies</td>
	                        <td>-</td>
	                    </tr>
	                    <tr>
	                    	<td>7</td>
	                        <td>Vendor-09</td>
	                        <td>100000</td>
	                        <td>97000</td>
	                        <td>94000</td>
	                        <td>SAP Technologies</td>
	                        <td>-</td>
	                    </tr>
	                    <tr>
	                    	<td>8</td>
	                    	<td>Vendor-22</td>
	                        <td>100000</td>
	                        <td>97000</td>
	                        <td>96000</td>
	                        <td>Swap Technologies</td>
	                        <td>-</td>
	                    </tr>
	                    <tr>
	                    	<td>9</td>
	                    	<td>Vendor-20</td>
	                        <td>100000</td>
	                        <td>97000</td>
	                        <td>96000</td>
	                        <td>Swap Technologies</td>
	                        <td>-</td>
	                    </tr>
	                    <tr>
	                    	<td>10</td>
	                        <td>Vendor-20</td>
	                        <td>100000</td>
	                        <td>97000</td>
	                        <td>96000</td>
	                        <td>Network Technologies</td>
	                        <td>-</td>
	                    </tr> -->
	                </tbody>
	            </table>
	        </div>
        </div>
    </div>
</div>
<!-- END CONTENT BODY -->
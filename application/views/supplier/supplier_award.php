<?php
    $user_data=$this->session->userdata();
	if (strtolower($user_data['status']) !='active') {
	    redirect("supplier/dashboard");
	}
	$search_rfq = $search_title = $search_name = $search_date= $search_enddate= $search_partno= $search_partname='';
	if (!empty($data["search"])) {
		$search_rfq = $data["search"]['rfq_no'];
		$search_title = $data["search"]['tender_title'];
		$search_date =$data["search"]['award_date'];
		$search_enddate =$data["search"]['award_enddate'];
		$search_partno = $data["search"]['part_no'];
		$search_partname = $data["search"]['part_name'];
		
	}
?>
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="<?php echo base_url()?>assets/layouts/layout/img/de-active/awards.png" class="imgbasline"> Supplier Awards</div>
            <div class="actions">
                <!-- <a href="javascript:;" class="btn btn-default btn-sm">
                    <i class="fa fa-plus"></i> Add </a>
                <a href="javascript:;" class="btn btn-default btn-sm">
                    <i class="fa fa-print"></i> Print </a> -->
            </div>
        </div>
        <div class="portlet-body">
        	<form name="frm_award" id="frm_award" method="POST">
		        <div class="row">
		        	<div class="col-md-12 paddingleftright">
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                           <input type="text" class="form-control" name="award[rfq_no]" id="rfq_no" placeholder="RFQ No" value="<?php echo $search_rfq ?>">
	                        </div>
		        		</div>
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                           <input type="text" class="form-control" name="award[part_no]" id="part_no" placeholder="Part No" value="<?php echo $search_partno ?>">
	                        </div>
		        		</div>
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                            <input type="text" class="form-control" name="award[tender_title]" id="tender_title" placeholder="E Quote Name" value="<?php echo $search_title ?>">
	                        </div>
		        		</div>
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                           <input type="text" class="form-control" name="award[part_name]" id="part_name" placeholder="Part Name" value="<?php echo $search_partname ?>">
	                        </div>
		        		</div>
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                           <input type="text" class="form-control" name="award[award_date]" id="award_date" placeholder="Awarded From Date" autocomplete="off" value="<?php echo $search_date ?>" data-date-format="dd/mm/yyyy">
	                        </div>
		        		</div>
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                           <input type="text" class="form-control" name="award[award_enddate]" id="award_enddate" placeholder="Awarded To Date" autocomplete="off" value="<?php echo $search_enddate ?>" data-date-format="dd/mm/yyyy">
	                        </div>
		        		</div>
		        		<div class="col-md-3">
		        			<div class="col-md-12 paddingleftright">
		        				<button type="submit" class="btn btn-warning customsearchtbtn"> <i class="fa fa-search"></i> Search</button>
		        				<a href="<?php echo base_url()."supplier/award"?>" type="button" class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Reset</a>
		        			</div>
		        		</div>
		        	</div>
		        </div>
	        	<div class="table-responsive" style="overflow-x: inherit;margin-top:0px;">
		            <table class="table table-striped table-bordered table-hover suppliertbl" id="award-tenders">
		            	<thead>
		                    <tr>
		                    	<th>SI.No</th>
		                        <th>RFQ NO</th>
		                        <th>Part Name</th>
		                        <th>Part Number</th>
		                        <th>E Quote Name</th>
		                        <th>Awarded Date</th>
		                        <th>Awarded</th>
		                    </tr>
		                </thead>
		                </tbody>
		                    <?php
		                    if(!empty($data['supplierAwardList'])){
		                 
		                    	$sno=1;
		                    	foreach($data['supplierAwardList'] as $AwardListData){
		                    ?>
		                    <tr>
		                    	<td><?php echo $sno ?></td>
		                        <td><?php echo $AwardListData["ref_no"] ?></td>
		                        <td><?php echo $AwardListData["part_name"] ?></td>
		                        <td><?php echo $AwardListData["part_no"] ?></td>
		                        <td><?php echo $AwardListData["tender_title"] ?></td>
		                        <td><?php echo date("d/m/Y",strtotime($AwardListData["created_on"])) ?></td>
		                        <td> 
		                        	<i class="icon-badge font-yellow-casablanca"></i>
		                        </td>
		                    </tr>
		                    <?php
		                        $sno++;
		                    	}
		                    }
		                    ?>
		                    
		                </tbody>
		            </table>
		        </div>
		    </form>
        </div>
    </div>
</div>

<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="<?php echo base_url();?>assets/layouts/layout/img/de-active/tenders.png" class="imgbasline"> E Quote Details</div>
            <div class="actions">
            </div>
        </div>
        <div class="portlet-body">
        	<div class="table-responsive" style="overflow-x: inherit;margin-top:0px;">
	            <table class="table table-striped table-bordered table-hover suppliertbl" id="supplier-quote">
	            	<thead>
	                    <tr>
	                    	<th> SI.No </th>
	                        <th> RFQ No </th>
	                        <th> E Quote Title</th>
	                        <th> Duration in Days </th>
	                        <th> E Quote End Date </th>
	                        <th> Action </th>
	                    </tr>
	                </thead>
	                </tbody>
	                    <tr>
	                    	<td> 1 </td>
	                        <td> TENDER001 </td>
	                        <td> Spare Part </td>
	                        <td> 90 days </td>
	                        <td> 28/05/2018</td>
	                        <td> 
	                        	<a href="equote/single_quote" type="button" class="btn green btn-xs customactionbtn"><i class="fa fa-eye"></i> View</a>
	                        </td>
	                    </tr>
	                    <tr>
	                    	<td> 2 </td>
	                        <td> TENDER002 </td>
	                        <td> Spare Part1 </td>
	                        <td> 95 days </td>
	                        <td> 29/05/2018</td>
	                        <td> 
	                        	<a href="equote/single_quote" type="button" class="btn green btn-xs customactionbtn"><i class="fa fa-eye"></i> View</a>
	                        </td>
	                    </tr>
	                    <tr>
	                    	<td> 3 </td>
	                        <td> TENDER003 </td>
	                        <td> Spare Part3 </td>
	                        <td> 90 days </td>
	                        <td> 28/05/2018</td>
	                        <td> 
	                        	<a href="equote/single_quote" type="button" class="btn green btn-xs customactionbtn"><i class="fa fa-eye"></i> View</a>
	                        </td>
	                    </tr>
	                    <tr>
	                    	<td> 4 </td>
	                        <td> TENDER004 </td>
	                        <td> Spare Part4 </td>
	                        <td> 60 days </td>
	                        <td> 29/05/2018</td>
	                        <td> 
	                        	<a href="equote/single_quote" type="button" class="btn green btn-xs customactionbtn"><i class="fa fa-eye"></i> View</a>
	                        </td>
	                    </tr>
	                    <tr>
	                    	<td> 5 </td>
	                        <td> TENDER005 </td>
	                        <td> Spare Part5 </td>
	                        <td> 40 days </td>
	                        <td> 28/05/2018</td>
	                        <td> 
	                        	<a href="equote/single_quote" type="button" class="btn green btn-xs customactionbtn"><i class="fa fa-eye"></i> View</a>
	                        </td>
	                    </tr>
	                    <tr>
	                    	<td> 6 </td>
	                        <td> TENDER006 </td>
	                        <td> Spare Part6 </td>
	                        <td> 60 days </td>
	                        <td> 29/05/2018</td>
	                        <td> 
	                        	<a href="equote/single_quote" type="button" class="btn green btn-xs customactionbtn"><i class="fa fa-eye"></i> View</a>
	                        </td>
	                    </tr>
	                    <tr>
	                    	<td> 7 </td>
	                        <td> TENDER007 </td>
	                        <td> Spare Part7 </td>
	                        <td> 70 days </td>
	                        <td> 28/05/2018</td>
	                        <td> 
	                        	<a href="equote/single_quote" type="button" class="btn green btn-xs customactionbtn"><i class="fa fa-eye"></i> View</a>
	                        </td>
	                    </tr>
	                    <tr>
	                    	<td> 8 </td>
	                        <td> TENDER008 </td>
	                        <td> Spare Part8 </td>
	                        <td> 75 days </td>
	                        <td> 29/05/2018</td>
	                        <td> 
	                        	<a href="equote/single_quote" type="button" class="btn green btn-xs customactionbtn"><i class="fa fa-eye"></i> View</a>
	                        </td>
	                    </tr>
	                    <tr>
	                    	<td> 9 </td>
	                        <td> TENDER009 </td>
	                        <td> Spare Part9 </td>
	                        <td> 70 days </td>
	                        <td> 28/05/2018</td>
	                        <td> 
	                        	<a href="equote/single_quote" type="button" class="btn green btn-xs customactionbtn"><i class="fa fa-eye"></i> View</a>
	                        </td>
	                    </tr>
	                    <tr>
	                    	<td> 10 </td>
	                        <td> TENDER010 </td>
	                        <td> Spare Part10 </td>
	                        <td> 80 days </td>
	                        <td> 29/05/2018</td>
	                        <td> 
	                        	<a href="equote/single_quote" type="button" class="btn green btn-xs customactionbtn"><i class="fa fa-eye"></i> View</a>
	                        </td>
	                    </tr>
	                    
	                </tbody>
	            </table>
	        </div>
        </div>
    </div>
</div>
<?php 
error_reporting(0);
$navigationURL = reset(explode("?",end(explode("/",$_SERVER["REQUEST_URI"]))));
if (is_numeric($navigationURL)) {
    $navigationURL=$this->uri->segment(3);
}
?>
<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 0px">
            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
            <li class="sidebar-toggler-wrapper hide">
                <div class="sidebar-toggler">
                    <span></span>
                </div>
            </li>
            <li class="nav-item <?php echo ($navigationURL=='dashboard')?'active open':''; ?>">
                <a href="<?php echo base_url(); ?>supplier/dashboard" class="nav-link nav-toggle">
                    <i class="dashboard-image"></i>
                    <span class="title">Dashboard</span>
                    <span class="selected"></span>
                </a>
            </li>
            <?php $user_data=$this->session->userdata();
            if(!empty($user_data)) {
            if(strtolower($user_data['status'])=='active'){ ?>
            <li class="nav-item <?php echo ($navigationURL=='equote' || $navigationURL=='individual_quote' || $navigationURL=='preview_tender')?'active open':''; ?>">
                <a href="<?php echo base_url(); ?>supplier/equote" class="nav-link nav-toggle">
                    <i class="icon-tender"></i>
                    <span class="title">E Quote</span>
                    <span class="selected"></span>
                </a>
            </li>
            
            <li class="nav-item <?php echo ($navigationURL=='applied_tenders_list' || $navigationURL=='view_tender_application' || $navigationURL=='change_tender')?'active open':''; ?>">
                <a href="<?php echo base_url(); ?>supplier/equote/applied_tenders_list" class="nav-link nav-toggle">
                    <i class="icon-applied"></i>
                    <span class="title">Applied  E Quote</span>
                    <span class="selected"></span>
                </a>
            </li>
            <li class="nav-item <?php echo ($navigationURL=='bidding_applied_tenders_list'|| $navigationURL=='change_tender_price' || $navigationURL=='previewbidding')?'active open':''; ?>">
                <a href="<?php echo base_url(); ?>supplier/bidding/bidding_applied_tenders_list" class="nav-link nav-toggle">
                    <i class="icon-bidding"></i>
                    <span class="title">Bidding</span>
                    <span class="selected"></span>
                </a>
            </li>
            <li class="nav-item <?php echo ($navigationURL=='award')?'active open':''; ?>">
                <a href="<?php echo base_url(); ?>supplier/award" class="nav-link nav-toggle">
                    <i class="icon-award"></i>
                    <span class="title">Award  E Quote</span>
                    <span class="selected"></span>
                </a>
            </li>
            <li class="nav-item <?php echo ($navigationURL=='profile')?'active open':''; ?>">
                <a href="<?php echo base_url(); ?>supplier/profile" class="nav-link nav-toggle">
                    <i class="icon-setting"></i>
                    <span class="title">Profile Settings</span>
                    <span class="selected"></span>
                </a>
            </li>
            <?php }} ?>
        </ul>
    </div>
</div>
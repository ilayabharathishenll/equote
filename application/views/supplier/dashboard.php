<?php
error_reporting(0);
$user_data=$this->session->userdata();
$document='';
foreach ($dashboarCount as $dashboardData) {
   $latestequote = $dashboarCount["latestequote"];
   $data=array("supplierbadgecount"=>$latestequote);
   $this->session->set_userdata($data);
   $appliedquote = $dashboarCount["noofappliedquote"];
   $awardedquote = $dashboarCount["awardedquote"];
   $biddingquote = $dashboarCount["biddingquote"];
   if(!empty($dashboarCount["vendor"])){
    $document=$dashboarCount["vendor"][0]->supplier_doc;
   }
}
$interval='';$today=date('Y-m-d H:i:s');
function datetimeDiff($dt1, $dt2){
        $t1 = strtotime($dt1);
        $t2 = strtotime($dt2);
        $dtd = new stdClass();
        $dtd->interval = $t2 - $t1;
        $dtd->total_sec = abs($t2-$t1);
        $dtd->total_min = floor($dtd->total_sec/60);
        $dtd->total_hour = floor($dtd->total_min/60);
        $dtd->total_day = floor($dtd->total_hour/24);

        $dtd->day = $dtd->total_day;
        $dtd->hour = $dtd->total_hour -($dtd->total_day*24);
        $dtd->min = $dtd->total_min -($dtd->total_hour*60);
        $dtd->sec = $dtd->total_sec -($dtd->total_min*60);
        return $dtd;
    }
?>
<!-- BEGIN CONTENT BODY -->
<style>
.feeds li .col2 {
  float: left;
  width: 152px !important;
  margin-left: -179px !important;
}
</style>
<div class="page-content"  id="dashboard">
	<div class="row widget-row">
        <?php
        $msg=$this->session->flashdata('success');
        if(!empty($msg)){
        ?>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="alert alert-success alert-dismissible">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <?php echo $msg ?>
            </div>
        </div>
        <?php
        }
        //print_r($user_data);exit;
        if($user_data['status']=='Active'){
        ?>
        <div class="col-md-3">
            <!-- BEGIN WIDGET THUMB -->
            <a href="<?php echo base_url()?>supplier/equote" style="text-decoration:none;">
            <div class="widget-thumb dashboard-stat red text-uppercase margin-bottom-20 bordered">
                <h4 class="widget-thumb-heading">Latest E Quote</h4>
                <div class="widget-thumb-wrap">
                    <i class="widget-thumb-icon bg-white icon-social-dropbox redicon dashboardborder"></i>
                    <div class="widget-thumb-body">
                        <!-- <span class="widget-thumb-subtitle">USD</span> -->
                        <span class="widget-thumb-body-stat" data-counter="counterup" data-value="<?php echo $latestequote ?>"><?php echo $latestequote ?></span>
                    </div>
                </div>
            </div>
            </a>
            <!-- END WIDGET THUMB -->
        </div>
        <div class="col-md-3">
            <!-- BEGIN WIDGET THUMB -->
            <a href="<?php echo base_url()?>supplier/equote/applied_tenders_list" style="text-decoration:none;">
            <div class="widget-thumb dashboard-stat bg-green-jungle text-uppercase margin-bottom-20 bordered">
                <h4 class="widget-thumb-heading">No Of Applied E Quote</h4>
                <div class="widget-thumb-wrap">
                    <i class="widget-thumb-icon bg-white icon-layers font-green-jungle dashboardborder"></i>
                    <div class="widget-thumb-body">
                        <span class="widget-thumb-body-stat" data-counter="counterup" data-value="<?php echo $appliedquote ?>"><?php echo $appliedquote ?></span>
                    </div>
                </div>
            </div>
            </a>
            <!-- END WIDGET THUMB -->
        </div>
        <div class="col-md-3">
            <!-- BEGIN WIDGET THUMB -->
            <a href="<?php echo base_url()?>supplier/award" style="text-decoration:none;">
            <div class="widget-thumb dashboard-stat  yellow-casablanca text-uppercase margin-bottom-20 bordered">
                <h4 class="widget-thumb-heading">No Of Awards E Quote</h4>
                <div class="widget-thumb-wrap">
                    <i class="widget-thumb-icon bg-white icon-badge dashboardborder font-yellow-casablanca"></i>
                    <div class="widget-thumb-body">
                        <!-- <span class="widget-thumb-subtitle">USD</span> -->
                        <span class="widget-thumb-body-stat" data-counter="counterup" data-value="<?php echo $awardedquote;?>"><?php echo $awardedquote;?></span>
                    </div>
                </div>
            </div>
            </a>
            <!-- END WIDGET THUMB -->
        </div>
        <div class="col-md-3">
            <!-- BEGIN WIDGET THUMB -->
            <a href="<?php echo base_url()?>supplier/bidding/bidding_applied_tenders_list" style="text-decoration:none;">
            <div class="widget-thumb dashboard-stat purple-intense  text-uppercase margin-bottom-20 bordered">
                <h4 class="widget-thumb-heading">Bidding</h4>
                <div class="widget-thumb-wrap">
                    <i class="widget-thumb-icon bg-white fa fa-gavel dashboardborder font-purple-intense "></i>
                    <div class="widget-thumb-body">
                       <!--  <span class="widget-thumb-subtitle">USD</span> -->
                        <span class="widget-thumb-body-stat" data-counter="counterup" data-value=" <?php echo $biddingquote?>"><?php echo $biddingquote?></span>
                    </div>
                </div>
            </div>
            </a>
            <!-- END WIDGET THUMB -->
        </div>
        <?php } ?>
        <div class="col-md-3">
            <!-- BEGIN WIDGET THUMB -->
            <div class="widget-thumb dashboard-stat red-soft text-uppercase margin-bottom-20 bordered">
                <h4 class="widget-thumb-heading">Supplier Document</h4>
                <div class="widget-thumb-wrap">
                    <i class="widget-thumb-icon red-soft"></i>
                    <div class="widget-thumb-body">
                        <?php if(!empty($document)){?>
                            <span class="widget-thumb-body-stat"><a href="<?php echo base_url('supplier/dashboard/download').'/'.$document;?>"><i class="fa fa-download" style="color:white;"></i></a></span>
                        <?php }else{ ?>
                        <span class="widget-thumb-body-stat"><a href="<?php echo base_url('supplier/dashboard/download')."/E-quote_supplier_document.xls"?>"><i class="fa fa-download" style="color:white;"></i></a></span>
                    <?php } ?>
                        <!-- <span class="widget-thumb-subtitle">USD</span> -->
                        <!-- <span class="widget-thumb-body-stat" data-counter="counterup" data-value="08">08</span> -->
                    </div>
                </div>
            </div>
            <!-- END WIDGET THUMB -->
        </div>
       
        <div class="col-md-3">
            <!-- BEGIN WIDGET THUMB -->
            <div class="widget-thumb dashboard-stat blue-hoki text-uppercase margin-bottom-20 bordered">
                <h4 class="widget-thumb-heading">Supplier Upload Document</h4>
                <form name="frm_dashboard" id="frm_dashboard" action="<?php echo base_url('/supplier/dashboard');?>" method="POST"  enctype="multipart/form-data">
                    <div class="widget-thumb-wrap">
                        <i class="widget-thumb-icon blue-hoki"></i>
                        <div class="widget-thumb-body" style="float:left;margin-top: -55px;">
                            <input type="file" name="upload_file" style="float:left;">
                            <button type="submit" class="btn green btn-xs customactionbtn" style="background-color: #4CAF50 !important;border-color: #4CAF50 !important;border-radius: 3px !important;font-family: Roboto-Regular;margin-top: 8px;"><i class="fa fa-upload"></i> Upload</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- END WIDGET THUMB -->
        </div>
    </div>
    <?php if($user_data['status']=='Active'){
        ?>
    <div class="row" id="recent">
        <div class="col-lg-12 col-xs-12 col-sm-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-share font-dark hide"></i>
                        <span class="caption-subject font-dark bold uppercase recent-activity">Recent Activities</span>
                    </div>             
                </div>
                <div class="portlet-body">
                    <div class="scroller" style="height: 300px;" data-always-visible="1" data-rail-visible="0">
                        <ul class="feeds">
                            <?php
                            if(!empty($dashboarCount["recent_tender"])){
                                //print_r($dashboarCount["recent_tender"]);
                                foreach($dashboarCount["recent_tender"] as $tender){ ?>
                            <li>
                                <?php
                                if ($tender->current_status !='Awarded') {
                                ?>
                                <a href="<?php echo base_url()?>supplier/equote">
                                    
                                    <div class="col1">
                                        <div class="cont">
                                            <div class="cont-col1">
                                                <div class="label label-sm label-success">
                                                    <i class="icon-envelope"></i>
                                                </div>
                                            </div>
                                            <div class="cont-col2">
                                                <div class="desc">
                                                    <?php

                                                        if($tender->current_status=='Add'){
                                                            echo "New E Quote ".$tender->tender_title." Posted";
                                                        }elseif($tender->current_status=='Delete'){
                                                            echo "Deleted E Quote ".$tender->tender_title." Successfully";
                                                        }elseif($tender->current_status=='Update'){
                                                             echo "Update E Quote ".$tender->tender_title." Successfully";
                                                        }
                                                        elseif($tender->current_status=='Cancelled'){
                                                             echo "Cancelled E Quote ".$tender->tender_title." Successfully";
                                                        }
                                                    ?>
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                    <div class="col2">
                                        <div class="date">
                                            <?php 
                                            $interval=datetimeDiff($today,$tender->modified_on);
                                            //print_r($interval);
                                            if(!empty($interval->day)){
                                                echo $interval->day."Day ago";
                                            }elseif(empty($interval->day) && !empty($interval->hour) && !empty($interval->min)){
                                                echo $interval->hour."Hour ".$interval->min."min ago";
                                            }elseif(empty($interval->day) && empty($interval->hour) && !empty($interval->min)){
                                                echo $interval->min."min ago";
                                            }elseif(empty($interval->day) && empty($interval->hour) && empty($interval->min) && !empty($interval->sec) ){
                                                echo "Just ".$interval->sec." sec ago";
                                            }
                                            ?>
                                        </div>
                                    </div>

                                </a>
                                <?php
                                    }
                                ?>
                            </li>
                            <?php }} if(!empty($dashboarCount["recent_award"])){
                                foreach($dashboarCount["recent_award"] as $award){ ?>
                            <li>
                                <a href="<?php echo base_url()?>supplier/award">
                                <div class="col1">
                                    <div class="cont">
                                        <div class="cont-col1">
                                            <div class="label label-sm bg-yellow-casablanca">
                                                <i class="icon-badge"></i>
                                            </div>
                                        </div>
                                        <div class="cont-col2">
                                            <div class="desc"> 
                                                <?php 
                                                  echo "Award Added This ".$award->tender_title." E Quote";
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col2">
                                    <div class="date">
                                        <?php 
                                            $interval=datetimeDiff($today,$award->modified_on);
                                            //print_r($interval);
                                            if(!empty($interval->day)){
                                                echo $interval->day."Day ago";
                                            }elseif(empty($interval->day) && !empty($interval->hour) && !empty($interval->min)){
                                                echo $interval->hour."Hour ".$interval->min."min ago";
                                            }elseif(empty($interval->day) && empty($interval->hour) && !empty($interval->min)){
                                                echo $interval->min."min ago";
                                            }elseif(empty($interval->day) && empty($interval->hour) && empty($interval->min) && !empty($interval->sec) ){
                                                echo "Just ".$interval->sec." sec ago";
                                            }
                                            ?>
                                    </div>
                                </div>
                               </a>
                            </li>
                        <?php }} ?>
                        </ul>
                    </div>
                    <div class="scroller-footer">
                        <div class="btn-arrow-link pull-right">
                           <!--  <a href="javascript:;">See All Records</a>
                                <i class="icon-arrow-right"></i> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>
</div>
<?php 
function icon($value){
    $outputs=explode(".",$value);
    $icons=$outputs[1];
    if($icons=='jpg' || $icons=='jpeg' || $icons=='png' || $icons=='gif' || $icons=='svg'){
        return "image";
    }elseif($icons=='doc' || $icons=='docx'){
         return "word";
    }elseif($icons=='xls' || $icons=='xlsx'){
         return "excel";    
    }elseif($icons=='PDF'){
         return "pdf";  
    }elseif($icons=='ppt' || $icons=='pptx'){
         return "powerpoint";  
    }
}
foreach($data['tender_list'] as $tender){ ?>
<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/tenders.png" class="imgbasline"> View E Quote Details</div>
            <div class="actions">
                <a href="tender_list" class="btn red btn-sm customrestbtn">
                    <i class="fa fa-angle-left"></i> Back</a>
            </div>
        </div>
        
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <form name="frm_rfq" id="frm_rfq" action="tender_list" class="ind-quote horizontal-form" method="POST">
                <div class="form-body">
                    <h3 class="form-section formheading">E Quote Basic Detail</h3>
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">RFQ Number</label>
                                <div class="col-md-8">
                                    <?php echo !empty($tender->ref_no)?$tender->ref_no:"--"; ?>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Part Name</label>
                                <div class="col-md-8">
                                    <?php echo !empty($tender->part_name)?$tender->part_name:"--"; ?>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->

                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Delivery Duration</label>
                                <div class="col-md-8">
                                    <?php echo !empty($tender->delivery_duration)?$tender->delivery_duration:"--"; ?>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Delivery Date</label>
                                <div class="col-md-8">
                                    <?php echo !empty($tender->delivery_date)?date('d-m-Y',strtotime($tender->delivery_date)):"--"; ?>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->

                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Quantity</label>
                                <div class="col-md-8">
                                    <?php echo !empty($tender->quantity)?$tender->quantity:"--"; ?>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Part Price</label>
                                <div class="col-md-8">
                                    <?php echo !empty($tender->part_price)?$tender->part_price:"--"; ?>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->

                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">E Quote Title</label>
                                <div class="col-md-8">
                                   <?php echo !empty($tender->tender_title)?$tender->tender_title:"--"; ?>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Upload drawing & Technical Spesc</label>
                                <div class="col-md-8">
                                 <?php if(!empty($tender->drawing_doc)){?> <a class="download_doc" href="<?php echo base_url('admin/tender/download');?>/<?php echo !empty($tender->drawing_doc)?$tender->drawing_doc:"#"; ?>"><img src="<?php echo base_url()."assets/layouts/layout/img/dow.png" ?>" style="margin-bottom: 10px;">Click here to download</a>
                                 <?php }else{ ?>
                                    --
                                 <?php } ?>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->

                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Norm</label>
                                <div class="col-md-8">
                                    <?php echo !empty($tender->norm)?$tender->norm:"--"; ?>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">RM Price</label>
                                <div class="col-md-8">
                                     <?php echo !empty($tender->rm_price)?$tender->rm_price:"--"; ?>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-12 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-12">E Quote Details</label>
                                    <div class="col-md-12">
                                     <?php echo !empty($tender->tender_detail)?$tender->tender_detail:"--"; ?>
                                    </div>
                            </div>
                            
                        </div>
                    </div>
                    <!--/row-->

                    <h3 class="form-section formheading">Upload E Quote Documents</h3>
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-5" style="padding-right: 0px;">Tender Terms and Condition</label>
                                
                                <div class="col-md-7"><?php foreach($data['tender_document'] as $documents){ 
                                    if($documents->t_doc_type=='upload_terms'){ ?>
                                     <a class="download_doc" href="<?php echo base_url('admin/tender/download');?>/<?php echo !empty($documents->t_doc_name)?$documents->t_doc_name:"#"; ?>"><img src="<?php echo base_url()."assets/layouts/layout/img/dow.png" ?>" style="margin-bottom: 10px;">Click here to download</a><?php } } ?>
                                </div>
                                <!-- <div class="col-md-2">
                                </div> -->
                                
                            </div>
                            <div id="terms">
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-5">Help Document</label>
                                
                                <div class="col-md-7"><?php foreach($data['tender_document'] as $documents){ 
                                    if($documents->t_doc_type=='upload_helpdoc'){ ?>
                                     <a class="download_doc" style="padding-right: 0;" href="<?php echo base_url('admin/tender/download');?>/<?php echo !empty($documents->t_doc_name)?$documents->t_doc_name:"#"; ?>"><img src="<?php echo base_url()."assets/layouts/layout/img/dow.png" ?>" style="margin-bottom: 10px;">Click here to download</a><?php } } ?>
                                </div>
                                <!-- <div class="col-md-2">
                                </div> -->
                                
                            </div>
                            <div id="helpdoc">
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-5">E Quote Detail</label>
                                
                                <div class="col-md-7"><?php foreach($data['tender_document'] as $documents){ 
                                    if($documents->t_doc_type=='upload_tenderdetail'){ ?>
                                     <a class="download_doc" style="padding-right: 5px;" href="<?php echo base_url('admin/tender/download');?>/<?php echo !empty($documents->t_doc_name)?$documents->t_doc_name:"#"; ?>"><img src="<?php echo base_url()."assets/layouts/layout/img/dow.png" ?>" style="margin-bottom: 10px;">Click here to download</a><?php } } ?>
                                </div>
                                <!-- <div class="col-md-2">
                                </div> -->
                                
                            </div>
                            <div id="detaildoc">
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-5" style="padding-right: 0px;">E Quote Documents Required</label>
                                
                                <div class="col-md-7"><?php foreach($data['tender_document'] as $documents){ 
                                    if($documents->t_doc_type=='upload_tenderdoc'){ ?>
                                     <a class="download_doc" href="<?php echo base_url('admin/tender/download');?>/<?php echo !empty($documents->t_doc_name)?$documents->t_doc_name:"#"; ?>"><img src="<?php echo base_url()."assets/layouts/layout/img/dow.png" ?>" style="margin-bottom: 10px;">Click here to download</a><?php } } ?>
                                </div>
                                <!-- <div class="col-md-2">
                                </div> -->
                                
                            </div>
                            <div id="tenderdoc">
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-5">E Quote Document</label>
                                
                                <div class="col-md-7"><?php foreach($data['tender_document'] as $documents){ 
                                    if($documents->t_doc_type=='tender_docone'){ ?>
                                     <a class="download_doc" href="<?php echo base_url('admin/tender/download');?>/<?php echo !empty($documents->t_doc_name)?$documents->t_doc_name:"#"; ?>"><img src="<?php echo base_url()."assets/layouts/layout/img/dow.png" ?>" style="margin-bottom: 10px;">Click here to download</a><?php } } ?>
                                </div>
                               <!--  <div class="col-md-2">
                                </div> -->
                                
                            </div>
                            <div id="tenderdocone">
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-5">E Quote Document</label>
                                
                                <div class="col-md-7"><?php foreach($data['tender_document'] as $documents){ 
                                    if($documents->t_doc_type=='tender_doctwo'){ ?>
                                     <a class="download_doc" style="padding-right: 5px;" href="<?php echo base_url('admin/tender/download');?>/<?php echo !empty($documents->t_doc_name)?$documents->t_doc_name:"#"; ?>"><img src="<?php echo base_url()."assets/layouts/layout/img/dow.png" ?>" style="margin-bottom: 10px;"> Click here to download</a> <?php } } ?>
                                </div>
                                <!-- <div class="col-md-2">
                                </div> -->
                               
                            </div>
                             <div id="tenderdoctwo">
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->

                    <h3 class="form-section formheading">E Quote Date Detail</h3>
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Start Date & Time</label>
                                <div class="col-md-8">
                                     <?php echo !empty($tender->start_date)?date('d-m-Y',strtotime($tender->start_date)):"--"; ?> <?php echo !empty($tender->start_time)?date('H:i A',strtotime($tender->start_time)):"--"; ?>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">End Date & Time</label>
                                <div class="col-md-8">
                                     <?php echo !empty($tender->end_date)?date('d-m-Y',strtotime($tender->end_date)):"--"; ?> <?php echo !empty($tender->end_time)?date('H:i A',strtotime($tender->end_time)):"--"; ?>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                </div>
                <?php } ?>
            </form>
            <!-- END FORM-->
        </div>
        <style>
            .fa-file-pdf-o{
                color:#D11408;
            }
            .fa-file-excel-o{
                color:#1C7D3A;
            }
            .fa-file-image-o{
                color:#000000;
            }
            .fa-file-word-o{
                color:#295494;
            }
            .fa-file-powerpoint-o{
                color:#D64727;
            }
        </style>
    </div>
</div>
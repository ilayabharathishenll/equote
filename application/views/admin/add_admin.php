<?php
error_reporting(0);
if(count($getadmin)>0) {
	$admin_name=$getadmin["admin_name"];
	$admin_email=$getadmin["admin_email"];
	$admin_password=$getadmin["admin_password"];
	$admin_phone=$getadmin["admin_phone"];
	$admin_altphone=$getadmin["admin_altphone"];
} else {
	$admin_name="";
	$admin_email="";
	$admin_password="";
	$admin_phone="";
	$admin_altphone="";
}
?>
<div class="page-content">
	<div class="portlet box blue boardergrey">
		<div class="portlet-title">
			<div class="caption">
				<img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/user.png" class="imgbasline"> Add Admin
			</div>
		</div>
		<div class="portlet-body form">
			<!-- BEGIN FORM-->
			<form name="frm_addadmin" id="frm_addadmin" action="add_admin" class="horizontal-form" method="POST">
				<input type="hidden" name="hndid" value="<?php echo $getadmin["admin_id"] ?>">
				<div class="form-body">
					<div class="row">
						<div class="col-md-6 paddingbottom">
							<div class="form-group">
								<label class="control-label col-md-4">Name  <span class="redstar">*</span></label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="name" id="name" placeholder="" value="<?php echo $admin_name ?>">
								</div>
							</div>
						</div>
						<!--/span-->
						<div class="col-md-6 paddingbottom">
							<div class="form-group">
								<label class="control-label col-md-4">Email  <span class="redstar">*</span></label>
								<div class="col-md-8">
									<input type="email" class="form-control" name="email" id="email" placeholder="" value="<?php echo $admin_email ?>">
								</div>
							</div>
						</div>
						<!--/span-->
					</div>
					<!--/row-->
					<div class="row">
						<div class="col-md-6 paddingbottom">
							<div class="form-group">
								<label class="control-label col-md-4">Password  <span class="redstar">*</span></label>
								<div class="col-md-8">
									<input type="password" class="form-control" name="password" id="password" placeholder="" value="<?php echo $admin_password ?>">
								</div>
							</div>
						</div>
						<!--/span-->
						<div class="col-md-6 paddingbottom">
							<div class="form-group">
								<label class="control-label col-md-4">Confirm Password  <span class="redstar">*</span></label>
								<div class="col-md-8">
									<input type="Password" class="form-control" name="conpassword" id="conpassword" placeholder="" value="<?php echo $admin_password ?>">
								</div>
							</div>
						</div>
						<!--/span-->
					</div>
					<!--/row-->
					<div class="row">
						<div class="col-md-6 paddingbottom">
							<div class="form-group">
								<label class="control-label col-md-4">Phone  <span class="redstar">*</span></label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="phone" id="phone" placeholder="" value="<?php echo $admin_phone ?>" minlength="10" maxlength="10">
								</div>
							</div>
						</div>
						<!--/span-->
						<div class="col-md-6 paddingbottom">
							<div class="form-group">
								<label class="control-label col-md-4">Alternate Phone No  <span class="redstar">*</span></label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="alt_phone" id="alt_phone" placeholder="" value="<?php echo $admin_altphone?>" minlength="10" maxlength="10">
								</div>
							</div>
						</div>
						<!--/span-->
					</div>
					<!--/row-->
				</div>
				<div class="form-actions formbtncenter">
					<button type="submit" class="btn green customsavebtn">
						<i class="fa fa-check"></i> Save Data
					</button>
					<a href="admin_list" type="button" class="btn red customrestbtn backbtn"> <i class="fa fa-angle-left"></i> Back</a>
				</div>
			</form>
			<!-- END FORM-->
		</div>
	</div>
</div>
<script type="text/javascript">
	jQuery(document).ready(function(){
		$("#frm_addadmin").validate({
			rules: {
				name:"required",
				email:{required:true,email:true,unique:true},
				password:{
					required: true
				},
				conpassword:{required: true,equalTo: "#password"},
				phone:{required:true,number:true},
				alt_phone:{required:true,number:true},
			},
			messages: {
				name:"Please enter name",
				email:{required:"Please enter email",email:"Please enter vaild email",unique:"Email already exists"},
				password:{required:"Please enter password"},
				conpassword:
				{
					required:"Please enter confirm password",
					equalTo: "Your password and confirmation password do not match"
				},
				
				phone:{required:"Please enter phone number",number:"Please enter valid phone number"},
				alt_phone:{required:"Please enter phone number",number:"Please enter valid phone number"},
			},
		});
	});
	$.validator.addMethod('unique',function(value)
		{
			var email=$("#email").val();
			var adminid=$('input[name="hndid"]').val();
			var result=$.ajax({
						type:"POST",
						async:false,
						url:base_url+"admin/settings/admin_email_exists",
						data:{"email":email,"adminid":adminid},
						dataType:"text"});
		 if(result.responseText=="not-exists"){
			 return true;
		 }
		 else{ 
			return false;
		 }
	},"Email already already exists!");
</script>
<script>
	$(document).ready(function(){
		$(".customrestbtn").click(function(){
	$("#frm_addadmin").trigger("reset");
});});
</script>
<!-- END CONTENT BODY -->
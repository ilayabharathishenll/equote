<?php
	$search_rfq = $search_tender = $search_part = $search_partno= '';
	if (!empty($search)) {
		$search_rfq = $search['rfq_no'];
		$search_tender = $search['tender_title'];
		$search_part = $search['part_name'];
		$search_partno = $search['part_no'];
	}
?>
<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="<?php echo base_url(); ?>/assets/layouts/layout/img/de-active/invitation.png" class="imgbasline"> E Quote Invitation
            </div>
        </div>
        <div class="portlet-body">
            <form name="frm_invtender" id="frm_invtender" method="POST">
		        <div class="row">
		        	<div class="col-md-12 paddingleftright">
		        		<div class="col-md-2 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                           <input type="text" class="form-control" name="invite[rfq_no]" id="rfq_no" placeholder="RFQ No" value="<?php echo $search_rfq ?>">
	                        </div>
		        		</div>
		        		<div class="col-md-2 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                            <input type="text" class="form-control" name="invite[tender_title]" id="tender_title" placeholder="E Quote Name" value="<?php echo $search_tender ?>">
	                        </div>
		        		</div>
		        		<div class="col-md-2 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                           <input type="text" class="form-control" name="invite[part_name]" id="part_name" placeholder="Part Name" value="<?php echo $search_part ?>">
	                        </div>
		        		</div>
		        		<div class="col-md-2 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                           <input type="text" class="form-control" name="invite[part_no]" id="part_no" placeholder="Part Number" value="<?php echo $search_partno ?>">
	                        </div>
		        		</div>
		        		<div class="col-md-3">
		        			<div class="col-md-12 paddingleftright">
		        				<button type="submit" class="btn btn-warning customsearchtbtn"> <i class="fa fa-search"></i> Search</button>
		        				<a href="<?php echo base_url()."admin/invite/tender_invite";?>" type="button" class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Reset</a>
		        			</div>
		        		</div>
		        	</div>
		        </div>
	        	<div class="table-responsive" style="overflow-x: inherit;margin-top:0px;">
		            <table class="table table-striped table-bordered table-hover admintbl" id="tender-invite">
		            	<thead>
		                    <tr>
		                    	<th> SI.NO </th>
		                        <th> RFQ NO </th>
		                        <th> E Quote Name</th>
		                        <th> Part Name</th>
		                        <th> Part Number</th>
		                        <th> E Quote Base Price</th>
		                        <th> Action </th>
		                    </tr>
		                </thead>
		                </tbody>
		                    <?php
		                       $sno=1;
		                       foreach ($getTenderInvList as $singleTender) {
		                    ?>
		                    <tr>
		                    	<td><?php echo $sno; ?></td>
		                        <td><?php echo $singleTender->ref_no;?></td>
		                        <td><?php echo $singleTender->tender_title;?></td>
		                        <td><?php echo $singleTender->part_name;?></td>
		                         <td><?php echo $singleTender->part_no;?></td>
		                        <td><?php echo $singleTender->base_price;?></td>
		                        <td> 
		                        	<a href="viewtender/<?php echo $singleTender->tender_id;?>" type="button" class="btn green btn-xs customactionbtn"><i class="fa fa-eye"></i> View</a> 
		                        	<a href="send_invite/<?php echo $singleTender->tender_id;?>" type="button" class="btn grey-cascade btn-xs custominvitebtn"> <i class="fa fa-envelope-o"></i> Invite</a>
		                        </td>
		                    </tr>
		                    <?php
		                    	$sno++;
		                    }
		                    ?>
		                </tbody>
		            </table>
		        </div>
		    </form>
        </div>
    </div>
</div>
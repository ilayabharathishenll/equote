<!-- BEGIN CONTENT BODY -->
<?php
$rfq= $Arrterms= $Arrhelpdoc= $Arrtenderdetail= $Arrtenderdoc= $Arrdocone= $Arrdoctwo=$terms_count=$helpdoc_count=$tenderdetail_count=$tenderdoc_count=$docone_count=$doctwo_count="";
foreach($data['tender_document'] as $key=>$documents){ 
    if ($documents->t_doc_type=='upload_terms') { 
        $Arrterms[] =$documents->t_doc_name;
    }
    if ($documents->t_doc_type=='upload_helpdoc') { 
       $Arrhelpdoc[] =$documents->t_doc_name;
    }
    if ($documents->t_doc_type=='upload_tenderdetail') { 
        $Arrtenderdetail[] =$documents->t_doc_name;
    }
    if ($documents->t_doc_type=='upload_tenderdoc') { 
        $Arrtenderdoc[] =$documents->t_doc_name;
    }
    if ($documents->t_doc_type=='tender_docone') { 
        $Arrdocone[] =$documents->t_doc_name;

    }
    if ($documents->t_doc_type=='tender_doctwo') { 
        $Arrdoctwo[] =$documents->t_doc_name;
    }
}
$terms_count = count($Arrterms);
$helpdoc_count = count($Arrhelpdoc);
$tenderdetail_count = count($Arrtenderdetail);
$tenderdoc_count = count($Arrtenderdoc);
$docone_count = count($Arrdocone);
$doctwo_count = count($Arrdoctwo);
foreach($data['tender_list'] as $value) { ?>
<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/tenders.png" class="imgbasline"> Edit RFQ Details
            </div>
        </div>
        <div class="portlet-body form">
            <?php //echo form_open_multipart('admin/tender/upload_file'); ?>
            <!-- BEGIN FORM-->
            <form name="frm_rfq" id="frm_rfq" class="horizontal-form" method="POST" enctype="multipart/form-data">
                <div class="form-body">
                    <h3 class="form-section formheading">Admin Posting E Quote Detail</h3>
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Part No  <span class="redstar"></span></label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="part_no" id="part_no" placeholder="" value="<?php echo $value->part_no; ?>" readonly>
                                   
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Part Price  <span class="redstar"></span></label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="part_price" id="part_price" placeholder="" value="<?php echo $value->part_price; ?>" readonly>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>

                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Category  <span class="redstar"></span></label>
                                <div class="col-md-8">
                                    <select id="selcate" name="selcate" class="form-control" disabled>
                                      <option value="">Select Category</option>
                                      <?php
                                      foreach($data["category"] as $categoryData){
                                        $selected="";                                        
                                        if($categoryData->category_id==$value->commodity_id){
                                            $selected="selected";
                                        }
                                        echo "<option value=".$categoryData->category_id." ".$selected.">".$categoryData->category_name."-".$categoryData->cat_number."</option>";

                                      }
                                      ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Drawing Issues No</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="drawingno" id="drawingno" placeholder="" value="<?php echo $value->drawing_no; ?>" readonly>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <div class="row">
                        <div class="col-md-12 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Supplier can change the price details after the tender applied with in particular time of period</label>
                                <div class="col-md-8">
                                    <div class="radio-list">
                                        <label class="radio-inline">
                                            <input type="radio" name="period" class="checkempty" value="1"  <?php if($value->price_edit=="Yes"){echo "checked";} else{ echo " ";};?> disabled> Yes 
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="period" class="checkempty" value="2" <?php if($value->price_edit=="No"){echo "checked";} else{ echo " ";};?> disabled> No 
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <h3 class="form-section formheading">E Quote Basic Detail</h3>
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">RFQ Number  <span class="redstar"></span></label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="rfq_no" id="rfq_no" placeholder="" value="<?php echo $value->ref_no; ?>" readonly>
                                   
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Part Name  <span class="redstar"></span></label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="part_name" id="part_name" value="<?php echo !empty($value->part_name)?$value->part_name:"";?>" readonly>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->

                    

                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Quantity <span class="redstar"></span></label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="sel_quantity" id="sel_quantity" value="<?php echo !empty($value->quantity)?$value->quantity:"0";?>" readonly> 
                                   
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">E Quote Base Price  <span class="redstar">*</span></label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="base_price" id="base_price" placeholder="" value="<?php echo $value->base_price ?>" readonly>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->

                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">E Quote Title  <span class="redstar">*</span></label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="tender_title" id="tender_title" placeholder=""  value="<?php echo $value->tender_title ?>" readonly>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Upload drawing & Technical Spes</label>
                                <div class="col-md-8">
                                    <input type="file" name="upload_drawing" id="upload_drawing" value="<?php echo $value->drawing_doc ?>">
                                    <i>(Note:Upload Only for .pdf, .jpeg, .png)</i>
                                    <?php if(!empty($value->drawing_doc)){?>
                                    <div class="col-md-10" style="padding: 0;">
                                        <a style="display: inline-block;margin-top: 5px;font-size: 12px;text-decoration: none;" href="<?php echo base_url('admin/tender/download');?>/<?php echo !empty($value->drawing_doc)?$value->drawing_doc:"#"; ?>"><img src="<?php echo base_url()?>assets/layouts/layout/img/dow.png" style="width: 17px;">Click here to download</a>
                                    </div>
                                    <?php
                                    }?>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->

                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Norm  <span class="redstar"></span></label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="norm" id="norm" placeholder="" value="<?php echo $value->norm ?>">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">RM Price  <span class="redstar"></span></label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="rm_price" id="rm_price" placeholder="" value="<?php echo $value->rm_price ?>">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-12 paddingbottom">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <textarea class="form-control" name="tender_detail" id="tender_detail" rows="5" cols="50" placeholder="Enter the E Quote Details...."><?php echo $value->tender_detail?></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/row-->

                    <h3 class="form-section formheading">Upload E Quote Documents</h3>
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="col-md-10" style="padding-right: 0px;">
                                <span class="help-block pull-right">(Upload Multiple Document's)</span>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-5" style="padding-right: 0px;">E Quote Terms and Condition</label>
                                <div class="col-md-5">
                                    <input type="file" name="upload_terms[]" id="upload_terms">
                                </div>
                                <div class="col-md-2">
                                     <button type="button" class="btn btn-primary customaddmorebtn" name="addmore" id="addmore"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
                                </div>
                            </div>
                            <div id="terms">
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="col-md-10" style="padding-right: 0px;">
                                <span class="help-block pull-right">(Upload Multiple Document's)</span>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-5">Help Document</label>
                                <div class="col-md-5">
                                   
                                    <input type="file" name="upload_helpdoc[]" id="upload_helpdoc">
                                </div>
                                <div class="col-md-2">
                                     <button type="button" class="btn btn-primary customaddmorebtn" name="helpmore" id="helpmore"><span class="glyphicon glyphicon-plus" aria-hidden="true" ></span></button>
                                </div>
                            </div>
                            <div id="helpdoc">
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-5">E Quote Detail</label>
                                <div class="col-md-5">
                                    <input type="file" name="upload_tenderdetail[]" id="upload_tenderdetail">
                                </div>
                                <div class="col-md-2">
                                     <button type="button" class="btn btn-primary customaddmorebtn" name="detailmore" id="detailmore"><span class="glyphicon glyphicon-plus" aria-hidden="true" ></span></button>
                                </div>
                            </div>
                            <div id="detaildoc">
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-5" style="padding-right: 0px;">E Quote Documents Required</label>
                                <div class="col-md-5">

                                    <input type="file" name="upload_tenderdoc[]" id="upload_tenderdoc">
                                </div>
                                <div class="col-md-2">
                                     <button type="button" class="btn btn-primary customaddmorebtn" name="tenderdocmore" id="tenderdocmore"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
                                </div>
                            </div>
                            <div id="tenderdoc">
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-5">E Quote Document</label>
                                <div class="col-md-5">
                                    <input type="file"  name="tender_docone[]" id="tender_docone">
                                </div>
                                <div class="col-md-2">
                                     <button type="button" class="btn btn-primary customaddmorebtn" name="tenderdoconemore" id="tenderdoconemore"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
                                </div>
                            </div>
                            <div id="tenderdocone">
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-5">E Quote Document</label>
                                <div class="col-md-5">
                                    <input type="file" name="tender_doctwo[]" id="tender_doctwo">
                                </div>
                                <div class="col-md-2">
                                     <button type="button" class="btn btn-primary customaddmorebtn" name="tenderdoctwomore" id="tenderdoctwomore"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
                                </div>
                            </div>
                             <div id="tenderdoctwo">
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->

                    <h3 class="form-section formheading">E Quote Date Detail</h3>

                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Start Date & Time  <span class="redstar">*</span></label>
                                <div class="col-md-5">
                                    <input type="text" class="form-control" name="start_date" id="start_date" autocomplete="off" data-date-format="dd/mm/yyyy" data-date-start-date="+0d" value="<?php echo date("d/m/Y",strtotime($value->start_date)); ?>" disabled>
                                </div>
                                <div class="col-md-3">
                                    <input type="text" class="form-control timepicker timepicker-24" name="start_time" id="start_time" autocomplete="off" value="<?php echo date("H:i",strtotime($value->start_time)); ?>" disabled>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">End Date & Time  <span class="redstar">*</span></label>
                                <div class="col-md-5">
                                    <input type="text" class="form-control" name="end_date" id="end_date" autocomplete="off" data-date-format="dd/mm/yyyy" value="<?php echo date("d/m/Y",strtotime($value->end_date)); ?>" placeholder="DD/MM/YYYY" disabled>
                                </div>
                                <div class="col-md-3">
                                    <input type="text" class="form-control timepicker timepicker-24" name="end_time" id="end_time" autocomplete="off" value="<?php echo date("H:i",strtotime($value->end_time)); ?>" placeholder="DD/MM/YYYY" disabled>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Delivery Date  <span class="redstar">*</span></label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="delivery_date" id="delivery_date" autocomplete="off" data-date-format="dd/mm/yyyy" data-date-start-date="+0d" value="<?php echo date("d/m/Y",strtotime($value->delivery_date)); ?>" placeholder="DD/MM/YYYY" disabled>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Delivery Duration  <span class="redstar">*</span></label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="delivery_duration" id="delivery_duration" placeholder="Delivery Duration in Days" value="<?php echo $value->delivery_duration; ?>" readonly>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Notification setting  <span class="redstar">*</span></label>
                                <div class="col-md-8">
                                    <select class="form-control" name="notification_setting" disabled>
                                        <option value="1" <?php if ($value->notification_setting == 1 ) echo 'selected' ; ?>>All</option>
                                        <option value="2" <?php if ($value->notification_setting == 2 ) echo 'selected' ; ?>>Select Supplier</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-actions formbtncenter">
                    <button type="submit" name="submit" value="submit" class="btn green customsavebtn">
                        <i class="fa fa-check"></i> Update
                    </button>
                    <a href="<?php echo base_url()?>admin/tender/tender_list" class="btn red  customrestbtn" style="width:90px;">
                    <i class="fa fa-angle-left"></i> Back</a>
                </div>
            </form>
            <!-- END FORM-->
        </div>
    </div>
</div>
<?php } ?>

<script type="text/javascript">
  jQuery(document).ready(function(){
    $('#end_date').on('focusout',function(){
       // console.log($(this).val());
       // console.log($('#end_date').val());
    })
   $('#start_date').on('focusout',function(){
        //console.log($(this).val());
       // console.log($('#start_date').val());
    })
   $('#delivery_date').on('focusout',function(){
        console.log($(this).val());
        console.log($('#delivery_date').val());
    })
    jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-z," "]+$/i.test(value);
    }, "Please enter the alphabet letters only"); 
    $("#frm_rfq").validate({
        rules: {
            //part_no:{required:true,number:true},
            //part_name:{required:true,lettersonly: true},
            //part_price:{required:true,number: true},
            sel_quantity:{required:true,number: true},
            //drawingno:{required:true,number: true},
            delivery_duration:{required:true,number:true},
            delivery_date:{required:true},
            base_price:{required:true,number: true},
            tender_title:{required:true,lettersonly: true},
            //norm:{required:true},
            //rm_price:{required:true,number: true},
            tender_detail:{required:true},
            start_date: {required: true},
            start_time:{required: true},
            end_date:{required: true},
            end_time:{required: true},
            notification_setting:{required: true},
        },
        messages: {
            //part_no: {required: "Please enter the part number",number: "Please enter the numbers only"},
            //part_price: { required: "Please enter the part price",number: "Please enter the numbers only"},
            //part_name:{required:"Please enter the part name",lettersonly:"Please enter the alphabet letters only"},
            sel_quantity: {required: "Please enter the quantity",number: "Please enter the numbers only"},
            //drawingno: {required: "Please enter the drawing number",number: "Please enter the numbers only"},
            delivery_duration:{required:"Please enter the delivery duration",number:"Please enter the numbers only"},
            delivery_date:{required:"Please select delivery date"},
            base_price:{required:"Please enter the base price",number:"Please enter the numbers only"},
            //tender_title:{required:"Please enter the quantity",lettersonly: "Please enter the alphabet letters only"},
            //norm:{required:"Please enter the norm"},
            tender_detail:{required:"Please enter the tender details"},
            //rm_price:{required:"Please enter the rm price",number:"Please enter the numbers only"},
            start_date: {required: "Please select start date"},
            start_time:{required: "Please select the start time"},
            end_date:{required: "Please select end date"},
            end_time:{required: "Please select the end time"},
            notification_setting:{required:"Please select the notification"},
        },
    });
});
</script>
<script type="text/javascript">
    $('#start_time').timepicker({showMeridian: false,minuteStep: 1,
    });
    $('#end_time').timepicker({ showMeridian: false,minuteStep: 1,
    });

    $("#start_date").datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
    }).on('changeDate', function (selected) {
         $("#start_date").valid();
        //console.log($("#start_date").val());
        var startDate = new Date(selected.date.valueOf());
        $('#end_date').datepicker('setStartDate', startDate);
    }).on('clearDate', function (selected) {
        $('#end_date').datepicker('setStartDate', null);
    }).change(function () {
        $('#start_date').validate();
    });

    $("#end_date").datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true            
    }).on('changeDate', function (selected) {
        $("#end_date").valid();
        console.log($("#end_date").val());
        var endDate = new Date(selected.date.valueOf());
        $('#start_date').datepicker('setEndDate', endDate);
    }).on('clearDate', function (selected) {
        $('#start_date').datepicker('setEndDate', null);
    }).change(function () {
        $('#end_date').validate();
    });
    $("#delivery_date").datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true            
    }).on('changeDate', function (selected) {
        $("#delivery_date").valid();
        //console.log($("#delivery_date").val());
        var deliveryDate= $("#delivery_date").val();
        //alert(deliveryDate);
        if (deliveryDate!="") {
            $.ajax({
                url: base_url+'admin/tender/get_delivery',
                type: 'POST',
                data: {
                    'delivery_date': deliveryDate
                },
                success: function(response) {
                    var data=response.split("##^^##");
                    if (data[0] == 'success') {
                        $("#delivery_duration").val(data[1]);
                    }
                }
            });
        } else {
            return false;
        }
    }).change(function () {
        $('#delivery_date').validate();
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#resetEmpty').on('click',function(){
            $("#frms_rfq")[0].reset();
        });
    });
    //j=2;
    var termsArr=[];
    var baseurls='<?php echo base_url();?>';
    var terms=JSON.parse('<?php echo json_encode($Arrterms);?>');
    var termsArr = JSON.parse('<?php echo json_encode($terms_count); ?>');
    for (var i = 0;i<terms.length; i++) {
        var newfield = '<div class="col-md-12 col-sm-12 col-xs-12 margintop10 paddingleftright"><div class="form-group"><label class="control-label col-md-5" style="padding-right: 0px;"></label><div class="col-md-5" style="padding-right: 0px;"><a href="'+baseurls+'admin/tender/download/'+terms[i]+'" class="editdownload" ><img src="'+baseurls+'assets/layouts/layout/img/dow.png'+'">Click here to download</a><input type="hidden" name="upload_terms[]" id="upload_terms_'+i+'" value="'+terms[i]+'"></div><div class="col-md-2"><button type="button" class="btn btn-danger customaddmorebtn" name="remove" id="remove"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button></div></div></div>';
        $('#terms').append(newfield);
    };

    var helpArr=[];
    var help=JSON.parse('<?php echo json_encode($Arrhelpdoc);?>');
    var helpArr = JSON.parse('<?php echo json_encode($helpdoc_count); ?>');
    for (var j = 0;j<help.length; j++) {
        var helpdocnewfield = '<div class="col-md-12 col-sm-12 col-xs-12 margintop10 paddingleftright"><div class="form-group"><label class="control-label col-md-5" style="padding-right: 0px;"></label><div class="col-md-5" style="padding-right: 0px;"><a href="'+baseurls+'admin/tender/download/'+help[j]+'" class="editdownload" ><img src="'+baseurls+'assets/layouts/layout/img/dow.png'+'">Click here to download</a><input type="hidden" name="upload_helpdoc[]" id="upload_helpdoc_'+j+'" value="'+help[j]+'"></div><div class="col-md-2"><button type="button" class="btn btn-danger customaddmorebtn" name="remove" id="remove"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button></div></div></div>';
        $('#helpdoc').append(helpdocnewfield);
    };

    var tenderdetailArr=[];
    var tenderdetail=JSON.parse('<?php echo json_encode($Arrtenderdetail);?>');
    var tenderdetailArr = JSON.parse('<?php echo json_encode($tenderdetail_count); ?>');
    for (var k = 0;k<tenderdetail.length; k++) {
        var tenderdetailnewfield = '<div class="col-md-12 col-sm-12 col-xs-12 margintop10 paddingleftright"><div class="form-group"><label class="control-label col-md-5" style="padding-right: 0px;"></label><div class="col-md-5" style="padding-right: 0px;"><a href="'+baseurls+'admin/tender/download/'+tenderdetail[k]+'" class="editdownload" ><img src="'+baseurls+'assets/layouts/layout/img/dow.png'+'">Click here to download</a><input type="hidden" name="upload_tenderdetail[]" id="upload_tenderdetail_'+k+'" value="'+tenderdetail[k]+'"></div><div class="col-md-2"><button type="button" class="btn btn-danger customaddmorebtn" name="remove" id="remove"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button></div></div></div>';
        $('#detaildoc').append(tenderdetailnewfield);
    };

    var tenderdocArr=[];
    var tenderdoc=JSON.parse('<?php echo json_encode($Arrtenderdoc);?>');
    var tenderdocArr = JSON.parse('<?php echo json_encode($tenderdoc_count); ?>');
    for (var l= 0;l<tenderdoc.length; l++) {
        var tenderdocnewfield = '<div class="col-md-12 col-sm-12 col-xs-12 margintop10 paddingleftright"><div class="form-group"><label class="control-label col-md-5" style="padding-right: 0px;"></label><div class="col-md-5" style="padding-right: 0px;"><a href="'+baseurls+'admin/tender/download/'+tenderdoc[l]+'" class="editdownload" ><img src="'+baseurls+'assets/layouts/layout/img/dow.png'+'">Click here to download</a><input type="hidden" name="upload_tenderdoc[]" id="upload_tenderdoc_'+l+'" value="'+tenderdoc[l]+'"></div><div class="col-md-2"><button type="button" class="btn btn-danger customaddmorebtn" name="remove" id="remove"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button></div></div></div>';
        $('#tenderdoc').append(tenderdocnewfield);
    };

    var doconeArr=[];
    var docone=JSON.parse('<?php echo json_encode($Arrdocone);?>');
    var doconeArr = JSON.parse('<?php echo json_encode($docone_count); ?>');
    for (var m = 0;m<docone.length; m++) {
        var doconenewfield = '<div class="col-md-12 col-sm-12 col-xs-12 margintop10 paddingleftright"><div class="form-group"><label class="control-label col-md-5" style="padding-right: 0px;"></label><div class="col-md-5" style="padding-right: 0px;"><a href="'+baseurls+'admin/tender/download/'+docone[m]+'" class="editdownload" ><img src="'+baseurls+'assets/layouts/layout/img/dow.png'+'">Click here to download</a><input type="hidden" name="tender_docone[]" id="tender_docone_'+m+'" value="'+docone[m]+'"></div><div class="col-md-2"><button type="button" class="btn btn-danger customaddmorebtn" name="remove" id="remove"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button></div></div></div>';
        $('#tenderdocone').append(doconenewfield);
    };
    

    var doctwoArr=[];
    var doctwo=JSON.parse('<?php echo json_encode($Arrdoctwo);?>');
    var doctwoArr = JSON.parse('<?php echo json_encode($doctwo_count); ?>');
    for (var n = 0;n<doctwo.length; n++) {
        var doctwonewfield = '<div class="col-md-12 col-sm-12 col-xs-12 margintop10 paddingleftright"><div class="form-group"><label class="control-label col-md-5" style="padding-right: 0px;"></label><div class="col-md-5" style="padding-right: 0px;"><a href="'+baseurls+'admin/tender/download/'+doctwo[n]+'" class="editdownload" ><img src="'+baseurls+'assets/layouts/layout/img/dow.png'+'">Click here to download</a><input type="hidden" name="tender_doctwo[]" id="tender_doctwo_'+n+'" value="'+doctwo[n]+'"></div><div class="col-md-2"><button type="button" class="btn btn-danger customaddmorebtn removeredicon" name="remove" id="remove"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button></div></div></div>';
        $('#tenderdoctwo').append(doctwonewfield);
    };
    //j++;
</script>
<!-- END CONTENT BODY -->
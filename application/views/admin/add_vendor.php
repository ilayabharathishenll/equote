<?php //print_r($data);exit; ?><div class="page-content">
    <?php
    $msg=$this->session->flashdata('message_inivte');
    if(!empty($msg)) {
    ?>  
        <div class="alert alert-success alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <?php echo $msg ?>
        </div>
    <?php
    }
    ?>
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/vendors.png" class="imgbasline"> Invite Vendor
            </div>
        </div>
        <div class="portlet-body form">
            <form name="add_vendor" id="add_vendor" class="horizontal-form" method="post" action="<?php echo base_url()."admin/vendor/add_vendor"?>">
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Vendor Name  <span class="redstar">*</span></label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="vendor_name" id="vendor_name" placeholder="">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Contact No  <span class="redstar">*</span></label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="contact_no" id="contact_no" placeholder=""  minlength="10">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Email Id  <span class="redstar">*</span></label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="email" id="email" placeholder="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-actions formbtncenter">
                	<button type="submit" class="btn green customsavebtn">
                        <i class="fa fa-check"></i> Invite
                    </button>
                    <button type="button" class="btn red customrestbtn" id="resetEmpty"> <i class="fa fa-refresh"></i> Reset </button>
                </div>
            </form>
            <!-- END FORM-->
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $(".customrestbtn").click(function(){
            $("#add_vendor").trigger("reset");
        });
    });
</script>
<!-- END CONTENT BODY -->
<!-- BEGIN CONTENT BODY -->
<?php
$rfq='';
 foreach($data['tender'] as $value){ ?>
<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/tenders.png" class="imgbasline"> Adding RFQ Details
            </div>
        </div>
        <div class="portlet-body form">
            <?php //echo form_open_multipart('admin/tender/upload_file'); ?>
            <!-- BEGIN FORM-->
            <form name="frm_rfq" id="frm_rfq" class="horizontal-form" method="POST" enctype="multipart/form-data">
                <div class="form-body">
                    <h3 class="form-section formheading">E Quote Basic Detail</h3>
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">RFQ Number  <span class="redstar"></span></label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="rfq_no" id="rfq_no" placeholder="" value="<?php echo $value->ref_no; ?>" readonly>
                                   
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Part Name  <span class="redstar"></span></label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="sel_partname" id="sel_partname" value="<?php echo !empty($value->part_name)?$value->part_name:"";?>" readonly>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->

                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Quantity  <span class="redstar"></span></label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="sel_quantity" id="sel_quantity" value="<?php echo !empty($value->quantity)?$value->quantity:"0";?>" readonly>
                                   
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">E Quote Price <span class="redstar">*</span></label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="base_price" id="base_price" placeholder="1000.00" value="<?php echo !empty($value->base_price)?$value->base_price:"";?>" readonly>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->

                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">E Quote Title  <span class="redstar">*</span></label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="tender_title" id="tender_title" placeholder="E Quote Title">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Upload drawing & Technical Spes</label>
                                <div class="col-md-8">
                                    <input type="file" name="upload_drawing" id="upload_drawing">
                                    <i>(Note:Upload Only for .pdf, .jpeg, .png)</i>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->

                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Norm  <span class="redstar"></span></label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="norm" id="norm" placeholder="Norm">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">RM Price  <span class="redstar"></span></label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="rm_price" id="rm_price" placeholder="1000.00">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-12 paddingbottom">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <textarea class="form-control" name="tender_detail" id="tender_detail" rows="5" cols="50" placeholder="Enter the E Quote Details...."></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/row-->

                    <h3 class="form-section formheading">Upload E Quote Documents</h3>
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="col-md-10" style="padding-right: 0px;">
                                <span class="help-block pull-right">(Upload Multiple Document's)</span>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-5" style="padding-right: 0px;">E Quote Terms and Condition</label>
                                <div class="col-md-5">
                                    <input type="file" name="upload_terms[]" id="upload_terms">
                                </div>
                                <div class="col-md-2">
                                     <button type="button" class="btn btn-primary customaddmorebtn" name="addmore" id="addmore"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
                                </div>
                            </div>
                            <div id="terms">
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="col-md-10" style="padding-right: 0px;">
                                <span class="help-block pull-right">(Upload Multiple Document's)</span>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-5">Help Document</label>
                                <div class="col-md-5">
                                   
                                    <input type="file" name="upload_helpdoc[]" id="upload_helpdoc">
                                </div>
                                <div class="col-md-2">
                                     <button type="button" class="btn btn-primary customaddmorebtn" name="helpmore" id="helpmore"><span class="glyphicon glyphicon-plus" aria-hidden="true" ></span></button>
                                </div>
                            </div>
                            <div id="helpdoc">
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-5">E Quote Detail</label>
                                <div class="col-md-5">
                                    <input type="file" name="upload_tenderdetail[]" id="upload_tenderdetail">
                                </div>
                                <div class="col-md-2">
                                     <button type="button" class="btn btn-primary customaddmorebtn" name="detailmore" id="detailmore"><span class="glyphicon glyphicon-plus" aria-hidden="true" ></span></button>
                                </div>
                            </div>
                            <div id="detaildoc">
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-5" style="padding-right: 0px;">E Quote Documents Required</label>
                                <div class="col-md-5">

                                    <input type="file" name="upload_tenderdoc[]" id="upload_tenderdoc">
                                </div>
                                <div class="col-md-2">
                                     <button type="button" class="btn btn-primary customaddmorebtn" name="tenderdocmore" id="tenderdocmore"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
                                </div>
                            </div>
                            <div id="tenderdoc">
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-5">E Quote Document</label>
                                <div class="col-md-5">
                                    <input type="file"  name="tender_docone[]" id="tender_docone">
                                </div>
                                <div class="col-md-2">
                                     <button type="button" class="btn btn-primary customaddmorebtn" name="tenderdoconemore" id="tenderdoconemore"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
                                </div>
                            </div>
                            <div id="tenderdocone">
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-5">E Quote Document</label>
                                <div class="col-md-5">
                                    <input type="file" name="tender_doctwo[]" id="tender_doctwo">
                                </div>
                                <div class="col-md-2">
                                     <button type="button" class="btn btn-primary customaddmorebtn" name="tenderdoctwomore" id="tenderdoctwomore"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
                                </div>
                            </div>
                             <div id="tenderdoctwo">
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->

                    <h3 class="form-section formheading">E Quote Date Detail</h3>
                    
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Start Date & Time  <span class="redstar">*</span></label>
                                <div class="col-md-5">
                                    <input type="text" class="form-control" name="start_date" id="start_date" autocomplete="off" data-date-format="dd/mm/yyyy" data-date-start-date="+0d" placeholder="DD/MM/YYYY">
                                </div>
                                <div class="col-md-3">
                                    <input type="text" class="form-control timepicker timepicker-24" name="start_time" id="start_time" autocomplete="off">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">End Date & Time  <span class="redstar">*</span></label>
                                <div class="col-md-5">
                                    <input type="text" class="form-control" name="end_date" id="end_date" autocomplete="off" data-date-format="dd/mm/yyyy" placeholder="DD/MM/YYYY">
                                </div>
                                <div class="col-md-3">
                                    <input type="text" class="form-control timepicker timepicker-24" name="end_time" id="end_time" autocomplete="off" >
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Delivery Date  <span class="redstar">*</span></label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="delivery_date" id="delivery_date" autocomplete="off" data-date-format="dd/mm/yyyy" data-date-start-date="+0d" placeholder="DD/MM/YYYY">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Delivery Duration  <span class="redstar">*</span></label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="delivery_duration" id="delivery_duration" placeholder="Delivery Duration in Days" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Notification setting  <span class="redstar">*</span></label>
                                <div class="col-md-8">
                                    <select class="form-control" name="notification_setting" id="notification_setting">
                                        <option value="1">All</option>
                                        <option value="2">Supplier with same commodity</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 paddingbottom hidden supplier-select">
                            <div class="form-group">
                                <label class="control-label col-md-4">Suppliers <span class="redstar">*</span></label>
                                <div class="col-md-8">
                                    <select name="suppliers[]" id="suppliers" class="select2 form-control" multiple>
                                        <?php
                                            foreach($data['suppliers'] as $supplier) {
                                                echo "<option value=".$supplier['ID'].">".$supplier['Name']."</option>";
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-actions formbtncenter">
                	<button type="submit" name="submit" value="submit" class="btn green customsavebtn">
                        <i class="fa fa-check"></i> Save Data
                    </button>
                    <button type="button" class="btn red customrestbtn" id="resetEmpty"> <i class="fa fa-refresh"></i> Reset Data</button>
                </div>
            </form>
            <!-- END FORM-->
        </div>
    </div>
</div>
<?php } ?>

<script type="text/javascript">
jQuery(document).ready(function(){
    $('#notification_setting').on('change', function() {
        if (this.value == 2)
            $('.supplier-select').removeClass('hidden');
        else
            $('.supplier-select').addClass('hidden');

    });
    $('#end_date').on('focusout',function(){
        console.log($(this).val());
        console.log($('#end_date').val());
    })
   $('#start_date').on('focusout',function(){
        console.log($(this).val());
        console.log($('#start_date').val());
    })
   $('#delivery_date').on('focusout',function(){
        //console.log($(this).val());
        //console.log($('#delivery_date').val());
    })

    jQuery.validator.addMethod("lettersonly", function(value, element) {
      return this.optional(element) || /^[a-z," "]+$/i.test(value);
    }, "Please enter the alphabet letters only"); 
    $("#frm_rfq").validate({
        rules: {
            delivery_duration:{required:true,number:true},
            delivery_date:{required:true},
            base_price:{required:true,number: true},
            tender_title:{required:true},
            //norm:{required:true},
            //rm_price:{required:true,number: true},
            tender_detail:{required:true},
            start_date: {required: true},
            start_time:{required: true},
            end_date:{required: true},
            end_time:{required: true},
            notification_setting:{required: true},
        },
        messages: {
            delivery_duration:{required:"Please enter the delivery duration",number:"Please enter the numbers only"},
            delivery_date:{required:"Please select delivery date"},
            base_price:{required:"Please enter the base price",number:"Please enter the numbers only"},
            tender_title:{required:"Please enter the title",lettersonly: "Please enter the alphabet letters only"},
            //norm:{required:"Please enter the norm"},
            tender_detail:{required:"Please enter the tender details"},
            //rm_price:{required:"Please enter the rm price",number:"Please enter the numbers only"},
            start_date: {required: "Please select start date"},
            start_time:{required: "Please select the start time"},
            end_date:{required: "Please select end date"},
            end_time:{required: "Please select the end time"},
            notification_setting:{required:"Please select the notification"},
        },
    });
});
</script>
      
  <script type="text/javascript">
    $('#start_time').timepicker({showMeridian: false });
    $('#end_time').timepicker({ showMeridian: false});
    $("#start_date").datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
    }).on('changeDate', function (selected) {
         $("#start_date").valid();
        //console.log($("#start_date").val());
        var startDate = new Date(selected.date.valueOf());
        $('#end_date').datepicker('setStartDate', startDate);
    }).on('clearDate', function (selected) {
        $('#end_date').datepicker('setStartDate', null);
    }).change(function () {
        $('#start_date').validate();
    });

    $("#end_date").datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true            
    }).on('changeDate', function (selected) {
        $("#end_date").valid();
        console.log($("#end_date").val());
        var endDate = new Date(selected.date.valueOf());
        $('#start_date').datepicker('setEndDate', endDate);

        $("#delivery_date").datepicker('setStartDate', endDate);
        var deliveryDate = $("#delivery_date").val();
        var end_date = $("#end_date").val();
        //alert(deliveryDate);
        if (deliveryDate!="" && end_date!="") {
            $.ajax({
                url: base_url+'admin/tender/get_delivery',
                type: 'POST',
                data: {
                    'delivery_date': deliveryDate,
                    'end_date': end_date
                },
                success: function(response) {
                    var data=response.split("##^^##");
                    if (data[0] == 'success') {
                        $("#delivery_duration").val(data[1]);
                    }
                }
            });
        } else {
            return false;
        }
    }).on('clearDate', function (selected) {
        $('#start_date').datepicker('setEndDate', null);
    }).change(function () {
        $('#end_date').validate();
    });
    $("#delivery_date").datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true            
    }).on('changeDate', function (selected) {
        $("#delivery_date").valid();
        var deliveryDate = $("#delivery_date").val();
        var end_date = $("#end_date").val();
        //alert(deliveryDate);
        if (deliveryDate!="" && end_date!="") {
            $.ajax({
                url: base_url+'admin/tender/get_delivery',
                type: 'POST',
                data: {
                    'delivery_date': deliveryDate,
                    'end_date': end_date
                },
                success: function(response) {
                    var data=response.split("##^^##");
                    if (data[0] == 'success') {
                        $("#delivery_duration").val(data[1]);
                    }
                }
            });
        } else {
            return false;
        }
        
    }).change(function () {
        $('#delivery_date').validate();
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#resetEmpty').on('click',function(){
            $("#frms_rfq")[0].reset();
        });
    });
</script>
<!-- END CONTENT BODY -->
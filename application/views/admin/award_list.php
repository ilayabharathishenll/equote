<!-- BEGIN CONTENT BODY -->
<?php
//print_r($getAwardList);exit;
	$search_rfq = $search_title = $search_name = $search_partno=$search_part='';
	if (!empty($search)) {
		$search_rfq = $search['rfq_no'];
		$search_title = $search['tender_title'];
		$search_name = $search['vendor_name'];
		$search_part = $search['part_name'];
		$search_partno = $search['part_no'];
	}
?>
<div class="page-content">
	<?php
        $msg=$this->session->flashdata('message');
        if(!empty($msg)) {
    ?>
    <div class="alert alert-success alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          	<?php echo $msg ?>
    </div>
    <?php
    }
    ?>
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/awards.png" class="imgbasline"> Manage Awards
            </div>
        </div>
        <div class="portlet-body">
        	<form id="frm_award" name="frm_award" method="POST">
		        <div class="row">
		        	<div class="col-md-12 paddingleftright">
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                           <input type="text" class="form-control" name="award[rfq_no]" id="rfq_no" placeholder="RFQ No" value="<?php echo $search_rfq ?>">
	                        </div>
		        		</div>
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                            <input type="text" class="form-control" name="award[tender_title]" id="tender_title" placeholder="E Quote Name"  value="<?php echo $search_title ?>">
	                        </div>
		        		</div>
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                            <input type="text" class="form-control" name="award[part_name]" id="part_name" placeholder="Part Name"  value="<?php echo $search_part ?>">
	                        </div>
		        		</div>
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                            <input type="text" class="form-control" name="award[part_no]" id="part_no" placeholder="Part Number"  value="<?php echo $search_partno ?>">
	                        </div>
		        		</div>
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                           <input type="text" class="form-control" name="award[vendor_name]" id="vendor_name" placeholder="Vendor Name" value="<?php echo $search_name ?>">
	                        </div>
		        		</div>
		        		<div class="col-md-3">
		        			<div class="col-md-12 paddingleftright">
		        				<button type="submit" class="btn btn-warning customsearchtbtn"> <i class="fa fa-search"></i> Search</button>
		        				<a href="<?php base_url()."admin/awards/award_list"?>" type="button" class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Reset</a>
		        			</div>
		        		</div>
		        	</div>
		        </div>
	        	<div class="table-responsive" style="overflow-x: inherit;margin-top:0px;">
		            <table class="table table-striped table-bordered table-hover suppliertbl" id="award-list">
		            	<thead>
		                    <tr> 
		                    	<th> SI.NO </th>
		                        <th> RFQ NO </th>
		                        <th> Part NO </th>
		                        <th> E Quote Name</th>
		                        <th> Vendor Name</th>
		                        <th> Quote Price</th>
		                        <th> Status </th>
		                        <th> Action </th>
		                    </tr>
		                </thead>
		                </tbody>
		                    <?php
		                       if (count($getAwardList,COUNT_RECURSIVE)>1) {
		                       	    $sno=1;
		                       		foreach ($getAwardList as $AwardList) {
		                    ?>
		                    <tr>
		                    	<td><?php echo $sno ?></td>
		                        <td><?php echo $AwardList["ref_no"]?></td>
		                        <td><?php echo $AwardList["part_no"]?></td>
		                        <td><?php echo $AwardList["tender_title"]?></td>
		                        <td><?php echo $AwardList["vendor_name"]?></td>
		                        <td><?php echo $AwardList["applyprice"]?></td>
		                        <td>
		                        	<a type="button" class="btn btn-success btn-xs custominvitebtn"> <i class="icon-badge"></i> Awarded
                            		</a>
		                        </td>
		                        <td class="action">
		                        	<a data-toggle="modal" data-value="view" data-target="#biddingCount" type="button" class="btn btn-primary btn-xs custominvitebtn"> <i class="fa fa-comments-o"></i> Bidding Log
	                               	<input id="vendor_name_visible" type="hidden" value="<?php echo $AwardList["vendor_name"]?>">
                            		<input id="bidding_count_visible" type="hidden" value="<?php echo $AwardList["bidding_count"]?>">
                            		<input id="vendor_code_visible" type="hidden" value="<?php echo $AwardList["vendor_code"]?>">
                            		<input id="quote_price_visible" type="hidden" value="<?php echo $AwardList["applyprice"]?>">
                            		<table class="hidden table table-striped table-bordered table-hover" id="bidding_log">
							            	<thead>
							                    <tr>
							                    	<th> SI.NO </th>
							                        <th> Date </th>
							                        <th> Price </th>
							                    </tr>
							                </thead>
							                <tbody>
						                    <?php
						                      if (count($AwardList['bidding_log'],COUNT_RECURSIVE)>1) { 
												$no = 1;
												foreach ($AwardList['bidding_log'] as $bidding_log) {
													$date = date_create($bidding_log["date"]);
												?>
													<tr>
														<td><?php echo $no;?></td>
														<td><?php echo date_format($date, "Y/m/d H:i:s")?></td>
														<td><?php echo $bidding_log["price"]?></td>
													</tr>
												<?php 
												$no++;
												}
											}
						                      	?>
							                </tbody>
						                </table>
		                        </td>
		                    </tr>
		                    <?php
		                    $sno++;
		                		}
 		                    } 
		                    ?>
		                </tbody>
		            </table>
		        </div>
		    </from>
        </div>
    </div>
</div>
<div class="modal fade in" id="biddingCount" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><b>Vendor Bidding Status</b></h4>
            </div>
            <div class="modal-body"> 
	             <div class="row">
	             	<div class="col-md-11" style="margin-top: 15px;">
	             		<div class="form-group">
	             			<label class="control-label col-md-4">Vendor Name:</label>
	             			<div class="col-md-8"> <span id="vendor_hidden_name" style="font-weight: bold;"></span></div>
	             		</div>
	             	</div>
	             	<div class="col-md-11" style="margin-top: 15px;">
	             		<div class="form-group">
	             			<label class="control-label col-md-4">Vendor Code:</label>
	             			<div class="col-md-8" style="font-weight: bold;"> <span id="vendor_hidden_code"></span></div>
	             		</div>
	             	</div>
	             	<div class="col-md-11" style="margin-top: 15px;">
	             		<div class="form-group">
	             			<label class="control-label col-md-4">Awarded Price:</label>
	             			<div class="col-md-8" style="font-weight: bold;"> <span id="vendor_quote_price"></span></div>
	             		</div>
	             	</div>
	             	<div class="col-md-11" style="margin-top: 15px;">
	             		<div class="form-group">
	             			<label class="control-label col-md-4">Bidding Changes:</label>
	             			<div class="col-md-8" style="font-weight: bold;"> <span id="vendor_bidding_count"></span> Time(s)</div>
	             		</div>
	             	</div>
	             	<div class="col-md-12" style="margin-top: 15px;">
	             		<div>
	             			<table id="vendor_bidding_log" class="table table-striped table-bordered table-hover"></table>
	             		</div>
	             	</div>
	             </div>
            </div>
       </div>
   </div>
</div>

<script type="text/javascript">
	$(document).on('click','#award-list .action a.btn-primary', function(){
		name = $(this).children('#vendor_name_visible').val();
		code = $(this).children('#vendor_code_visible').val();
		count = $(this).children('#bidding_count_visible').val();
		price = $(this).children('#quote_price_visible').val();
		bidding_log = $(this).children('table.hidden').html();
		$('#vendor_hidden_name').text(name);
		$('#vendor_hidden_code').text(code);
		$('#vendor_bidding_count').text(count);
		$('#vendor_quote_price').text(price);
		$('#vendor_bidding_log').html(bidding_log);
	});
	
</script>
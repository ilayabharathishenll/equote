<?php
foreach ($dashboarCount as $dashboardData) {
   $latestequote = $dashboarCount["latestequote"];
   $data=array("badgecount"=>$latestequote);
   $this->session->set_userdata($data);
   $appliedquote = $dashboarCount["noofappliedquote"];
   $awardedquote = $dashboarCount["awardedquote"];
   $noofvendor = $dashboarCount["noofvendor"];
}
$interval='';$today=date('Y-m-d H:i:s');
function datetimeDiff($dt1, $dt2){
        $t1 = strtotime($dt1);
        $t2 = strtotime($dt2);
        $dtd = new stdClass();
        $dtd->interval = $t2 - $t1;
        $dtd->total_sec = abs($t2-$t1);
        $dtd->total_min = floor($dtd->total_sec/60);
        $dtd->total_hour = floor($dtd->total_min/60);
        $dtd->total_day = floor($dtd->total_hour/24);

        $dtd->day = $dtd->total_day;
        $dtd->hour = $dtd->total_hour -($dtd->total_day*24);
        $dtd->min = $dtd->total_min -($dtd->total_hour*60);
        $dtd->sec = $dtd->total_sec -($dtd->total_min*60);
        return $dtd;
    }
?>
<!-- BEGIN CONTENT BODY -->
<style>
.feeds li .col2 {
  float: left;
  width: 152px !important;
  margin-left: -179px !important;
}
</style>
<div class="page-content"  id="dashboard">
    
    <?php
    $msg=$this->session->flashdata('success');
    if(!empty($msg)){
    ?>
    <div>
        <div class="alert alert-success alert-dismissible">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <?php echo $msg ?>
        </div>
    </div>
    <?php
    }
    ?>
    <div class="row widget-row">
        <div class="col-md-3">
            <!-- BEGIN WIDGET THUMB -->
            <a href="<?php echo base_url()?>admin/tender/tender_list" style="text-decoration:none;">
            <div class="widget-thumb dashboard-stat red text-uppercase margin-bottom-20 bordered">
                <h4 class="widget-thumb-heading">Latest E Quote</h4>
                <div class="widget-thumb-wrap">
                    <i class="widget-thumb-icon bg-white icon-social-dropbox redicon dashboardborder"></i>
                    <div class="widget-thumb-body">
                        <!-- <span class="widget-thumb-subtitle">USD</span> -->
                        <span class="widget-thumb-body-stat" data-counter="counterup" data-value="<?php echo $latestequote?>"><?php echo $latestequote?></span>
                    </div>
                </div>
            </div>
            </a>
            <!-- END WIDGET THUMB -->
        </div>
        <div class="col-md-3">
            <!-- BEGIN WIDGET THUMB -->
            <a href="<?php echo base_url()?>admin/tender/applied_tender" style="text-decoration:none;">
            <div class="widget-thumb dashboard-stat bg-green-jungle text-uppercase margin-bottom-20 bordered">
                <h4 class="widget-thumb-heading">No Of Applied E Quote</h4>
                <div class="widget-thumb-wrap">
                    <i class="widget-thumb-icon bg-white icon-layers font-green-jungle dashboardborder"></i>
                    <div class="widget-thumb-body">
                        <!-- <span class="widget-thumb-subtitle">USD</span> -->
                        <span class="widget-thumb-body-stat" data-counter="counterup" data-value="<?php echo $appliedquote?>"><?php echo $appliedquote?></span>
                    </div>
                </div>
            </div>
            </a>
            <!-- END WIDGET THUMB -->
        </div>
        <div class="col-md-3">
            <!-- BEGIN WIDGET THUMB -->
            <a href="<?php echo base_url()?>admin/awards/award_list" style="text-decoration:none;">
            <div class="widget-thumb dashboard-stat yellow-casablanca text-uppercase margin-bottom-20 bordered">
                <h4 class="widget-thumb-heading">No Of Awards E Quote</h4>
                <div class="widget-thumb-wrap">
                    <i class="widget-thumb-icon bg-white icon-badge dashboardborder font-yellow-casablanca"></i>
                    <div class="widget-thumb-body">
                        <span class="widget-thumb-body-stat" data-counter="counterup" data-value="<?php echo $awardedquote?>"><?php echo $awardedquote?></span>
                    </div>
                </div>
            </div>
            </a>
            <!-- END WIDGET THUMB -->
        </div>
        <div class="col-md-3">
            <!-- BEGIN WIDGET THUMB -->
            <a href="<?php echo base_url()?>admin/vendor/vendor_list" style="text-decoration:none;">
            <div class="widget-thumb dashboard-stat blue text-uppercase margin-bottom-20 bordered">
                <h4 class="widget-thumb-heading">No of Vendors</h4>
                <div class="widget-thumb-wrap">
                    <i class="widget-thumb-icon bg-white font-blue icon-drawer dashboardborder"></i>
                    <div class="widget-thumb-body">
                       <!--  <span class="widget-thumb-subtitle">USD</span> -->
                        <span class="widget-thumb-body-stat" data-counter="counterup" data-value="<?php echo $noofvendor?>"><?php echo $noofvendor?></span>
                    </div>
                </div>
            </div>
            </a>
            <!-- END WIDGET THUMB -->
        </div>
    </div>
    <div class="row" id="recent">
        <div class="col-lg-12 col-xs-12 col-sm-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-share font-dark hide"></i>
                        <span class="caption-subject font-dark bold uppercase recent-activity">Recent Activities</span>
                    </div>             
                </div>
                <div class="portlet-body">
                    <div class="scroller" style="height: 300px;" data-always-visible="1" data-rail-visible="0">
                        <ul class="feeds">
                            <?php if(!empty($dashboarCount["recent_vendor"])){
                                foreach($dashboarCount["recent_vendor"] as $vendor){ ?>
                            <li>
                                <a href="<?php echo base_url()?>admin/vendor/vendor_list">
                                <div class="col1">
                                    <div class="cont">
                                        <div class="cont-col1">
                                            <div class="label label-sm label-info">
                                                <i class="fa fa-user"></i>
                                            </div>
                                        </div>
                                        <div class="cont-col2">
                                            <div class="desc"> New Vendor Registered -
                                                <span class="label label-sm label-warning "><?php echo $vendor->first_name." ".$vendor->last_name;?>
                                                   <!--  <i class="fa fa-user"></i> -->
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col2">
                                    <div class="date"> <?php 
                                    $interval=datetimeDiff($today,$vendor->modified_date);
                                    //print_r($interval);
                                    if(!empty($interval->day)){
                                        echo $interval->day."Day ago";
                                    }elseif(empty($interval->day) && !empty($interval->hour) && !empty($interval->min)){
                                        echo $interval->hour."Hour ".$interval->min."min ago";
                                    }elseif(empty($interval->day) && empty($interval->hour) && !empty($interval->min)){
                                        echo $interval->min."min ago";
                                    }elseif(empty($interval->day) && empty($interval->hour) && empty($interval->min) && !empty($interval->sec) ){
                                                echo "Just ".$interval->sec." sec ago";
                                            }
                                    ?></div>

                                    <!-- <div class="date"> Just Now </div> -->
                                
                                </div>
                                </a>
                            </li>
                            <?php } } 
                             if(!empty($dashboarCount["recent_tender"])){
                                foreach($dashboarCount["recent_tender"] as $tender){ ?>
                            <li>
                                <?php
                                    if ($tender->current_status !='Awarded') {
                                ?>
                                <a href="<?php echo base_url()?>admin/tender/tender_list">
                                   
                                    <div class="col1">
                                        <div class="cont">
                                            <div class="cont-col1">
                                                <div class="label label-sm label-success">
                                                    <i class="icon-envelope"></i>
                                                </div>
                                            </div>
                                            <div class="cont-col2">
                                                <div class="desc">
                                                    <?php

                                                        if($tender->current_status=='Add'){
                                                            echo "New E Quote ".$tender->tender_title." Posted";
                                                        }elseif($tender->current_status=='Delete'){
                                                            echo "Deleted E Quote ".$tender->tender_title." Successfully";
                                                        }elseif($tender->current_status=='Update'){
                                                             echo "Update E Quote ".$tender->tender_title." Successfully";
                                                        }
                                                        elseif($tender->current_status=='Cancelled'){
                                                             echo "Cancelled E Quote ".$tender->tender_title." Successfully";
                                                        }
                                                    ?>
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                    <div class="col2">
                                        <div class="date">
                                            <?php 
                                            $interval=datetimeDiff($today,$tender->modified_on);
                                            //print_r($interval);
                                            if(!empty($interval->day)){
                                                echo $interval->day."Day ago";
                                            }elseif(empty($interval->day) && !empty($interval->hour) && !empty($interval->min)){
                                                echo $interval->hour."Hour ".$interval->min."min ago";
                                            }elseif(empty($interval->day) && empty($interval->hour) && !empty($interval->min)){
                                                echo $interval->min."min ago";
                                            }elseif(empty($interval->day) && empty($interval->hour) && empty($interval->min) && !empty($interval->sec) ){
                                                echo "Just ".$interval->sec." sec ago";
                                            }
                                            ?>
                                        </div>
                                    </div>
                                    
                                </a>
                                <?php
                                }
                                ?>
                            </li>
                            <?php }} if(!empty($dashboarCount["recent_award"])){
                                foreach($dashboarCount["recent_award"] as $award){ ?>
                            <li>
                                <a href="<?php echo base_url()?>admin/awards/award_list">
                                <div class="col1">
                                    <div class="cont">
                                        <div class="cont-col1">
                                            <div class="label label-sm bg-yellow-casablanca">
                                                <i class="icon-badge"></i>
                                            </div>
                                        </div>
                                        <div class="cont-col2">
                                            <div class="desc"> 
                                                <?php 
                                                  echo "Award Added This ".$award->tender_title." Tender";
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col2">
                                    <div class="date">
                                        <?php 
                                            $interval=datetimeDiff($today,$award->modified_on);
                                            //print_r($interval);
                                            if(!empty($interval->day)){
                                                echo $interval->day."Day ago";
                                            }elseif(empty($interval->day) && !empty($interval->hour) && !empty($interval->min)){
                                                echo $interval->hour."Hour ".$interval->min."min ago";
                                            }elseif(empty($interval->day) && empty($interval->hour) && !empty($interval->min)){
                                                echo $interval->min."min ago";
                                            }elseif(empty($interval->day) && empty($interval->hour) && empty($interval->min) && !empty($interval->sec) ){
                                                echo "Just ".$interval->sec." sec ago";
                                            }
                                            ?>
                                    </div>
                                </div>
                                </a>
                            </li>
                        <?php }} ?>
                        </ul>
                    </div>
                    <div class="scroller-footer">
                        <div class="btn-arrow-link pull-right">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
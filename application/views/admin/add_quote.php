<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/tenders.png" class="imgbasline"> Admin Posting E Quote
            </div>
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <form name="frm_quote" id="frm_quote" class="horizontal-form" method="POST" enctype="multipart/form-data">
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Enter the Part No <span class="redstar">*</span></label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="part_no" id="part_no" placeholder="Part Number">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Enter the Part Name  <span class="redstar">*</span></label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="part_name" id="part_name" placeholder="Part Name">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->

                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Enter the Part Price  <span class="redstar">*</span></label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="part_price" id="part_price" placeholder="1000.00">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Enter the Quantity  <span class="redstar">*</span></label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="quantity" id="quantity" placeholder="Quantity">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Category  <span class="redstar">*</span></label>
                                <div class="col-md-8">
                                    <select class="form-control" name="category" id="category">
                                        <option value="">Select category</option>
                                        <?php
                                            foreach($data as $value){
                                                echo "<option value='".$value->category_id."'>".$value->category_name."-".$value->cat_number."</option>";
                                            } 
                                        ?>
                                    </select> 
                                </div>
                            </div>
                        </div>
                    </div>    
                    <div class="row">
                        <div class="col-md-12 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Supplier can change the price details after the tender applied with in particular time of period </label>
                                <div class="col-md-8">
                                    <div class="radio-list">
                                        <label class="radio-inline">
                                            <input type="radio" name="period" class="checkempty" value="1" checked> Yes 
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="period" class="checkempty" value="2"> No 
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-12 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-3 drawinglabel">Enter the Drawing Issues No</label>
                                <div class="col-md-3 drawingtextbox">
                                    <input type="text" class="form-control" name="drawing_no" id="drawing_no" placeholder="20">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                </div>
                <div class="form-actions formbtncenter">
                	<button type="submit" name="submit" value="submit" class="btn green customsavebtn">
                        <i class="fa fa-check"></i> Save Data
                    </button>
                    <button type="button" class="btn red customrestbtn" id="resetEmpty"> <i class="fa fa-refresh"></i> Reset Data</button>
                </div>
            </form>
            <!-- END FORM-->
        </div>
         
    </div>
</div>
<script type="text/javascript">
    jQuery.validator.addMethod("lettersonly", function(value, element) {
    return this.optional(element) || /^[a-z," "]+$/i.test(value);
    }, "Please enter the alphabet letters only"); 
    $("#frm_quote").validate({
        rules: {
            part_no:{required:true},
            //part_no:{required:true,number:true},
            //part_no:{required:true,unique:true},
            part_name:{required:true},
            part_price:{required:true,number: true},
            quantity:{required:true,number: true},
            category:{required:true},
            //drawing_no:{required:true,number: true},
        },
        messages: {
            part_no:{required:"Please enter the part number"},
            //part_no:{required:"Please enter the part number",unique:"Part number already exists!"},
            //part_no:{required:"Please enter the part number",number:"Please enter the numbers only"},
            part_name:{required:"Please enter the part name",lettersonly:"Please enter the alphabet letters only"},
            part_price:{required:"Please enter the part price",number:"Please enter the numbers only"},
            quantity:{required:"Please enter the quantity",number: "Please enter the numbers only"},
            category:{required:"Please select the category"},
            //drawing_no:{required:"Please enter the drawing number",number:"Please enter the numbers only"},
        },
    });
    $.validator.addMethod('unique',function(value)
        {
            var part_no=$("#part_no").val();
           //var adminid=$('input[name="hndid"]').val();
            var result=$.ajax({
                        type:"POST",
                        async:false,
                        url:base_url+"admin/tender/part_number_exists",
                        data:{"part_no":part_no},
                        dataType:"text"});
         if(result.responseText=="not-exists"){
             return true;
         }
         else{ 
            return false;
         }
    },"Part number already  exists!");
  </script>
  <script type="text/javascript">
    $(document).ready(function(){
        $('#resetEmpty').on('click',function(){
            $("#frm_quote")[0].reset();
        });
    });
</script>
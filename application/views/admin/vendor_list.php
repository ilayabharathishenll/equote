<?php
//print_r($data);

	$search_name = $search_email = $search_num = '';
	if (!empty($data['search'])) {
		$search_name = $data['search']['first_name'];
		$search_email = $data['search']['email'];
		$search_num = $data['search']['phone'];
	}

	function icon($value){
	    $outputs=explode(".",$value);
	    $icons=$outputs[1];
	    if($icons=='jpg' || $icons=='jpeg' || $icons=='png' || $icons=='gif' || $icons=='svg'){
	        return "image";
	    }elseif($icons=='doc' || $icons=='docx'){
	         return "word";
	    }elseif($icons=='xls' || $icons=='xlsx'){
	         return "excel";    
	    }elseif($icons=='PDF'){
	         return "pdf";  
	    }elseif($icons=='ppt' || $icons=='pptx'){
	         return "powerpoint";  
	    }
	}
?>
<div class="page-content">
	<?php
    $msg=$this->session->flashdata('message');
    if(!empty($msg)) {
    ?>  
        <div class="alert alert-success alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <?php echo $msg ?>
        </div>
    <?php
    }
    ?>
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/vendors.png" class="imgbasline"> Vendor List
            </div>
            <div class="actions">
                <a href="<?php echo base_url()?>admin/vendor/add_vendor" class="btn green btn-sm customaddbtn">
                    <i class="fa fa-plus"></i> Invite Vendor</a>
            </div>
        </div>
        <div class="portlet-body">
        	<form id="vendor_list_search" method="post" class="row">
	        	<div class="col-md-12 paddingleftright">
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                            <input type="text" class="form-control" name="vendor[first_name]" id="vendor_name" placeholder="Vendor Name" value="<?php echo $search_name;?>">
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                           <input type="text" class="form-control" name="vendor[email]" id="email" placeholder="Email" value="<?php echo $search_email;?>">
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                            <input type="text" class="form-control" name="vendor[phone]" id="phone" placeholder="Contact Number" value="<?php echo $search_num;?>">
                        </div>
	        		</div>
	        		<div class="col-md-3">
	        			<div class="col-md-12 paddingleftright">
	        				<button type="submit" class="btn btn-warning customsearchtbtn"> <i class="fa fa-search"></i> Search</button>
	        				<a type="button" class="btn red customrestbtn" href="<?php echo base_url('admin/vendor/vendor_list');?>"> <i class="fa fa-refresh"></i> Reset</a>
	        			</div>
	        		</div>
	        	</div>
	        </form>
        	<div class="table-responsive" style="overflow-x: inherit;margin-top:0px;">
	            <table class="table table-striped table-bordered table-hover admintbl" id="vendor-list">
	            	<thead>
	                    <tr>
	                        <th> SI.NO </th>
	                        <th> Vendor Code</th>
	                        <th> Vendor Name </th>
	                        <th> Email </th>
	                        <th> Contact Number</th>
	                        <th> Status </th>
	                        <th> Action </th>
	                    </tr>
	                </thead>
	                </tbody>
	                	<?php 
	                		if (!empty($data)) {
	                			$i = 1;
	                			$status='';
	                			foreach ($data['vendor_list'] as $vendor) {
	                				$status = $vendor->status;
	                				?>
	                				<tr>
		                				<td> <?php echo $i?> </td>
										<td> Vendor-<?php echo $vendor->vendor_id; ?> </td>
										<td> <?php echo $vendor->first_name; ?> </td>
										<td> <?php echo $vendor->email; ?> </td>
		                				<td> <?php echo $vendor->phone; ?> </td>
		                				<?php if($status == 'Active'){?>
		                					<input type="hidden" name="vendor_hidden_name" id="vendor_hidden_name" value="<?php echo $vendor->first_name.' '.$vendor->last_name ;?>">
		                					<td><span class="label label-sm label-info labelboader" style="padding: 2px 10px;">Active</span>
		                					</td>
		                					<td style="text-align:left;"><a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn" id="btndelete" data-value="Trash" data-id="<?php echo $vendor->vendor_id; ?>" title="Delete" ><i class="fa fa-trash"></i> Delete</a><a href="#" type="button" class="btn green btn-xs customactionbtn documentDownload" data-value="view" data-id="<?php echo $vendor->vendor_id; ?>"  id="click_vendor"  title="View"><i class="fa fa-eye"></i> View
		                						<input type="hidden" name="vendor_id" id="vendor_id" class="vendor_id" value="<?php echo $vendor->vendor_id; ?>">
		                						<input type="hidden" name="vendor_hidden_name" id="vendor_hidden_name" value="<?php echo $vendor->first_name.' '.$vendor->last_name ;?>"><input type="hidden" name="supplier_doc" id="supplier_doc" class="supplier_doc" value="<?php echo $vendor->supplier_doc; ?>"></a></td>
	                					<?php } else if ($status == 'Inactive'){?>
	                						<td><span class="label label-sm label-danger labelboader">Inactive</span></td>
	                						<td style="text-align:left;" class="action">
	                							<a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn" id="btndelete1"data-value="Trash" data-id="<?php echo $vendor->vendor_id; ?>" title="Delete"><i class="fa fa-trash"></i> Delete</a><a href="#" type="button" class="btn green btn-xs customactionbtn documentDownload" data-toggle="modal" data-value="view" data-id="<?php echo $vendor->vendor_id; ?>"  id="click_vendor"  title="View"><i class="fa fa-eye"></i> View
	                							<input type="hidden" name="vendor_id" id="vendor_id" class="vendor_id" value="<?php echo $vendor->vendor_id; ?>"><input type="hidden" name="supplier_doc" id="supplier_doc" class="supplier_doc" value="<?php echo $vendor->supplier_doc; ?>"><input type="hidden" name="vendor_hidden_name" id="vendor_hidden_name" value="<?php echo $vendor->first_name.' '.$vendor->last_name ;?>"></a></td>
	                					<?php } else if ($status == 'Trash') { ?>
	                						<td><span class="label label-sm label-warning labelboader" style="padding: 2px 12px;">Trash</span></td>
	                						<td style="text-align:left;"> <a href="javascript:void(0);" data-value="Restore" data-id="<?php echo $vendor->vendor_id; ?>" type="button" class="btn green btn-xs customactionbtn " id="btnRestore" title="Restore"><i class="fa fa-floppy-o"></i> Restore <input type="hidden" name="vendor_hidden_name" id="vendor_hidden_name" value="<?php echo $vendor->first_name.' '.$vendor->last_name ;?>"></a></td>
                						<?php } else { ?>
                							<td><span class="label label-sm label-warning labelboader">Pending</span></td>
	                						<td style="text-align:left;" class="action"> 
	                							<a href="#" type="button" class="btn green btn-xs customactionbtn documentDownload" data-toggle="modal" data-value="view" data-id="<?php echo $vendor->vendor_id; ?>"  id="click_vendor"  title="View"><i class="fa fa-eye"></i> Activate
	                							<input type="hidden" name="vendor_id" id="vendor_id" class="vendor_id" value="<?php echo $vendor->vendor_id; ?>"><input type="hidden" name="supplier_doc" id="supplier_doc" class="supplier_doc" value="<?php echo $vendor->supplier_doc; ?>"><input type="hidden" name="vendor_hidden_name" id="vendor_hidden_name" value="<?php echo $vendor->first_name.' '.$vendor->last_name ;?>"></a></td>
	                					<?php } ?>
	                				</tr>
	                			<?php 
	                				$i++;
	                			}
	                		}
                		?>
	                </tbody>
	            </table>
	        </div>
        </div>
    </div>
</div>
<!-- END CONTENT BODY -->
<div class="modal fade in" id="docPopup" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        	<form class="vendor_status" id="vendor_status" action="<?php echo base_url('admin/vendor/vendor_list');?>" method="POST">
        		<input type="hidden" name="hnd_id" id="hnd_id">
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
	                <h4 class="modal-title"><b>Vendor Document</b></h4>
	            </div>
	            <div class="modal-body"> 
		             <div class="row">
		             	<div class="col-md-11" style="margin-top: 15px;">
		             		<div class="form-group">
		             			<label class="control-label col-md-4">Vendor Name:</label>
		             			<div class="col-md-8"> <span id="vendor_hidden_names" style="font-weight: bold;"></span></div>
		             		</div>
		             	</div>
		             </div>
		            <div class="row" id="non_upload_doc">
		             	<div class="col-md-11" style="margin-top: 15px;">
		             		<div class="form-group">
		                        <label class="control-label col-md-4">Vendor Document:</label>
		                        <div class="col-md-8 vendor-doc">
		                            <a href="javascript:void(0);" class="download_doc" id="upload_link"><img src="<?php echo base_url()?>assets/layouts/layout/img/dow.png">Click here to download</a>
		                            <input type="hidden" name="upload_link" id="vendor_link">
		                        </div>
		                    </div>
		             	</div>
		             	<div class="col-md-11" style="margin-top: 10px;margin-bottom: 15px;">
		             		<div class="form-group">
		                        <label class="control-label col-md-4">Vendor Status: <span class="redstar">*</span></label>
		                        <div class="col-md-8 vendor-detail">
		                        	<!-- <input type="hidden" name="vid" id="vid" value=""> -->
		                        	<input type="hidden" name="id" id="v_id">
		                            <select class="form-control" name="status" id="status">
		                            	<option>--Select Status--</option>
		                            	<option value="Active">Active</option>
		                            	<option value="Inactive">Inactive</option>
		                            	<option value="Pending">Pending</option>
		                            	<option value="Trash">Trash</option>
		                            </select>
		                        </div>
		                    </div>
		             	</div>
		             	<div class="col-md-11" style="margin-top: 5px;margin-bottom: 15px;">
		             		<div class="form-group">
		                        <label class="control-label col-md-4">Rating: <span class="redstar">*</span></label>
		                        <div class="col-md-8 vendor-doc">
		                            
		                            <input type="text" class="form-control" name="rating" id="rating" min="0" max="5">
		                        </div>
		                    </div>
		             	</div>
		            </div>
		            <div class="col-md-12" id="supply_doc_upload"><p>This vendor cannot be activated since he is not uploaded the supplier document.</p></div>
		            <div class="modal-footer" id="footer_cont">
		            	<button type="button" class="btn red customrestbtn"  data-dismiss="modal"> <i class="fa fa-close"></i> Close</button>
	                </div>
	                 <div class="modal-footer" id="modal-footer">
		            	<button type="submit" name="submit" value="submit" class="btn green customaddbtn" id="statuschange"> <i class="fa fa-check"></i> Save</button>
		            	<button type="button" class="btn red customrestbtn"  data-dismiss="modal"> <i class="fa fa-close"></i> Close</button>
	                </div>
	            </div>
            </form>
        <!-- /.modal-content -->
       </div>
    <!-- /.modal-dialog -->
   </div>
</div>
<!-- END CONTENT BODY -->
<script type="text/javascript">
    $(document).on('click','.documentDownload', function() {
    	let ids = $(this).attr('data-id');
    	//alert(ids);
    	$("#hnd_id").val(ids);
    	let doc=name=link="";
		$('#v_id').val(ids);
		doc=$(this).children('#supplier_doc').val();
		//alert(doc);
		name=$(this).children('#vendor_hidden_name').val();
		$('#vendor_hidden_names,#non_upload_vendor_name').text(name);
		if(doc!=''){
			link="<?php echo base_url('admin/tender/download').'/'; ?>" +doc;
			$('#upload_link').attr("href",link);
			$('#vendor_link').attr("value",doc);
			$('#supply_doc_upload').hide();
			$('#footer_cont').hide();
			$('#non_upload_doc').show();
			$('#modal-footer').show();

		}
		else{
			$('#non_upload_doc').hide();
			$('#footer_cont').show();
			$('#modal-footer').hide();
			$('#supply_doc_upload').show();
		}
		getvendordata(ids);
		$("#docPopup").modal("show");
	
	});

	$(document).on('click','#btnRestore,#btndelete,#btndelete1', function() {
		let id = $(this).attr('data-id');
		//alert(id);
		let datavalue= $(this).attr('data-value');
		//alert(datavalue);
		var msg;
		var confirmcheck;
		if (datavalue=="Trash") {
			msg='Are you sure you want to delete this vendor?';
			confirmcheck = confirm(msg);
        } else {
        	confirmcheck = true;
        }
		if(id!="" && confirmcheck == true && (datavalue=="Restore"|| datavalue=="Trash")) {
			var result=$.ajax({
					type:"POST",
					async:false,
					url:base_url+"admin/vendor/restoreajax",
					data:{"vendorid":id,"datavalue":datavalue},
					dataType:"text"});	
			if(result.responseText=="success"){
				location.reload();
			}
			else{ 
				return false;
			}

		} else {
			return false;
		}
	});
	$("#vendor_status").validate({
		submitHandler: function(form) {
			var vendor_data = tender_serialize(form);
			$.ajax({
				url: base_url+'admin/vendor/update_status',
				type: 'POST',
				data: {
					'vendor_data': vendor_data
				},
				success: function(response) {
					if (response == 'success') {
						location.reload();
					} else {
						$('#docPopup').modal('toggle');
					}
				}
			});
		}
	});
	function getvendordata(id) {
		$.ajax({
			url: base_url+'admin/vendor/get_vendor',
			type: 'POST',
			data: {
				'vendor_id': id
			},
			success: function(response) {
				var data=response.split("#^^#");
				if (data[0] == 'success') {
					$("#status").val(data[1]);
					$("#rating").empty().val(data[2]);
				}
			}
		});
	}
</script>
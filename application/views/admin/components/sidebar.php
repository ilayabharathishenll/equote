<?php
error_reporting(0);
$navigationURL = reset(explode("?",end(explode("/",$_SERVER["REQUEST_URI"]))));
if (is_numeric($navigationURL)) {
    $navigationURL=$this->uri->segment(3);
}
?>
<div class="page-sidebar-wrapper">
	<div class="page-sidebar navbar-collapse collapse">
		<ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 0px">
			<li class="sidebar-toggler-wrapper hide">
				<div class="sidebar-toggler">
					<span></span>
				</div>
			</li>
			<li class="nav-item <?php echo ($navigationURL=='dashboard')?'active open':''; ?>">
				<a href="<?php echo base_url().'admin/dashboard'; ?>" class="nav-link nav-toggle">
					<i class="dashboard-image"></i>
					<span class="title">Dashboard</span>
					<span class="selected"></span>
				</a>
			</li>
			<li class="nav-item <?php echo ($navigationURL=='add_quote' || $navigationURL=='add_rfq' || $navigationURL=='tender_list' || $navigationURL=='edit_rfq' || $navigationURL=='view_tender')?'active open':''; ?>">
				<a href="javascript:;" class="nav-link nav-toggle">
					<i class="icon-tender"></i>
					<span class="title">Manage E Quote</span>
					<span class="selected"></span>
					<span class="arrow"></span>
				</a>
				<ul class="sub-menu">
					<li class="nav-item start <?php echo ($navigationURL=='add_quote' || $navigationURL=='add_rfq' || $navigationURL=='edit_rfq')?'active open':''; ?>">
						<a href="<?php echo base_url().'admin/tender/add_quote'; ?>" class="nav-link">
							<span class="title">Posting E Quote</span>
						</a>
					</li>
					<li class="nav-item <?php echo ($navigationURL=='tender_list')?'active open':''; ?>">
						<a href="<?php echo base_url().'admin/tender/tender_list'; ?>" class="nav-link">
							<span class="title">E Quote List</span>
						</a>
					</li>
				</ul>
			</li>
			<li class="nav-item  <?php echo ($navigationURL=='vendor_list' || $navigationURL=='add_vendor' ||  $navigationURL=='edit_vendor')?'active open':''; ?>">
				<a href="javascript:;" class="nav-link nav-toggle">
					<i class="icon-vendor"></i>
					<span class="title">Manage Vendors</span>
					<span class="selected"></span>
					<span class="arrow"></span>
				</a>
				<ul class="sub-menu">
					<li class="nav-item start <?php echo ($navigationURL=='vendor_list' || $navigationURL=='edit_vendor')?'active open':''; ?>">
						<a href="<?php echo base_url().'admin/vendor/vendor_list'; ?>" class="nav-link">
							<span class="title">Vendor List</span>
						</a>
					</li>
					<li class="nav-item <?php echo ($navigationURL=='add_vendor' ||  $navigationURL=='edit_vendor')?'active open':''; ?>">
						<a href="<?php echo base_url().'admin/vendor/add_vendor'; ?>" class="nav-link">
							<span class="title">Invite Vendor</span>
						</a>
					</li>
				</ul>
			</li>
			<li class="nav-item  <?php echo ($navigationURL=='tender_invite' || $navigationURL=='send_invite' || $navigationURL=='viewtender'|| $navigationURL=='invited_tender')?'active open':''; ?>">
				<a href="javascript:;" class="nav-link nav-toggle">
					<i class="icon-invite"></i>
					<span class="title">Manage Invitation</span>
					<span class="selected"></span>
					<span class="arrow"></span>
				</a>
				<ul class="sub-menu">
					<li class="nav-item <?php echo ($navigationURL=='tender_invite' || $navigationURL=='send_invite' || $navigationURL=='view_tender')?'active open':''; ?>">
						<a href="<?php echo base_url().'admin/invite/tender_invite'; ?>" class="nav-link">
							<span class="title">E Quote Invitation</span>
						</a>
					</li>
					<li class="nav-item <?php echo ($navigationURL=='invited_tender')?'active open':''; ?>">
						<a href="<?php echo base_url().'admin/invite/invited_tender'; ?>" class="nav-link">
							<span class="title">Invited E Quotes</span>
						</a>
					</li>
				</ul>
			</li>
			<li class="nav-item <?php echo ($navigationURL=='applied_tender' || $navigationURL=='applied_list')?'active open':''; ?>">
				<a href="<?php echo base_url().'admin/tender/applied_tender'; ?>" class="nav-link nav-toggle">
					<i class="icon-applied"></i>
					<span class="title">Manage Applied E Quotes</span>
					<span class="selected"></span>
				</a>
			</li>
			<li class="nav-item <?php echo ($navigationURL=='award_list')?'active open':''; ?>">
				<a href="<?php echo base_url().'admin/awards/award_list'; ?>" class="nav-link nav-toggle">
					<i class="icon-award"></i>
					<span class="title">Manage Awards</span>
					<span class="selected"></span>
				</a>
			</li>
			<li class="nav-item <?php echo ($navigationURL=='admin_list' || $navigationURL=='add_admin' || $navigationURL=='edit_admin' || $navigationURL=='category_list' || $navigationURL=='add_category' || $navigationURL=='edit_category')?'active open':''; ?>">
				<a href="javascript:void(0);" class="nav-link nav-toggle">
					<i class="icon-setting"></i> 
					<span class="title">Settings</span>
					<span class="arrow"></span>
					<span class="selected"></span>
				</a>
				<ul class="sub-menu">
					<li class="nav-item start <?php echo ($navigationURL=='admin_list')?'active open':''; ?>">
						<a href="<?php echo base_url().'admin/settings/admin_list'; ?>" class="nav-link">
							<span class="title">Admin List</span>
						</a>
					</li>
					<li class="nav-item <?php echo ($navigationURL=='add_admin' || $navigationURL=='add_admin' )?'active open':''; ?>">
						<a href="<?php echo base_url().'admin/settings/add_admin'; ?>" class="nav-link">
							<span class="title">Add Admin</span>
						</a>
					</li>
					<li class="nav-item <?php echo ($navigationURL=='category_list' || $navigationURL=='category_list' || $navigationURL=='add_category' || $navigationURL=='edit_category')?'active open':''; ?>">
						<a href="<?php echo base_url().'admin/category/category_list'; ?>" class="nav-link">
							<span class="title">Category List</span>
						</a>
					</li>
				</ul>
			</li>
		</ul>
	</div>
</div>
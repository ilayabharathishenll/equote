<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/tenders.png" class="imgbasline"> Add Category
            </div>
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <form name="add_category" id="add_category" class="horizontal-form" method="POST" enctype="multipart/form-data">
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Category Name  <span class="redstar">*</span></label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="category_name" id="category_name" placeholder="">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Category Code  <span class="redstar">*</span></label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="cat_number" id="cat_number" placeholder="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-actions formbtncenter">
                	<button type="submit" name="submit" value="submit" class="btn green customsavebtn">
                        <i class="fa fa-check"></i> Save Data
                    </button>
                    <a href="category_list" type="button" class="btn red customrestbtn backbtn"> <i class="fa fa-angle-left"></i> Back</a>
                </div>
            </form>
            <!-- END FORM-->
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function(){
        $("#add_category").validate( {
            rules: {
                category_name:{required:true},
                cat_number:{required:true},
            },
            messages: {
                category_name:{required:"Please enter the category name",lettersonly:"Please enter the alphabet letters only"},
                cat_number:{required:"Please enter the category code",number:"Please enter number only"},
            },
        });
    });
     jQuery.validator.addMethod("lettersonly", function(value, element) {
      return this.optional(element) || /^[a-z," "]+$/i.test(value);
    }, "Please enter the alphabet letters only"); 
</script>
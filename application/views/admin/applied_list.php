<?php
	$search_rfq = $search_code = $search_name = $search_partno='';
	if (!empty($search)) {
		$search_rfq = $search['rfq_no'];
		$search_code = $search['vendor_code'];
		$search_name = $search['vendor_name'];
		$search_partno = $search['part_no'];
	}
?>
<style>
	#invitetbl thead tr th {
    text-align: center;
    font-size: 12.5px;
}
</style>
<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/applied_tenders.png" class="imgbasline"> Applied E Quote
            </div>
            <div class="actions">
            	<a href="<?php echo base_url(); ?>admin/tender/applied_tender" type="button" class="btn red customrestbtn backbtn"> <i class="fa fa-angle-left"></i> Back</a>
            </div>
        </div>
        <div class="portlet-body">
        	<form name="frm_appliedlist" id="frm_appliedlist" method="POST">
		        <div class="row">
		        	<div class="col-md-12 paddingleftright">
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                           <input type="text" class="form-control" name="appliedvendor[rfq_no]" id="rfq_no" placeholder="RFQ No" value="<?php echo $search_rfq ?>">
	                        </div>
		        		</div>
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                           <input type="text" class="form-control" name="appliedvendor[part_no]" id="part_no" placeholder="Part Number" value="<?php echo $search_partno ?>">
	                        </div>
		        		</div>
		        		<!-- <div class="col-md-2 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                            <input type="text" class="form-control" name="appliedvendor[vendor_code]" id="vendor_code" placeholder="Vendor Code" value="<?php echo $search_code ?>">
	                        </div>
		        		</div>
		        		<div class="col-md-2 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                           <input type="text" class="form-control" name="appliedvendor[vendor_name]" id="vendor_name" placeholder="Vendor Name" value="<?php echo $search_name ?>">
	                        </div>
		        		</div> -->
		        		<div class="col-md-3">
		        			<div class="col-md-12 paddingleftright">
		        				<button type="submit" class="btn btn-warning customsearchtbtn"> <i class="fa fa-search"></i> Search</button>
		        				<a href="<?php echo base_url()."admin/tender/applied_list/".$this->uri->segment(4).""?>" type="button" class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Reset</a>
		        			</div>
		        		</div>
		        	</div>
		        </div>
	        	<div class="table-responsive" style="overflow-x: inherit;margin-top:0px;">
		            <table class="table table-striped table-bordered table-hover" id="invitetbl">
		            	<thead>
		                    <tr>
		                    	<th> SI.NO </th>
		                        <th> RFQ NO </th>
		                        <th> Part No </th>
		                        <th> Rating </th>
		                        <th> Vender Code</th>
		                        <th> Vendor Name </th>
		                        <th> E Quote Base Price</th>
		                        <th> Quoted Price </th>
		                        <th> Action </th>
		                    </tr>
		                </thead>
		                </tbody>
		                    <?php
		                      if (count($getvendorList,COUNT_RECURSIVE)>1) {
		                      	$sno=1;
		                      	foreach ($getvendorList as $appliedData) {
		                      		$currentDate = date("Y-m-d");
		                      		$currentTime = date("H:i");
		                      		$ratings=($appliedData["rating"]!="")?$appliedData["rating"]:"0";
                                	$orderid="order_". $sno;
		                    	    $vendorderRating="<script>
                               		$('#".$orderid."').rateYo({rating: $ratings,readOnly: true,starWidth: '15px'});
                            </script>";
		                    ?>
		                    <tr>
		                    	<td><?php echo $sno;?></td>
		                        <td><?php echo $appliedData["ref_no"]?></td>
                                <td><?php echo $appliedData["part_no"]?></td>
		                       
		                        <td><div id='order_<?php echo $sno ?>' style="margin: auto;text-align: center;"><?php echo $vendorderRating; ?></div></td>
		                        <?php
		                        if (date("Y-m-d",strtotime($appliedData["end_date"])) <= $currentDate && date("H:i",strtotime($appliedData["end_time"])) <= $currentTime) {
		                        ?>
		                            <td><?php echo $appliedData["vendor_code"]?></td>
		                            <td><?php echo $appliedData["vendor_name"]?></td>
		                        <?php
		                        } else {
		                        ?>
		                           <td> - </td>
                                   <td> - </td>
		                        <?php
		                        } ?>
		                       
		                        <td><?php echo $appliedData["base_price"]?></td>
		                        <?php 
		                        if (date("Y-m-d",strtotime($appliedData["end_date"])) <= $currentDate && date("H:i",strtotime($appliedData["end_time"])) <= $currentTime) {
		                        ?>
		                       
			                    <td><?php echo $appliedData["applyprice"]?></td>
			                    
					            <td class="action" style="text-align:left;display:flex;">
				                    <?php
				                    if (!empty($appliedData["applyprice"])) {
				                    ?>
		                    			<a href="<?php echo base_url().'admin/tender/applied_list/'.$appliedData["tender_id"].'/'.$appliedData["application_id"].''; ?>" onclick="return confirm('Are you sure want to award this tender?');" type="button" class="btn green btn-xs customactionbtn"> <i class="icon-badge"></i> Award</a>
				                    <?php
				                    } else {
				                    ?>
		                                <a href="Javascript:void(0);" type="button" class="btn grey-cascade btn-xs custominvitebtn"> <i class="icon-badge"></i> Award</a>
				                    <?php
				                    }
				                    ?>
				                    <?php
				                    if (!empty($appliedData["applyprice"])) {
				                    ?>
				                    <a data-toggle="modal" data-value="view" data-target="#biddingCount" type="button" class="btn btn-primary btn-xs custominvitebtn"> <i class="fa fa-comments-o"></i> Bidding Log
	                                	<input id="vendor_name_visible" type="hidden" value="<?php echo $appliedData["vendor_name"]?>">
                                		<input id="bidding_count_visible" type="hidden" value="<?php echo $appliedData["bidding_count"]?>">
                                		<input id="vendor_code_visible" type="hidden" value="<?php echo $appliedData["vendor_code"]?>">
                                		<input id="quote_price_visible" type="hidden" value="<?php echo $appliedData["applyprice"]?>">
                                		<table class="hidden table table-striped table-bordered table-hover" id="bidding_log">
							            	<thead>
							                    <tr>
							                    	<th> SI.NO </th>
							                        <th> Date </th>
							                        <th> Price </th>
							                    </tr>
							                </thead>
							                <tbody>
						                    <?php
						                      if (count($appliedData['bidding_log'],COUNT_RECURSIVE)>1) { 
												$no = 1;
												foreach ($appliedData['bidding_log'] as $bidding_log) {
													$date = date_create($bidding_log["date"]);
												?>
													<tr>
														<td><?php echo $no;?></td>
														<td><?php echo date_format($date, "Y/m/d H:i:s")?></td>
														<td><?php echo $bidding_log["price"]?></td>
													</tr>
												<?php 
												$no++;
												}
											}
						                      	?>
							                </tbody>
						                </table>
                            		</a>
                            		<?php
                            	    }
                            		?>
					            </td>
				                <?php
				                } else {
                                ?>
                                <td> - </td>
                                
                    			<td class="action" style="text-align:center;">
	                                <a href="Javascript:void(0);" type="button" class="btn grey-cascade btn-xs custominvitebtn"> <i class="icon-badge"></i> Award</a>
	                                <!-- <a data-toggle="modal" data-value="view" data-target="#biddingCount" type="button" class="btn btn-primary btn-xs custominvitebtn"> <i class="fa fa-comments-o"></i> Bidding Log
	                                	<input id="vendor_name_visible" type="hidden" value="<?php echo $appliedData["vendor_name"]?>">
                                		<input id="bidding_count_visible" type="hidden" value="<?php echo $appliedData["bidding_count"]?>">
                                		<input id="vendor_code_visible" type="hidden" value="<?php echo $appliedData["vendor_code"]?>">
                                		<input id="quote_price_visible" type="hidden" value="<?php echo $appliedData["applyprice"]?>">
                                		<table class="hidden table table-striped table-bordered table-hover" id="bidding_log">
							            	<thead>
							                    <tr>
							                    	<th> SI.NO </th>
							                        <th> Date </th>
							                        <th> Price </th>
							                    </tr>
							                </thead>
							                <tbody>
						                    <?php
						                      if (count($appliedData['bidding_log'],COUNT_RECURSIVE)>1) { 
												$no = 1;
												foreach ($appliedData['bidding_log'] as $bidding_log) {
													$date = date_create($bidding_log["date"]);
												?>
													<tr>
														<td><?php echo $no;?></td>
														<td><?php echo date_format($date, "Y/m/d")?></td>
														<td><?php echo $bidding_log["price"]?></td>
													</tr>
												<?php 
												$no++;
												}
											}
						                      	?>
							                </tbody>
						                </table>
                            		</a> -->
	                        	</td>
	                        	<?php
	                        	}
	                        	?>
		                    </tr>
		                    <?php
		                        $sno++;
		                        } 
		                    }
		                    ?>
		                </tbody>
		            </table>
		        </div>
		    </form>
        </div>
    </div>
</div>
<div class="modal fade in" id="biddingCount" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><b>Vendor Bidding Status</b></h4>
            </div>
            <div class="modal-body"> 
	             <div class="row">
	             	<div class="col-md-11" style="margin-top: 15px;">
	             		<div class="form-group">
	             			<label class="control-label col-md-4">Vendor Name:</label>
	             			<div class="col-md-8"> <span id="vendor_hidden_name" style="font-weight: bold;"></span></div>
	             		</div>
	             	</div>
	             	<div class="col-md-11" style="margin-top: 15px;">
	             		<div class="form-group">
	             			<label class="control-label col-md-4">Vendor Code:</label>
	             			<div class="col-md-8" style="font-weight: bold;"> <span id="vendor_hidden_code"></span></div>
	             		</div>
	             	</div>
	             	<div class="col-md-11" style="margin-top: 15px;">
	             		<div class="form-group">
	             			<label class="control-label col-md-4">Current Price:</label>
	             			<div class="col-md-8" style="font-weight: bold;"> <span id="vendor_quote_price"></span></div>
	             		</div>
	             	</div>
	             	<div class="col-md-11" style="margin-top: 15px;">
	             		<div class="form-group">
	             			<label class="control-label col-md-4">Bidding Changes:</label>
	             			<div class="col-md-8" style="font-weight: bold;"> <span id="vendor_bidding_count"></span> Time(s)</div>
	             		</div>
	             	</div>
	             	<div class="col-md-12" style="margin-top: 15px;">
	             		<div>
	             			<table id="vendor_bidding_log" class="table table-striped table-bordered table-hover"></table>
	             		</div>
	             	</div>
	             </div>
            </div>
       </div>
   </div>
</div>

<script type="text/javascript">
	$(document).on('click','#invitetbl .action a.btn-primary', function(){
		name = $(this).children('#vendor_name_visible').val();
		code = $(this).children('#vendor_code_visible').val();
		count = $(this).children('#bidding_count_visible').val();
		price = $(this).children('#quote_price_visible').val();
		bidding_log = $(this).children('table.hidden').html();
		$('#vendor_hidden_name').text(name);
		$('#vendor_hidden_code').text(code);
		$('#vendor_bidding_count').text(count);
		$('#vendor_quote_price').text(price);
		$('#vendor_bidding_log').html(bidding_log);
	});
	// $('#vendor_bidding_log').DataTable({
	// 	"bPaginate": false,
	// 	"bLengthChange": false,
	// 	"bFilter": false,
	// 	"bInfo": false,
	// 	"iDisplayLength":5,
	// 	"ordering": false
	// });
</script>
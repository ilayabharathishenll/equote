<?php //print_r($data);exit;?>
<div class="page-content">
	<?php
		$msg=$this->session->flashdata('success');
		if(!empty($msg)){
		?>
			<div>
				<div class="alert alert-success alert-dismissible">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<?php echo $msg ?>
				</div>
			</div>
		<?php
		}
	?>
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/vendors.png" class="imgbasline"> Category List</div>
				<div class="actions">
					<a href="<?php echo base_url(); ?>admin/category/add_category" class="btn green btn-sm customaddbtn">
						<i class="fa fa-plus"></i> Add Category</a>
				</div>
        </div>
        <div class="portlet-body">
        	<div class="row">
	        	<div class="col-md-12 paddingleftright">
	        		<form method="post">
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                            <input type="text" class="form-control" name="category_name" id="category_name" placeholder="Category Name" value="<?php echo $this->input->get('category_name')!=''?$this->input->get('category_name'):'';?>">
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                            <input type="text" class="form-control" name="category_code" id="category_code" placeholder="Category Code" value="<?php echo $this->input->get('category_code')!=''?$this->input->get('category_code'):'';?>">
                        </div>
	        		</div>
	        		<div class="col-md-3">
	        			<div class="col-md-12 paddingleftright">
	        				<button type="submit" class="btn btn-warning customsearchtbtn" name="search" value="search"> <i class="fa fa-search"></i> Search</button>
	        				<?php if($this->input->get('search')==1){ ?>
	        				<a href="<?php echo base_url('admin/category/category_list'); ?>" class="btn red customrestbtn"><i class="fa fa-refresh"></i> Reset</a>
	        				<?php }else{ ?>
	        				<button type="button" class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Reset</button>
	        				<?php } ?>

	        			</div>
	        		</div>
	        	</form>
	        	</div>
	        </div>
        	<div class="table-responsive" style="overflow-x: inherit;margin-top:0px;">
	            <table class="table table-striped table-bordered table-hover admintbl" id="vendor-list">
	            	<thead>
	                    <tr>
	                        <th> SI.NO </th>
	                        <th> Category Name </th>
	                        <th> Code </th>
							<th> Status </th>
	                        <th> Action </th>
	                    </tr>
	                </thead>
	                </tbody>
                 <?php
                  $i=1;
                  if(!empty($data)){
                  foreach ($data as $value) { ?>
                   <tr>
                         <td><?php echo $i; ?></td>
                         <td><?php echo $value->category_name;?></td>
                         <td><?php echo $value->cat_number;?></td>
                         <td><?php echo $value->status;?></td>
                         <td><a href="<?php echo base_url('admin/category/edit_category');?>/<?php echo $value->category_id;?>" type="button" class="btn grey-cascade btn-xs custominvitebtn"><i class="fa fa-edit"></i> Edit</a> <a href="<?php echo base_url('admin/category/delete_category');?>/<?php echo $value->category_id;?>" type="button" class="btn green btn-xs customactionredbtn" onclick="return confirm('Do you want to delete this category?')"><i class="fa fa-trash"></i> Delete</a> </td>
                      </tr>
                   
                 <?php $i++;}}
                  ?>
                     
                 </tbody>
	            </table>
	        </div>
        </div>
    </div>
</div>
<!-- END CONTENT BODY -->

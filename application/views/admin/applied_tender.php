<?php
	$search_startdate = $search_enddate= $search_rfq = $search_tender = $search_part = $search_partno='';
	if (!empty($search)) {
		$search_startdate = $search['start_date'];
		$search_enddate = $search['end_date'];
		$search_rfq = $search['rfq_no'];
		$search_tender = $search['tender_title'];
		$search_part = $search['part_name'];
		$search_partno = $search['part_no'];
	}
	//print_r($getTenderList);
?>
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/applied_tenders.png" class="imgbasline"> Applied E Quote
            </div>
        </div>
        <div class="portlet-body">
        	<form id="frmapplytender" name="frmapplytender" method="POST">
		        <div class="row">
		        	<div class="col-md-12 paddingleftright">
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                        	<input type="text" class="form-control" name="applied[start_date]" id="start_date" autocomplete="off" Placeholder="E Quote Start Date" value="<?php echo $search_startdate ?>" data-date-format="dd/mm/yyyy">
	                        </div>
		        		</div>
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                            <input type="text" class="form-control" name="applied[end_date]" id="end_date" autocomplete="off" Placeholder="E Quote End Date" value="<?php echo $search_enddate ?>" data-date-format="dd/mm/yyyy">
	                        </div>
		        		</div>
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                           <input type="text" class="form-control" name="applied[rfq_no]" id="rfq_no" placeholder="RFQ No" value="<?php echo $search_rfq ?>">
	                        </div>
		        		</div>
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                            <input type="text" class="form-control" name="applied[part_name]" id="part_name" placeholder="Part Name" value="<?php echo $search_part ?>">
	                        </div>
		        		</div>
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                            <input type="text" class="form-control" name="applied[part_no]" id="part_no" placeholder="Part Number" value="<?php echo $search_partno ?>">
	                        </div>
		        		</div>
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                           <input type="text" class="form-control" name="applied[tender_title]" id="tender_title" placeholder="E Quote Name" value="<?php echo $search_tender ?>">
	                        </div>
		        		</div>
		        		<div class="col-md-3">
		        			<div class="col-md-12 paddingleftright">
		        				<button type="submit" class="btn btn-warning customsearchtbtn"> <i class="fa fa-search"></i> Search</button>
		        				<a href="<?php echo base_url()."admin/tender/applied_tender";?>" type="button" class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Reset</a>
		        			</div>
		        		</div>
		        	</div>
		        </div>
	        	<div class="table-responsive" style="overflow-x: inherit;margin-top:0px;">
		            <table class="table table-striped table-bordered table-hover admintbl" id="applied-tender">
		            	<thead>
		                    <tr>
		                    	<th> SI.NO </th>
		                        <th> RFQ NO </th>
		                        <th> E Quote Name</th>
		                        <th> Part Name</th>
		                        <th> Part Number</th>
		                        <th> No of Applied</th>
		                        <th> Action </th>
		                    </tr>
		                </thead>
		                </tbody>
	                        <?php
	                        if(count($getTenderList>0)) {
	                        	$sno=1;
	                        	foreach ($getTenderList as $getTenderDate) {
	                        	?>
	                        	<tr>
			                    	<td><?php echo $sno ?></td>
			                        <td><?php echo $getTenderDate["ref_no"]?></td>
			                        <td><?php echo $getTenderDate["tender_title"]?></td>
			                        <td><?php echo $getTenderDate["part_name"]?></td>
			                        <td><?php echo $getTenderDate["part_no"]?></td>
			                        <td><?php echo $getTenderDate["applycount"]?></td>
			                        <td><a href="applied_list/<?php echo $getTenderDate["tender_id"];?>" type="button" class="btn green btn-xs customactionbtn"><i class="fa fa-eye"></i> View </a> </td>
		                    	</tr>
		                    	<?php
		                    	$sno++;
	                        	}
	                        }
	                        ?>
		                </tbody>
		            </table>
		        </div>
		    </form>
        </div>
    </div>
</div>

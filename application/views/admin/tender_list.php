<?php //print_r($data);exit;?>
<div class="page-content">
	<?php
		$msg=$this->session->flashdata('success');
		if(!empty($msg)){
		?>
			<div>
				<div class="alert alert-success alert-dismissible">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<?php echo $msg ?>
				</div>
			</div>
		<?php
		}
	?>
    <div class="portlet box blue boardergrey">

        <div class="portlet-title">
            <div class="caption">
                <img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/tenders.png" class="imgbasline"> Admin Announced List of E Quote
            </div>
        </div>
        <div class="portlet-body">
	        <div class="row">
	        	<div class="col-md-12 paddingleftright">
	        		<form method="POST" action="<?php echo base_url('admin/tender/export_excel'); ?>">
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                        	<input type="text" class="form-control" name="rfq_no" id="rfq_no" placeholder="Enter RFQ No">
                        </div>
	        		</div>
	        		<div class="col-md-2 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                            <button type="submit"  class="btn red customrestbtn tendersearch" action><i class="fa fa-file-excel-o"></i> Export to Excel</button>
                        </div>
	        		</div>
	        		</form>
	        		<form method="POST" action="#">
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
	        				<?php ?>
                           <input type="text" class="form-control" name="rfq_no" id="rfq_no" placeholder="RFQ No" value="<?php echo $this->input->get('no')!=''?$this->input->get('no'):'';?>">
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                           <input type="text" class="form-control" name="tender_name" id="tender_name" placeholder="E Quote Name" value="<?php echo $this->input->get('title')!=''?$this->input->get('title'):'';?>">
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                           <input type="text" class="form-control" name="partno" id="partno" placeholder="Part No" value="<?php echo $this->input->get('partno')!=''?$this->input->get('partno'):'';?>">
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                           <select class="form-control" name="status">
                           		<option value="">--Select Status--</option>
                           		<option value="Active" <?php echo $this->input->get('status')=='Active' ? 'Selected' : ''?>>Active</option>
                           		<option value="Awarded" <?php echo $this->input->get('status')=='Awarded' ? 'Selected' : ''?>>Awarded</option>
                           		<option value="Open" <?php echo $this->input->get('status')=='Open' ? 'Selected' : ''?>>Open</option>
                           		<option value="Closed" <?php echo $this->input->get('status')=='Closed' ? 'Selected' : ''?>>Closed</option>
                           		<option value="Pending" <?php echo $this->input->get('status')=='Pending' ? 'Selected' : ''?>>Pending</option>
                           		<option value="Cancelled" <?php echo $this->input->get('status')=='Cancelled' ? 'Selected' : ''?>>Cancelled</option>
                           </select>
                        </div>
	        		</div>
	        		<div class="col-md-3">
	        			<div class="col-md-12 paddingleftright">
	        				<button type="submit" class="btn btn-warning customsearchtbtn" name="search" value="search"> <i class="fa fa-search"></i> Search</button>
	        				<?php if($this->input->get('search')==1){ ?>
	        					<a href="<?php echo base_url('admin/tender/tender_list'); ?>" class="btn red customrestbtn"><i class="fa fa-refresh"></i> Reset</a>
	        				<?php }else{ ?>
	        					<button type="button" class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Reset</button>
	        				<?php } ?>
	        			</div>
	        		</div>
	        	</form>
	        	</div>
	        </div>
        	<div class="table-responsive" style="overflow-x: inherit;margin-top:0px;">
	            <table class="table table-striped table-bordered table-hover admintbl" id="tendertbl">
	            	<thead>
	                    <tr>
	                        <th> SI.NO </th>
	                        <th> RFQ NO </th>
	                        <th> E Quote Name </th>
	                        <th> Part Name </th>
	                        <th> Category Name </th>
	                        <th> Status </th>
	                        <th class="actionborder"></th>
	                        <th class="actionborder"></th>
	                        <th class="actionborder">Action</th>
	                        <th></th>
	                    </tr>
	                </thead>
	                </tbody>
	                <?php
	                	$i=1;
	                	if(!empty($data)){
	                	foreach ($data['tender'] as $value) {
	                	?><tr>
	                        <td><?php echo $i; ?></td>
	                        <td><?php echo $value->ref_no;?></td>
	                        <td><?php echo !empty($value->tender_title)?$value->tender_title:"--";?></td>
	                        <td> <?php echo !empty($value->part_name)?$value->part_name:"--";?></td>
	                        <?php if(!empty($data['category'])){
	                        	foreach ($data['category'] as $category) {
	                        		if($value->commodity_id==$category->category_id) {?>
	                        			<td><?php echo $category->category_name;?></td>
	                        		<?php }
	                        	}
	                        } ?>
	                       	<?php if(strtolower($value->status) =='awarded') { ?>
								<td><span class="label label-sm label-success labelboader"> Awarded</span></td>
	                        <?php }
	                        	elseif(strtolower($value->status) =='open'){ ?>
	                        	<td><span class="label label-sm label-info labelboader" style="padding: 2px 15px;"> Open </span></td>
	                        <?php } elseif(strtolower($value->status) =='closed'){ ?>
	                        	<td><span class="label label-sm label-danger labelboader" style="padding: 2px 12px;"> Closed </span></td>
	                        <?php }  elseif(strtolower($value->status) =='pending'){ ?>
	                        	<td><span class="label label-sm label-warning labelboader" style="padding: 2px 9px;"> Pending </span> </td>
	                        <?php } elseif(strtolower($value->status) =='cancelled'){ ?>
	                        	<td><span class="label label-sm label-default labelboader" style="padding: 2px 9px;"> Cancelled </span> </td>
	                        <?php } else {?>
	                        	<td></td>
	                        <?php } ?>
	                        <td><?php 
	                        	if(strtolower($value->status) == "pending" || strtolower($value->status) == "open"){ ?>
	                        		<a href="<?php echo base_url('admin/tender/edit_rfq');?>?id=<?php echo $value->tender_id;?>" type="button" class="btn grey-cascade btn-xs custominvitebtn"><i class="fa fa-edit"></i> Edit</a> 
	                        <?php	} else { ?>
	                        	    <div class="isDisabled"><a href="javascript:void(0);" type="button" class="btn grey-cascade btn-xs custominvitebtn" disabled><i class="fa fa-edit"></i> Edit</a> </div>
	                        <?php	}?>
	                        </td>
	                        	<td><a href="<?php echo base_url('admin/tender/view_tender');?>?id=<?php echo $value->tender_id;?>" type="button" class="btn green btn-xs customactionbtn"><i class="fa fa-eye"></i> View</a></td>
	                        	<td><?php if(strtolower($value->status)  == "pending" || strtolower($value->status)  == "open" || strtolower($value->status)  =="cancelled" ||  strtolower($value->status)  =="active"){ ?>
	                        		<a href="javascript:void(0);" data-id="<?php echo $value->tender_id;?>" data-type="cancel" type="button" class="btn red btn-xs customactionredbtn" id="cancellationpopup"><i class="fa fa-close"></i> Cancellation</a>

	                        <!-- <a href="<?php echo base_url('admin/tender/delete');?>?id=<?php echo $value->tender_id;?>" type="button" class="btn red btn-xs customactionredbtn" onclick="return confirm('Do you want to delete this tender?')"><i class="fa fa-close"></i> Cancellation</a> --><?php } else {?> 
	                        	  <div class="isDisabled"><a href="javascript:void(0);"  type="button" class="btn red btn-xs customactionredbtn" disabled><i class="fa fa-close"></i> Cancellation</a></div>
	                        	<?php } ?>
                                    
	                            </td>
	                            <td><a dataid="<?php echo $value->tender_id;?>" datatender="<?php echo $value->tender_title ?>" data-value="log" type="button" class="btn btn-primary btn-xs custominvitebtn" id="eqouteLogbtn"> <i class="fa fa-comments-o"></i> Log</a></td>
	                    	</tr>
	                <?php $i++;}}
	                 ?>
	                </tbody>
	            </table>
	        </div>
        </div>
    </div>
</div>
<div class="modal fade in" id="cancelpopup" tabindex="-1" role="basic" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
        	<form class="tender_cancel" id="tender_cancel"  method="POST">
        		<input type="hidden" name="hnd_id" id="hnd_id">
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
	                <h4 class="modal-title"><b>E-Quote Cancellation</b></h4>
	            </div>
	            <div class="modal-body"> 
		          
		            <div class="row" id="non_upload_doc">
		             	<div class="col-md-12" style="margin-top: 10px;margin-bottom: 15px;">
		             		<div class="form-group">
		                        <label class="control-label col-md-12">Reason: <span class="redstar">*</span></label>
		                        <div class="col-md-12 vendor-detail">
		                        	<textarea class="form-control" cols="5" rows="5" name="reason" id="reason"></textarea>
		                        	<label class="error reasonmsg" style="display:none">Please enter the cancel reason</label>
		                        </div>

		                    </div>
		             	</div>
		            </div>

	                <div class="modal-footer" id="modal-footer">
		            	<button type="button" name="submit" value="submit" class="btn green customaddbtn" id="popupstatuschange"> <i class="fa fa-check"></i> Save </button>
		            	<button type="button" class="btn red customrestbtn"  data-dismiss="modal"> <i class="fa fa-close"></i> Close</button>
	                </div>
	            </div>
            </form>
        <!-- /.modal-content -->
       </div>
    <!-- /.modal-dialog -->
   </div>
</div>

<div class="modal fade in" id="Adminlogpopup" tabindex="-1" role="basic" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
        	<form class="tender_cancel" id="tender_cancel"  method="POST">
        		<input type="hidden" name="hnd_id" id="hnd_id">
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
	                <h4 class="modal-title"><b>Admin Changes Log</b></h4>
	            </div>
	            <div class="modal-body"> 
	            	<h4 style="font-size:14px;margin-top: -4px;"><b>E-Quote Name:</b> <span id="titleapp"></span></h4>
		             <div class="table-responsive" style="overflow-x: inherit;margin-top:0px;">
	            		<table class="table table-striped table-bordered table-hover">
	            			<thead>
			            		<tr>
				            		<th>SI.No</th>
				            		<th>Name</th>
				            		<th>Log Status</th>
				            		<th>Date</th>
				            	</tr>
	            			</thead>
	            			<tbody id="appendAdminlogtbl">
	            			</tbody>
            			</table>
            		</div>
	                <div class="modal-footer" id="modal-footer">
		            	<button type="button" class="btn red customrestbtn"  data-dismiss="modal"> <i class="fa fa-close"></i> Close</button>
	                </div>
	            </div>
            </form>
        <!-- /.modal-content -->
       </div>
    <!-- /.modal-dialog -->
   </div>
</div>
<script>
	$(document).ready(function() {
		automaticUpdatetenderAjax();
		setTimeout(function(){ location.reload(); },150000);
	});
    function automaticUpdatetenderAjax() {
    	//console.log("test");
    	$.ajax({
			url: base_url+'admin/tender/automaticTenderUpdate',
			type: 'POST',
			data: {
				'post-type': 'automaticupdate'
			},
			success: function(response) {
				if (response == 'success') {
					// location.reload();
				}
			}
		});
    }
    $(document).on("click","#cancellationpopup",function(){
    	var tenderid  = $(this).attr("data-id");
    	var tendertype =$(this).attr("data-type");
		var msg;
		var confirmcheck;
		if (tendertype=="cancel") {
			msg='Are you sure you want to cancel this E-Quote?';
			confirmcheck = confirm(msg);
        } else {
        	confirmcheck = true;
        }
        if(tenderid!="" && confirmcheck == true && tendertype=="cancel") {
        	$("#hnd_id").val(tenderid);
        	$("#cancelpopup").modal("show");
        	if(tenderid!=""){
		    	$.ajax({
					url: base_url+'admin/tender/getCancelReasonTender',
					type: 'POST',
					data: {
						'post-type': 'getreason',
						'tenderid':tenderid,
					},
					success: function(response) {
						var data=response.split("#^^#");
						if (data[0] == 'success') {
							$("#reason").empty().val(data[1]);
						}
					}
				});
	    	}
		} else {
			return false;
		}
    });
    $(document).on("click", "#popupstatuschange" ,function() { 
    	var tenderid= $("#hnd_id").val();
    	var reason= $("#reason").val();
    	if(reason==""){
    		$(".reasonmsg").css("display","block");
    		return false;
    	} else {
    		$(".reasonmsg").css("display","none");
    	}
    	if(reason!=""){
    		$.ajax({
			url: base_url+'admin/tender/CancelTenderUpdate',
			type: 'POST',
			data: {
				'post-type': 'cancelupdate',
				'tenderid':tenderid,
				'reason':reason
			},
			success: function(response) {
				if (response == 'success') {
					$("#cancelpopup").modal("hide");
					location.reload();
				}
			}
		});
    	}
    });
    $(document).on("change blur keyup", "#reason" ,function() { 
    	var reason= $("#reason").val();
    	if(reason==""){
    		$(".reasonmsg").css("display","block");
    		return false;
    	} else {
    		$(".reasonmsg").css("display","none");
    	}

    });

    $ (document).on("click","#eqouteLogbtn",function (){
        var tenderid  = $(this).attr("dataid");
    	var tendervalue =$(this).attr("data-value");
    	var datatender =$(this).attr("datatender");
    	
    	//$("#Adminlogpopup").modal("show");
    	if (tenderid != "" && tendervalue == "log") {
	    		$.ajax({
				url: base_url+'admin/tender/logAdminTender',
				type: 'POST',
				//dataType:"html",
				data: {
					'post-type': 'tenderlogadmin',
					'tenderid':tenderid,
				},
				dataType: 'text',
				success: function(response) {
					//console.log(response);
					//response="success";##**##
					var tenderData=response.split("##**##");
					//console.log(tenderData);
					if (tenderData[0] == 'success') {
						$("#titleapp").empty().append(datatender);
						if (tenderData[1]=="") {
                           $("#appendAdminlogtbl").empty().append('<tr><td colspan="5" style="text-align:center;"> No records Found</td></tr>');
						} else {
                           $("#appendAdminlogtbl").empty().append(tenderData[1]);
						}
						
						
						$("#Adminlogpopup").modal("show");
					} else{
						$("#Adminlogpopup").modal("hide");
					}
				}
			});
    	}
    });
</script>
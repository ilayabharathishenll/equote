<?php
error_reporting(0);
?>
<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/tenders.png" class="imgbasline"> View E Quote Details</div>
            <div class="actions">
                <a href="<?php echo base_url(); ?>admin/invite/tender_invite" class="btn red btn-sm customrestbtn">
                    <i class="fa fa-angle-left"></i> Back</a>
            </div>
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <form name="frm_rfq" id="frm_rfq" action="tender_invite" class="ind-quote horizontal-form" method="POST">
                <div class="form-body">
                    <h3 class="form-section formheading">E Quote Basic Detail</h3>
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">RFQ Number</label>
                                <div class="col-md-8">
                                    : <?php echo $viewTender["ref_no"];?>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Part Name</label>
                                <div class="col-md-8">
                                    : <?php echo $viewTender["part_name"];?>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->

                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Delivery Duration</label>
                                <div class="col-md-8">
                                    : <?php echo $viewTender["delivery_duration"];?>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Delivery Date</label>
                                <div class="col-md-8">
                                    : <?php echo $viewTender["delivery_date"];?>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->

                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Select Qunatity</label>
                                <div class="col-md-8">
                                    : <?php echo $viewTender["quantity"];?>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4"> Part Price</label>
                                <div class="col-md-8">
                                    : <?php echo $viewTender["part_price"];?>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->

                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">E Quote Title</label>
                                <div class="col-md-8">
                                   : <?php echo $viewTender["tender_title"];?>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Upload drawing & Technical Spes</label>
                                <div class="col-md-8">
                                 <?php if (!empty($viewTender["drawing_doc"])) {?>
                                    <a class="download_doc" href="<?php echo base_url()."uploads/".$viewTender["drawing_doc"];?>" target="_blank">:<img src="<?php echo base_url()."assets/layouts/layout/img/dow.png" ?>" style="margin-bottom: 10px;">Click here to download</a>
                                    <?php } else {
                                        echo ':-';
                                    } ?>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->

                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Norm</label>
                                <div class="col-md-8">
                                   : <?php echo $viewTender["norm"];?>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">RM Price</label>
                                <div class="col-md-8">
                                   : <?php echo $viewTender["rm_price"];?>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-12 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-12">E Quote Details:</label>
                                    <div class="col-md-12">
                                    <?php echo $viewTender["tender_detail"];?>
                                    </div>
                            </div>
                            
                        </div>
                    </div>
                    <!--/row-->

                    <h3 class="form-section formheading">Upload E Quote Documents</h3>
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <?php
                                if(count($viewTender["upload_terms"],COUNT_RECURSIVE)>1) {
                                    $termcnt=1;
                                    foreach ($viewTender["upload_terms"] as $termskey) {
                                        foreach ($termskey as $keys => $DocTermsvalue) {
                                            if ($termcnt==1) {
                                                echo '<label class="control-label col-md-5" style="padding-right: 0px;">E Quote Terms and Condition :</label>';
                                            } else {
                                                echo '<label class="control-label col-md-5" style="padding-right: 0px;"></label>';
                                            }
                                        ?>
                                            <div class="col-md-5">
                                                <a class="download_doc" href="<?php echo base_url()."uploads/".$termskey["doc_name"]?>" target="_blank"><img src="<?php echo base_url()."assets/layouts/layout/img/dow.png" ?>" style="margin-bottom: 10px;">Click here to download</a>
                                            </div>
                                            <div class="col-md-2">
                                            </div>
                                        <?php
                                        }
                                        $termcnt++;
                                    }
                                } else {
                                    echo '<label class="control-label col-md-5" style="padding-right: 0px;">E Quote Terms and Condition :</label><div class="col-md-5">
                                        <a href="">-</i></a>
                                        </div>
                                        <div class="col-md-2">
                                        </div>';
                                }
                                ?>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <?php
                                if(count($viewTender["upload_helpdoc"],COUNT_RECURSIVE)>1) {
                                    $helpcnt=1;
                                    foreach ($viewTender["upload_helpdoc"] as $helpkey) {
                                        foreach ($helpkey as $keys => $Dochelpvalue) {
                                            if ($helpcnt==1) {
                                                echo '<label class="control-label col-md-5" style="padding-right: 0px;">Help Document :</label>';
                                            } else {
                                                echo '<label class="control-label col-md-5" style="padding-right: 0px;"></label>';
                                            }
                                        ?>
                                            <div class="col-md-5">
                                                <a class="download_doc" href="<?php echo base_url()."uploads/".$helpkey["doc_name"]?>" target="_blank"><img src="<?php echo base_url()."assets/layouts/layout/img/dow.png" ?>" style="margin-bottom: 10px;">Click here to download</a>
                                            </div>
                                            <div class="col-md-2">
                                            </div>
                                        <?php
                                        }
                                        $helpcnt++;
                                    }
                                } else {
                                    echo '<label class="control-label col-md-5" style="padding-right: 0px;">Help Document :</label><div class="col-md-5">
                                        <a href="">-</i></a>
                                        </div>
                                        <div class="col-md-2">
                                        </div>';
                                }
                                ?>
                                
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <?php
                                if(count($viewTender["upload_tenderdetail"],COUNT_RECURSIVE)>1) {
                                    $tenderdetail=1;
                                    foreach ($viewTender["upload_tenderdetail"] as $tenderdetailkey) {
                                        foreach ($tenderdetailkey as $keys => $Doctenderdetailvalue) {
                                            if ($tenderdetail==1) {
                                                echo '<label class="control-label col-md-5" style="padding-right: 0px;">E Quote Detail :</label>';
                                            } else {
                                                echo '<label class="control-label col-md-5" style="padding-right: 0px;"></label>';
                                            }
                                        ?>
                                            <div class="col-md-5">
                                               <a class="download_doc" href="<?php echo base_url()."uploads/".$tenderdetailkey["doc_name"]?>" target="_blank"><img src="<?php echo base_url()."assets/layouts/layout/img/dow.png" ?>" style="margin-bottom: 10px;">Click here to download</a>
                                            </div>
                                            <div class="col-md-2">
                                            </div>
                                        <?php
                                        }
                                        $tenderdetail++;
                                    }
                                } else {
                                    echo '<label class="control-label col-md-5" style="padding-right: 0px;">E Quote Detail :</label><div class="col-md-5">
                                        <a href="">-</i></a>
                                        </div>
                                        <div class="col-md-2">
                                        </div>';
                                }
                                ?>
                                
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <?php
                                if(count($viewTender["upload_tenderdoc"],COUNT_RECURSIVE)>1) {
                                    $tenderdoccnt=1;
                                    foreach ($viewTender["upload_tenderdoc"] as $tenderdockey) {
                                        foreach ($tenderdockey as $keys => $Doctenderdockeyvalue) {
                                            if ($tenderdoccnt==1) {
                                                echo '<label class="control-label col-md-5" style="padding-right: 0px;">E Quote Documents Required :</label>';
                                            } else {
                                                echo '<label class="control-label col-md-5" style="padding-right: 0px;"></label>';
                                            }
                                        ?>
                                            <div class="col-md-5">
                                                <a class="download_doc" href="<?php echo base_url()."uploads/".$tenderdockey["doc_name"]?>" target="_blank"><img src="<?php echo base_url()."assets/layouts/layout/img/dow.png" ?>" style="margin-bottom: 10px;">Click here to download</a>
                                            </div>
                                            <div class="col-md-2">
                                            </div>
                                        <?php
                                        }
                                        $tenderdoccnt++;
                                    }
                                } else {
                                    echo '<label class="control-label col-md-5" style="padding-right: 0px;">E Quote Documents Required :</label><div class="col-md-5">
                                        <a href="">-</i></a>
                                        </div>
                                        <div class="col-md-2">
                                        </div>';
                                }
                                ?>
                                
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <?php
                                if(count($viewTender["tender_docone"],COUNT_RECURSIVE)>1) {
                                    $tenderdoconecnt=1;
                                    foreach ($viewTender["tender_docone"] as $tenderdoconekey) {
                                        foreach ($tenderdoconekey as $keys => $Doctenderdockeyvalue) {
                                            if ($tenderdoconecnt==1) {
                                                echo '<label class="control-label col-md-5" style="padding-right: 0px;">Tender Document :</label>';
                                            } else {
                                                echo '<label class="control-label col-md-5" style="padding-right: 0px;"></label>';
                                            }
                                        ?>
                                            <div class="col-md-5">
                                                <a class="download_doc" href="<?php echo base_url()."uploads/".$tenderdoconekey["doc_name"]?>" target="_blank"><img src="<?php echo base_url()."assets/layouts/layout/img/dow.png" ?>" style="margin-bottom: 10px;">Click here to download</a>
                                            </div>
                                            <div class="col-md-2">
                                            </div>
                                        <?php
                                        }
                                        $tenderdoconecnt++;
                                    }
                                } else {
                                    echo '<label class="control-label col-md-5" style="padding-right: 0px;">E Quote Document :</label><div class="col-md-5">
                                        <a href="">-</i></a>
                                        </div>
                                        <div class="col-md-2">
                                        </div>';
                                }
                                ?>
                                
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <?php
                                if(count($viewTender["tender_doctwo"],COUNT_RECURSIVE)>1) {
                                    $tenderdoctwocnt=1;
                                    foreach ($viewTender["tender_doctwo"] as $tenderdoctwokey) {
                                        foreach ($tenderdoctwokey as $keys => $Doctenderdoctwovalue) {
                                            if ($tenderdoctwocnt==1) {
                                                echo '<label class="control-label col-md-5" style="padding-right: 0px;">Tender Document :</label>';
                                            } else {
                                                echo '<label class="control-label col-md-5" style="padding-right: 0px;"></label>';
                                            }
                                        ?>
                                            <div class="col-md-5">
                                                <a class="download_doc" href="<?php echo base_url()."uploads/".$tenderdoctwokey["doc_name"]?>" target="_blank"><img src="<?php echo base_url()."assets/layouts/layout/img/dow.png" ?>" style="margin-bottom: 10px;">Click here to download</a>
                                            </div>
                                            <div class="col-md-2">
                                            </div>
                                        <?php
                                        }
                                        $tenderdoctwocnt++;
                                    }
                                } else {
                                    echo '<label class="control-label col-md-5" style="padding-right: 0px;">E Quote Document :</label><div class="col-md-5">
                                        <a href="">-</i></a>
                                        </div>
                                        <div class="col-md-2">
                                        </div>';
                                }
                                ?>
                                
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->

                    <h3 class="form-section formheading">E Quote Date Detail</h3>
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Start Date & Time</label>
                                <div class="col-md-8">
                                   : <?php echo $viewTender["start_date"];?> <?php echo !empty($viewTender["start_time"])?date('H:i A',strtotime($viewTender["start_time"])):"--"; ?>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">End Date & Time</label>
                                <div class="col-md-8">
                                    : <?php echo $viewTender["end_date"];?> <?php echo !empty($viewTender["end_time"])?date('H:i A',strtotime($viewTender["end_time"])):"--"; ?>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                </div>
            </form>
            <!-- END FORM-->
        </div>
    </div>
</div>
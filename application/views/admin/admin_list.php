<?php
	$search_name = $search_email = $search_phone ='';
	if (!empty($search)) {
		$search_phone = $search['phone'];
		$search_name = $search['name']; 
		$search_email = $search['email']; 
	}
?>
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
	<?php
    $msg=$this->session->flashdata('message_success');
    if(!empty($msg)) {
    ?>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="alert alert-success alert-dismissible">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <?php echo $msg ?>
        </div>
    </div>
    <?php
    }
    ?>
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                 <img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/user.png" class="imgbasline"> Admin List</div>
            <div class="actions">
                <a href="add_admin" class="btn green btn-sm customaddbtn"><i class="fa fa-plus"></i> Add Admin</a>
            </div>
        </div>
        <div class="portlet-body">
        	<form name="frm_adminlist" id="frm_adminlist" method="POST">
		        <div class="row">
		        	<div class="col-md-12 paddingleftright">
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                           <input type="text" class="form-control" name="admin[name]" id="name" placeholder="Name" value="<?php echo $search_name ?>">
	                        </div>
		        		</div>
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                           <input type="text" class="form-control" name="admin[email]" id="email" placeholder="Email Address" value="<?php echo $search_email ?>">
	                        </div>
		        		</div>
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                            <input type="text" class="form-control" name="admin[phone]" id="phone" placeholder="Phone Number" value="<?php echo $search_phone ?>">
	                        </div>
		        		</div>
		        		<div class="col-md-3">
		        			<div class="col-md-12 paddingleftright">
		        				<button type="submit" class="btn btn-warning customsearchtbtn"> <i class="fa fa-search"></i> Search</button>
		        				<a href="<?php echo base_url()."admin/settings/admin_list"?>" type="button" class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Reset</a>
		        			</div>
		        		</div>
		        	</div>
		        </div>
	        	<div class="table-responsive" style="overflow-x: inherit;margin-top:0px;">
		            <table class="table table-striped table-bordered table-hover suppliertbl" id="admin-list">
		            	<thead>
		                    <tr>
		                        <th style="width: 50px;">SI.NO</th>
		                        <th>Name</th>
		                        <th>Email</th>
		                        <th>Phone</th>
		                        <th>Action </th>
		                    </tr>
		                </thead>
		                </tbody>
		                    <?php
		                    $sno=1;
		                    foreach ($adminList as $admindata) {
		                    	$adminid=$admindata->admin_id;
		                    ?>
		                    <tr>
		                        <td><?php echo $sno?></td>
		                        <td><?php echo $admindata->name; ?></td>
		                        <td><?php echo $admindata->email; ?></td>
		                        <td><?php echo $admindata->phone;?></td>
		                        <td> 
		                        	<?php 
		                        	  $admin_id=$this->session->userdata("admin_id");
		                        	  if($admin_id==$adminid) {
		                        	?>
		                        	<a href="add_admin/<?php echo $admindata->admin_id;?>" type="button" class="btn grey-cascade btn-xs custominvitebtn" title="Edit"><i class="fa fa-edit"></i> Edit</a> <!-- <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn" title="Delete"><i class="fa fa-trash"></i> Delete</a> -->
		                        	<?php
	                                 } else {
	                                 	echo "-";
	                                 }
		                        	?>
		                        </td>
		                    </tr>
		                    <?php
		                      $sno++;
		                    }
		                    ?>
		                    
		                </tbody>
		            </table>
		        </div>
		    </form>
        </div>
    </div>
</div>
<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of MY_Controller
 *
 * @author Shenll
 */
class MY_Controller extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('Admin_Model');
		$this->load->model('Login_Model');
		$this->load->model('Tender_Model');
		$this->load->model('Collective');
		$this->load->library('form_validation');
		$this->load->library('encrypt');
		$this->encrypt->set_mode(MCRYPT_MODE_CFB); 
		$this->load->library('upload');
		$this->load->helper('download');
		$this->load->library("excel");  
		$this->load->model('Category_Model');
		$this->load->helper("file");
		$this->load->model('Supplier_Model');
		$this->load->model('Bidding_Model');
		$this->load->model('Dashboard_Model');
		$this->load->model("Award_Model");
	}

}

<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Admin_Model extends MY_Model {
	function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->model('Collective');
	}

	function AdminLogin() {
		$email = $this->input->post("email");
		$password = $this->input->post("password");
		$passwords=$this->Collective->encode($password);

			if(!empty($email) && !empty($passwords)) {
				$stmt = $this->db->query("select * from tbl_admins where email='" . $email . "' and password='" . $passwords. "'");
				$rowcount = $stmt->num_rows();
				if ($rowcount == 1) {
					$data="";
					foreach ($stmt->result() as $row) {
						$full_name=$row->name;
						$names = explode(' ',$full_name);
						$first_name = $names[0];
						$data = array(
							'admin_id'  =>$row->admin_id,
							'admin_name' => $first_name,
							'admin_email'=>  $row->email,
							'user_type'=> "Admin",
						);
						$this->session->set_userdata($data);
						$adminid=$row->admin_id;
						if(!empty($adminid)) {
							$ip=$this->input->ip_address();
							$data_log = array(
								'admin_id'  =>$adminid,
								'logged_in' => date("Y-m-d H:i:s"),
								'ip_address'=>  $ip,
							);
							$this->db->insert('tbl_admin_login_audit',$data_log);
							$vendor_logid=$this->db->insert_id();
							$data_log = array("adminlogid"=>$vendor_logid);
						}
						$this->session->set_userdata($data_log);
						return true;
					}
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	function logout($adminlogid) {
		$data=array("logged_out"=>date("Y-m-d H:i:s"));
		$this->db->where('admin_login_id',$adminlogid);
		$this->db->update('tbl_admin_login_audit', $data);
		return true;
	}
}
<?php

if (!defined('BASEPATH'))

	exit('No direct script access allowed');

class Award_Model extends MY_Model {
	function __construct() {
		parent::__construct();
		//$this->load->library('session');
	}
	function  getAwardList() {
		if (!empty($_POST['award'])) {
			$award = $_POST['award'];
			$searchrfqno = $award['rfq_no'];
			$search_title = $award['tender_title'];
			$search_name = $award['vendor_name'];
			$search_part = $award['part_name'];
			$search_partno = $award['part_no'];
		} else {
			$searchrfqno = $search_title = $search_name = $search_part= $search_partno="";
		}

		$query = $this->db->select('award.tender_id,award.status, award.vendor_id, award.awarded_price as applyprice, tender.tender_title,tender.tender_title,tender.part_name,tender.part_no,tender.ref_no,tender.base_price,tender.part_price,tender.start_date,tender.end_date,tender.price_edit,vendor.vendor_code,vendor.first_name')
			->from('tbl_tender as tender')
			->join('tbl_awarded_tenders as award', 'tender.tender_id = award.tender_id', 'JOIN')
			->join('tbl_vendors as vendor', 'vendor.vendor_id = award.vendor_id', 'JOIN')
			->where("tender.ref_no LIKE '%$searchrfqno%'")
			->where("tender.part_name LIKE '%$search_part%'")
			->where("tender.part_no LIKE '%$search_partno%'")
			->where("tender.tender_title LIKE '%$search_title%'")
			->where("vendor.first_name LIKE '%$search_name%'")
			->where("award.id !=''")
			->where("tender.status = 'Awarded'")
			->order_by("award.id desc")
			->get();
		if ($query->num_rows() > 0) {
			$data=$bidding="";
			foreach ($query->result() as $row) {
				$count_query = $this->db->select('price, created_on')
					->from('tbl_apply_tender_audit')
					// ->limit(5)
					->order_by("created_on","desc")
					->where("tender_id = '$row->tender_id'")
					->where("vendor_id = '$row->vendor_id'")
					->get();
				if ($count_query->num_rows() > 0) {
					foreach ($count_query->result() as $count_row) {
						$bidding[] = array(
							'price' => $count_row->price,
							'date' => $count_row->created_on,
						);
					}
				}
				$currentdate=date("Y-m-d");
				$data[]= array(
					'tender_id' =>$row->tender_id,
					'vendor_id' =>$row->vendor_id,
					'applyprice'=> $row->applyprice,
					'tender_title'=> $row->tender_title,
					'part_name' => $row->part_name,
					'part_no' => $row->part_no,
					'ref_no'  =>$row->ref_no,
					'base_price' => $row->base_price,
					'status' => $row->status,
					'start_date' => $row->start_date,
					'end_date'  => $row->end_date,
					'vendor_code' =>$row->vendor_code,
					'vendor_name' =>$row->first_name,
					'bidding_count' =>$count_query->num_rows(),
					'bidding_log' => $bidding
				);
				$bidding = '';
			}
			return $data;
		} 
	}
	function supplierAwardList() {


		if (!empty($_POST['award'])) {

			$award = $_POST['award'];
			$searchrfqno = $award['rfq_no'];
			$searchpartno = $award['part_no'];
			$searchpartname = $award['part_name'];
			$search_title = $award['tender_title'];
			$search_startdate = $award['award_date'];
			$search_enddate = $award['award_enddate'];
			$condition = $search_startdate = $search_enddate="";
			if (!empty($award['award_date']) && empty($award['award_enddate'])) {
				$start_date       = str_replace("/", "-",$award['award_date']);
				$search_startdate = date("Y-m-d",strtotime($start_date));
				$condition.=" AND DATE_FORMAT(award.created_on,'%Y-%m-%d') ='".$search_startdate."'";
			}
			if (empty($award['award_date']) && !empty($award['award_enddate'])) {
				$end_date       = str_replace("/", "-",$award['award_enddate']);
				$search_enddate = date("Y-m-d",strtotime($end_date));
				$condition.=" AND DATE_FORMAT(award.created_on,'%Y-%m-%d') ='".$search_enddate."'";
			}
			if (!empty($award['award_date']) && !empty($award['award_enddate'])) {
				$start_date     = str_replace("/", "-",$award['award_date']);
				$end_date       = str_replace("/", "-",$award['award_enddate']);
				$search_startdate = date("Y-m-d",strtotime($start_date));
				$search_enddate  = date("Y-m-d",strtotime($end_date));
				$condition.=" AND DATE_FORMAT(award.created_on,'%Y-%m-%d') >= '".$search_startdate. "' and DATE_FORMAT(award.created_on,'%Y-%m-%d') >= '".$search_enddate. "'";
			}
		} else {
			$condition=$searchrfqno = $search_title = $search_startdate = $search_enddate= $searchpartno = $searchpartname="";
		}
		$sessionvendorid=$this->session->userdata('vendor_id');
		$query = $this->db->select('award.tender_id,award.status, award.vendor_id, award.awarded_price as applyprice, tender.tender_title,tender.tender_title,tender.part_name,tender.ref_no,tender.base_price,tender.part_price,tender.start_date,tender.end_date,tender.price_edit,vendor.vendor_code,vendor.company_name,award.created_on,tender.part_no')
			->from('tbl_tender as tender')
			->join('tbl_awarded_tenders as award', 'tender.tender_id = award.tender_id', 'JOIN')
			->join('tbl_vendors as vendor', 'vendor.vendor_id = award.vendor_id', 'JOIN')
			->where("tender.ref_no LIKE '%$searchrfqno%'")
			->where("tender.part_no LIKE '%$searchpartno%'")
			->where("tender.part_name LIKE '%$searchpartname%'")
			->where("tender.tender_title LIKE '%$search_title%'".$condition)
			->where("award.vendor_id='$sessionvendorid'")
			->where("award.status='Awarded'")
			->order_by("award.id desc")
			->get();
		if ($query->num_rows() > 0) {
			$data="";
			foreach ($query->result() as $row) {
				$currentdate=date("Y-m-d");
				$data[]= array(
					'tender_id' =>$row->tender_id,
					'vendor_id' =>$row->vendor_id,
					'applyprice'=> $row->applyprice,
					'tender_title'=> $row->tender_title,
					'part_name' => $row->part_name,
					'ref_no'  =>$row->ref_no,
					'part_no'  =>$row->part_no,
					'base_price' => $row->base_price,
					'status' => $row->status,
					'start_date' => $row->start_date,
					'end_date'  => $row->end_date,
					'vendor_code' =>$row->vendor_code,
					'vendor_name' =>$row->company_name,
					'created_on' =>$row->created_on,
				);
			}
			return $data;
		}
	}
}
<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class category_model extends MY_Model {
	function __construct() {
		parent::__construct();
	}

	function cat_list($searcharray=null) {
		$this->db->select("*");
		$this->db->from("tbl_category");
		if(!empty($searcharray)){
			foreach($searcharray as $key=>$value){
				$this->db->like($key,$value);
			}
		}
		$this->db->where('status !=', 'Trash');
		$query = $this->db->get();
		$num_rows = $query->num_rows();
		if($num_rows >0) {
			return $query->result();
		}else {
			return false;
		}
	}
	function check_cat($category_name,$category_id = NULL) {
		$this->db->select("*");
		$this->db->from("tbl_category");
		$this->db->where('category_name', $category_name);
		$this->db->where('status !=', 'Trash');
		if($category_id >0) {
			$this->db->where('category_id != ', $category_id);
		}
		$query = $this->db->get()->result();
		if(empty($query)){
			$result = true;
		} else {
			$result = false;
		}
		return $result;
	}
	function get_category($category_id) {
		$this->db->select("*");
		$this->db->from("tbl_category");
		$this->db->where('category_id', $category_id);
		$query = $this->db->get();
		return $query->result();
	}
	function add($data){
		$this->db->insert('tbl_category', $data);
		$insert_id = $this->db->insert_id();
		return $insert_id;
	}
	function update($data, $category_id) {
		$this->db->where('category_id', $category_id);
		$this->db->update('tbl_category', $data);
		return true;
	}
	function delete($category_id) {
		$data = array('status'=>'Trash','modified_on'=>date('Y-m-d H:i:s'));
		$this->db->where('category_id', $category_id);
		$this->db->update('tbl_category', $data);
		return true;
	}
}

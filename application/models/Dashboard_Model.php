<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Dashboard_Model extends MY_Model {
	function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->model('Collective');
	}

	function DashboardFileUpload() {
		$vendor_id= $this->session->userdata("vendor_id");
		$vendordoc='';
		if(!empty($_FILES['upload_file']['name'])){
			$config['upload_path'] = './uploads/';
			$config['allowed_types'] = 'doc|pdf|xls|xlsx|docx';
			$config['file_name'] = $_FILES['upload_file']['name'];
			$this->load->library('upload',$config);
			$this->upload->initialize($config);
			if ( ! $this->upload->do_upload('upload_file'))
			{
					$error = array('error' => $this->upload->display_errors());
					$vendordoc = '';
			}
			else {
				$data = array('upload_data' => $this->upload->data());
				$vendordoc=$data['upload_data']['file_name'];
				if (!empty($vendordoc)) {
					$data=array(
						"supplier_doc"=>$vendordoc,
						"modified_date"=>date("Y-m-d H:i:s"),
					);
					$this->db->where('vendor_id',$vendor_id);
					$this->db->update('tbl_vendors', $data);
					$data = "success";
					return $data;
				} else {
					return false;
				}
			}
		}
	}

	function DashboardCounts() {
		$monthyear=date("Y-m");
		$vendor_id= $this->session->userdata("vendor_id");
		if (!empty($vendor_id)) {
			$query = $this->db->select('count(tender_id) as latestequote')
						->from('tbl_tender')
						->where("status='Open'")
						->where("(DATE_FORMAT(start_date,'%Y-%m') ='$monthyear' OR DATE_FORMAT(end_date,'%Y-%m')='$monthyear')")
						->get();
			$rowcounts = $query->num_rows();
			$latestequoteCount= $query->result();
			foreach ($query->result() as $row) {
				$latestequoteCount=$row->latestequote;
			}
			$queryApplied = $this->db->select('count(id) as appliedCount')
						->from('tbl_tender_applications')
						->where("vendor_id='$vendor_id'")
						->where("DATE_FORMAT(created_date,'%Y-%m') ='$monthyear' GROUP BY vendor_id")
						->get();
			$noofappliedquoteCount = $queryApplied->num_rows();

			$queryAwarded = $this->db->select('count(vendor_id) as awardedCount')
						->from('tbl_awarded_tenders') 
						->where("vendor_id='$vendor_id'")
						->where("status='Awarded'")
						->where("DATE_FORMAT(created_on,'%Y-%m') ='$monthyear'")
						->get();
			$rowAwardedcounts = $queryAwarded->num_rows();
			
			foreach ($queryAwarded->result() as $Awarded) {
				$noofawardedCount=$Awarded->awardedCount;
			}
			
			$queryBid = $this->db->select('count(applytender.vendor_id) as biddingcount')
			->from('tbl_apply_tender_audit as applytender')
			->join('tbl_tender as tender', 'applytender.tender_id = tender.tender_id', 'INNER JOIN')
			->where("DATE_FORMAT(applytender.created_on,'%Y-%m')='$monthyear'")
			->where('applytender.vendor_id', $vendor_id)
			->get();
			$rowBidcounts = $queryBid->num_rows();
			
			foreach ($queryBid->result() as $Bidding) {
				$noofbiddingCount=$Bidding->biddingcount;
			}
			$query = $this->db->select('*')
						->from('tbl_vendors') 
						->where("vendor_id='$vendor_id'")
						->where("status='Active'")
						->get();
			$vendor= $query->result();

			$querytender = $this->db->select('*,activity.status as current_status')
						->from('tbl_tender_activity as activity')
						->join('tbl_tender as tender','tender.tender_id=activity.tender_id','Inner')
						->order_by("act_id",'desc')
						->limit(2)
						->get()->result();
		    $recenttender1=$recenttenders="";
            foreach($querytender as $tenderData){
            	$suppliers = $tenderData->suppliers;

                if($suppliers=="") {
                	$tender_id = $tenderData->tender_id;
                    $query1 = $this->db->select('*,activity.status as current_status')
								->from('tbl_tender_activity as activity')
								->join('tbl_tender as tender','tender.tender_id='.$tender_id.'','Inner')
								->limit(1)
								->order_by("act_id",'desc')
								->get()->result();
					$recenttender1=$query1;
				} else {
					$suppliers=$tenderData->suppliers;
                	$supplierarr = explode("##",$suppliers);
                	if (in_array($vendor_id,$supplierarr)) {
                	  $tender_id = $tenderData->tender_id;
                      $querys = $this->db->select('*,activity.status as current_status')
						->from('tbl_tender_activity as activity')
						->join('tbl_tender as tender','tender.tender_id='.$tender_id.'','Inner')
						->order_by("act_id",'desc')
						->limit(1)
						->get()->result();
					$recenttenders=$querys;
					//print_r($recent_tender);
				    }
				}
		    }
		    if(count($recenttender1)>0 && empty($recenttenders)){
               $recent_tender=$recenttender1;
		    } else if(count($recenttenders)>0 && empty($recenttender1)){
                  $recent_tender=$recenttenders;
		    } else {
		    	$recent_tender=array_merge($recenttender1,$recenttenders);
		    }
            
			$query = $this->db->select('*')
						->from('tbl_awarded_tenders as award')
						->join('tbl_tender as tender','tender.tender_id=award.tender_id','Inner')
						->join('tbl_vendors as vendor','vendor.vendor_id=award.vendor_id','Inner')
						->where("award.status='Awarded'")
						->where("vendor.vendor_id='$vendor_id'")
						->order_by("id",'desc')
						->limit(2)
						->get()->result();
			$recent_award=$query;

			$data=array(
				"latestequote"=>$latestequoteCount,
				"noofappliedquote"=>$noofappliedquoteCount,
				"awardedquote"=> $noofawardedCount,
				"biddingquote"=>$noofbiddingCount,
				"vendor"=>$vendor,
				"recent_tender"=>$recent_tender,
				"recent_award"=>$recent_award,
			);
			return $data;
		} else {
			$query = $this->db->select('count(tender_id) as latestequote')
						->from('tbl_tender')
						->where("status='Open'")
						->where("((DATE_FORMAT(start_date,'%Y-%m') ='$monthyear' OR DATE_FORMAT(end_date,'%Y-%m')='$monthyear'))")
						->get();
			$rowcounts = $query->num_rows();
			$latestequoteCount= $query->result();
			foreach ($query->result() as $row) {
				$latestequoteCount=$row->latestequote;
			}

			$queryApplied = $this->db->select('count(id) as appliedCount')
						->from('tbl_tender_applications')
						->where("DATE_FORMAT(created_date,'%Y-%m') ='$monthyear' GROUP BY vendor_id")
						->get();
			$noofappliedquoteCount = $queryApplied->num_rows();

			$queryAwarded = $this->db->select('count(vendor_id) as awardedCount')
						->from('tbl_awarded_tenders')
						->where("status='Awarded'")
						->where("DATE_FORMAT(created_on,'%Y-%m') ='$monthyear'")
						->get();
			$rowAwardedcounts = $queryAwarded->num_rows();
			foreach ($queryAwarded->result() as $Awarded) {
				$noofawardedCount=$Awarded->awardedCount;
			}

			$queryvendors = $this->db->select('count(vendor_id) as vendorsCount')
						->from('tbl_vendors')
						->where("status='Active'")
						->get();
			$rowvendorscounts = $queryvendors->num_rows();
			
			foreach ($queryvendors->result() as $vendors) {
				$noofvendorCount=$vendors->vendorsCount;
			}
			$query = $this->db->select('*,activity.status as current_status')
						->from('tbl_tender_activity as activity')
						->join('tbl_tender as tender','tender.tender_id=activity.tender_id','Inner')
						->order_by("act_id",'desc')
						->limit(2)
						->get()->result();
			$recent_tender=$query;

			$query = $this->db->select('*')
						->from('tbl_vendors')
						->where("status='Active'")
						->order_by("vendor_id",'desc')
						->limit(2)
						->get()->result();
			$recent_vendor=$query;

			$query = $this->db->select('*')
						->from('tbl_awarded_tenders as award')
						->join('tbl_tender as tender','tender.tender_id=award.tender_id','Inner')
						->join('tbl_vendors as vendor','vendor.vendor_id=award.vendor_id','Inner')
						->where("award.status='Awarded'")
						->order_by("id",'desc')
						->limit(2)
						->get()->result();
			$recent_award=$query;
			
			$data=array(
				"latestequote"=>$latestequoteCount,
				"noofappliedquote"=>$noofappliedquoteCount,
				"awardedquote"=> $noofawardedCount,
				"noofvendor"=>$noofvendorCount,
				"recent_tender"=>$recent_tender,
				"recent_vendor"=>$recent_vendor,
				"recent_award"=>$recent_award,
			);
			return $data;
		}
	}

	function add_activity($data){
		$this->db->insert('tbl_tender_activity', $data);
		$insert_id = $this->db->insert_id();
		return $insert_id;
	}
}
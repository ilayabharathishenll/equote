<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Settings_Model extends MY_Model {
	function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->model('Collective');
	}
	function AdminList(){
		if (!empty($_POST['admin'])) {
            $admin = $_POST['admin'];
            $search_name = $admin['name'];
            $search_email = $admin['email'];
            $search_phone = $admin['phone'];
        } else {
            $search_name = $search_email= $search_phone="";
        }
        $query = $this->db->select('*')
            ->from('tbl_admins')
            ->where("name LIKE '%$search_name%'")
            ->where("email LIKE '%$search_email%'")
            ->where("phone LIKE '%$search_phone%'")
            ->get();
		$rowcount = $query->num_rows();
		if($rowcount>0){
		$data = $query->result();
		return $data;
		} else {
		return false;
		}
	}
	function AdminGet($admin_id) {
		$stmt = $this->db->query("select * from tbl_admins where admin_id='" . $admin_id . "'");
		$rowcount = $stmt->num_rows();
		if ($rowcount == 1) {
			$data="";
			foreach ($stmt->result() as $row) {
				$data = array(
					'admin_id'  =>$row->admin_id,
					'admin_name' => $row->name,
					'admin_email'=>  $row->email,
					'admin_password'=>  $this->Collective->decode($row->password),
					'admin_phone'=>  $row->phone,
					'admin_altphone'=>  $row->phone_secondary,
					
				);
				return  $data;
			}
		} else {
			return false;
		}
	}
	function AdminAdded()
	{
		$name = $this->input->post("name");
		$email = $this->input->post("email");
		$password = $this->input->post("password"); 
		$passwords=$this->Collective->encode($password);
		$phone = $this->input->post("phone");
		$alt_phone = $this->input->post("alt_phone");
		$datetimedb = date("Y-m-d H:i:s");
		if (!empty($name)) {
			$data=array(
				"name"=>$name,
				"email"=>$email,
				"password"=>$passwords,
				"phone"=>$phone,
				"phone_secondary"=>$alt_phone,
				"status"=>"Active",
				"created_date"=>$datetimedb,
				"modified_date"=>$datetimedb,
			);
			$this->db->insert('tbl_admins', $data);
			return $this->db->insert_id();
		}
	}
	function Adminupdate($adminid) {
		$name = $this->input->post("name");
		$email = $this->input->post("email");
		$password = $this->input->post("password"); 
		$passwords=$this->Collective->encode($password);
		$phone = $this->input->post("phone");
		$alt_phone = $this->input->post("alt_phone");
		$datetimedb = date("Y-m-d H:i:s");
		$data=array(
			"name"=>$name,
			"email"=>$email,
			"password"=>$passwords,
			"phone"=>$phone,
			"phone_secondary"=>$alt_phone,
			"modified_date"=>$datetimedb,
		);
		$this->db->where('admin_id',$adminid);
		$this->db->update('tbl_admins', $data);
		return true;
	}
}

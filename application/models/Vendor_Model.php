<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Vendor_Model extends Admin_Model {
	function __construct() {
		parent::__construct();
		$this->load->database();
	}

	public function get_vendor_list() {
		if (!empty($_POST['vendor'])) {
			$vendor = $_POST['vendor'];
			$first_name = $vendor['first_name'];
			$email = $vendor['email'];
			$phone = $vendor['phone'];
			$query = $this->db->select('*')
					->from('tbl_vendors')
					->where("first_name LIKE '%$first_name%'")
					->where("email LIKE '%$email%'")
					->where("phone LIKE '%$phone%'")
					->get();
			return $query->result();
		} else {
			$query = $this->db->get_where('tbl_vendors');
			return $query->result();
		}
	}

	public function get_single_vendor($id) {
		if (!empty($id)) {
			$query = $this->db->get_where('tbl_vendors', array('vendor_id =' => $id));
			return $query->result();
		}
	}

	public function update_single_vendor($id, $status) {
		$this->db->set('status',$status);
		$this->db->where('vendor_id',$id);
		$this->db->update('tbl_vendors');
		return $this->db->affected_rows();
	}
	function update_vendor($id,$data){
		$this->db->where('vendor_id', $id);
		$this->db->update('tbl_vendors', $data);
		return $this->db->affected_rows();
	}
	public function update_vendor_status($vendor_data) {
		$this->db->set('status',$vendor_data['status']);
		$this->db->set('rating',$vendor_data['rating']);
		$this->db->where('vendor_id',$vendor_data['id']);
		$this->db->update('tbl_vendors');
		return $this->db->affected_rows();
	}

	public function add_vendor_invite($vendor_data) {
		$data = array(
			'vendor_name'=>$vendor_data['vendor_name'],
			'contact_no'=>$vendor_data['contact_no'],
			'email'=>$vendor_data['email']
		);
		$this->db->insert('tbl_vendor_invitations',$data);
		return $this->db->affected_rows();
	}
}
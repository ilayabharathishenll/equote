<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Applied_Model extends MY_Model {
	function __construct() {
		parent::__construct();
		$this->load->library('session');
	}

	public function getAppliedList() {
		$sessionVendorid=$this->session->userdata('vendor_id');
		$query = $this->db->select('applytender.tender_id, applytender.vendor_id,tender.tender_title,tender.tender_title,tender.ref_no,tender.delivery_duration,tender.end_date')
			->from('tbl_apply_tender_audit as applytender')
			->join('tbl_tender as tender', 'applytender.tender_id = tender.tender_id', 'INNER JOIN')
			->where('applytender.vendor_id', $sessionVendorid)
			->get();
		return $query->result();
	}

	public function getTenderDetails($tender_id) {
		$sessionVendorid=$this->session->userdata('vendor_id');
		$tender = $this->db->get_where('tbl_tender', array('tender_id =' => $tender_id));
		$price = $this->db->select('applytender.price')
			->from('tbl_apply_tender_audit as applytender')
			->join('tbl_tender as tender', 'applytender.tender_id = tender.tender_id', 'INNER JOIN')
			->where('applytender.vendor_id', $sessionVendorid)
			->where('applytender.tender_id', $tender_id)
			->get();
		foreach ($tender->result() as $tender) {
			$data['tender_id'] = $tender->tender_id;
			$data['part_no'] = $tender->part_no;
			$data['part_name'] = $tender->part_name;
			$data['part_price'] = $tender->part_price;
			$data['drawing_no']  = $tender->drawing_no;
			$data['ref_no'] = $tender->ref_no;
			$data['delivery_duration'] = $tender->delivery_duration;
			$data['delivery_date'] = $tender->delivery_date;
			$data['quantity']  = $tender->quantity;
			$data['base_price']  = $tender->base_price;
			$data['tender_title']  = $tender->tender_title;
			$data['start_date']  = $tender->start_date;
			$data['end_date']  = $tender->end_date;
			$data['norm']  = $tender->norm;
			$data['tender_detail']  = $tender->tender_detail;
			$data['rm_price']  = $tender->rm_price;
			$data['drawing_doc']  = $tender->drawing_doc;
		}
		foreach ($price->result() as $applied_price) {
			$data['applied_price'] = $applied_price->price;
		}
		return $data;
	}
}
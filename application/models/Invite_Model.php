<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Invite_Model extends MY_Model {
	function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->library('email');
		$config = array (
			'mailtype'=>'html'
		);
	}
	function tenderInviteList() {
		if (!empty($_POST['invite'])) {
			$invite = $_POST['invite'];
			$rfq_no = $invite['rfq_no'];
			$tender_title = $invite['tender_title'];
			$part_name = $invite['part_name'];
			$part_no = $invite['part_no'];
		} else {
			$rfq_no= $tender_title = $part_name = $part_no="";
		}
		$currentdate=date("Y-m-d");
		$query = $this->db->select('*')
				->from('tbl_tender')
				->where("ref_no LIKE '%$rfq_no%'")
				->where("tender_title LIKE '%$tender_title%'")
				->where("part_name LIKE '%$part_name%'")
				->where("part_no LIKE '%$part_no%'")
				->where("end_date >= '$currentdate'")
				->where("status = 'Open'")
				->where("notification_setting = '2'")
				->order_by('created_on', 'desc')
				->get();
		return $query->result();
	}

	function VendorInviteList($tenderId) {
		if (!empty($_POST['sendinvite']))  {
			$sendinvite = $_POST['sendinvite'];
			$vendor_code = $sendinvite['vendor_code'];
			$vendor_name = $sendinvite['vendor_name'];
			$vcategory = $sendinvite['category'];
		} else {
			$vendor_code= $vendor_name = $vcategory ="";
		}

		$queryStm = $this->db->select('*')
					->from('tbl_tender_invitations')
					->where("tender_id ='$tenderId'")
					->order_by('id', 'desc')
					->get();
		$rowcount = $queryStm->num_rows();
		if ($rowcount > 0) {
			foreach ($queryStm->result() as $rows) {
				$vendor_id[] = $rows->vendor_id;
			}
			$ArrVendorid = implode(',', $vendor_id);
			$query = $this->db->select('*')
					->from('tbl_vendors')
					->where("vendor_code LIKE '%$vendor_code%'")
					->where("company_name LIKE '%$vendor_name%'")
					->where("vendor_category LIKE '%$vcategory%'")
					->where("vendor_id NOT IN (".$ArrVendorid.")")
					->where("status ='Active'")
					->order_by('vendor_id', 'desc')
					->get();
			$rowcounts = $query->num_rows();
			if ($rowcounts > 0) {
				$data="";
				foreach ($query->result() as $row) {
					$vendorcategory= $row->vendor_category;
					$vendorId= $row->vendor_id;
					$rating= $row->rating;

					if (!empty($vcategory)) {
						$category_id=$vcategory;
					} else{
						$categorySplit = explode("##",$vendorcategory); 
						$category_id= next($categorySplit);
					}
					$stmt = $this->db->query("select category_name from tbl_category where  category_id='" . $category_id. "'");
					$cname = $stmt->row();
					if (!empty($cname)) {
						$vendor_cate = $cname->category_name;
					} else {
						$vendor_cate = "...";
					}
						$data[]= array(
							'vendor_id'  =>$row->vendor_id,
							'vendor_code'  =>$row->vendor_code,
							'tender_id'  =>$tenderId,
							'vendor_name' => $row->company_name,
							'vendor_category'=>$vendor_cate,  
							'rating'=>$rating,  
						);
				}
				return  $data;
			} else {
				return false;
			}
		} else {
			$query = $this->db->select('*')
					->from('tbl_vendors')
					->where("vendor_code LIKE '%$vendor_code%'")
					->where("company_name LIKE '%$vendor_name%'")
					->where("vendor_category LIKE '%$vcategory%'")
					->where("status ='Active'")
					->order_by('vendor_id', 'desc')
					->get();
			$rowcount = $query->num_rows();
			if ($rowcount > 0) {
				$data="";
				foreach ($query->result() as $row) {
					$vendorcategory= $row->vendor_category;
					$vendorId= $row->vendor_id;
					$rating= $row->rating;
					if (!empty($vcategory)) {
						$category_id = $vcategory;
					} else {
						$categorySplit = explode("##",$vendorcategory); 
						$category_id = next($categorySplit);
					}

					$stmt = $this->db->query("select category_name from tbl_category where  category_id='" . $category_id. "'");
					$cname = $stmt->row();

					if (!empty($cname)) {
						$vendor_cate = $cname->category_name;
					} else {
						$vendor_cate = "...";
					}
					$data[]= array(
						'vendor_id'  =>$row->vendor_id,
						'vendor_code'  =>$row->vendor_code,
						'tender_id'  =>$tenderId,
						'vendor_name' => $row->company_name,
						'vendor_category'=>$vendor_cate,
						'rating'=>$rating,
					);
				}
				return  $data;
			} else {
				return false;
			}
		}
	}
	function VendorInvitemail($tenderId,$ArrayVendor) {
		if (count($ArrayVendor>0)) {
			foreach ($ArrayVendor as $getvendorID) {
				$adminids=$this->session->userdata('admin_id');
				$datainvite=array(
					"vendor_id"=>$getvendorID,
					"tender_id"=>$tenderId,
					"created_by"=>$adminids,
					"created_on"=>date("Y-m-d H:i:s"),
				);
			$this->db->insert('tbl_tender_invitations', $datainvite);
			$stmt = $this->db->query("select first_name,email,company_name,vendor_code from tbl_vendors where vendor_id='" .$getvendorID. "'");
			$vendor_detail = $stmt->row();
			$first_name = $vendor_detail->first_name;
			$email  = $vendor_detail->email;
			$company_name = $vendor_detail->company_name;
			$vendor_code = $vendor_detail->vendor_code;

			$Tenderstmt = $this->db->query("select ref_no,tender_title,base_price,start_date,end_date,delivery_duration from tbl_tender where tender_id='" .$tenderId. "'");
			$tender_detail = $Tenderstmt->row();
			$ref_no = $tender_detail->ref_no;
			$tender_title  = $tender_detail->tender_title;
			$base_price = $tender_detail->base_price;
			$start_date = date("d/m/Y",strtotime($tender_detail->start_date));
			$end_date = date("d/m/Y",strtotime($tender_detail->end_date));
			$duration = $tender_detail->delivery_duration;
			$EmailMessage = '
					<html>
						<style>
							@media screen and (min-width: 320px) {
								.container1 {
									width: 100%!important;
								}
							}
						</style>
						<table class="container1" width="100%" cellpadding="0" cellspacing="0"  width="600px" style="font-family: verdana;font-size:13px;max-width: 600px;">
							<tr>
							<td>Hello '.$first_name.',</td>
							</tr>
							<tr style="height:10px"><td></td></tr>
							<tr>
							<td>As requested, your E-Quote Management Invite details are below:</td>
							</tr>
							<tr style="height:10px"><td></td></tr>
							<tr>
								<td>Company Name : '.$company_name.'</td>
							</tr>
							<tr style="height:10px"><td></td></tr>
							<tr>
								<td>Vendor Code : '.$vendor_code.'</td>
							</tr>
							<tr style="height:2px"><td></td></tr>
							<tr>
								<td>User Email : '.$email.'</td>
							</tr>
							<tr style="height:2px"><td></td></tr>
							<tr>
								<td>RFQ Number : '.$ref_no.'</td>
							</tr>
							<tr style="height:2px"><td></td></tr>
							<tr>
								<td>E-Quote Name : '.$tender_title.'</td>
							</tr>
							<tr style="height:2px"><td></td></tr>
							<tr>
								<td>E-Quote Price : '.$base_price.'</td>
							</tr>
							<tr style="height:2px"><td></td></tr>
							<tr>
								<td>Start Date : '.$start_date.'</td>
							</tr>
							<tr style="height:2px"><td></td></tr>
							<tr>
								<td>End Date : '.$end_date.'</td>
							</tr>
							<tr style="height:2px"><td></td></tr>
							<tr>
								<td>Delivery Duration : '.$duration.'</td>
							</tr>
							<tr style="height:10px"><td></td></tr>
							<tr>
							<td>Thank you,<br>E-Quote Management</td>
							</tr> 
						</table>
					</html>
					';
				$this->email->set_mailtype("html");
				$this->email->from('karuna.shenll@gmail.com', 'E-Quote');
				$this->email->to($email);
				$this->email->subject('Your E-Quote Management Invite Details');
				$this->email->message($EmailMessage);
				if ($this->email->send()) {
				} else {
					echo $this->email->print_debugger();
				} 
			}
			return "success";
		} else {
			return "failure";
		}
	}
	function viewTender($tenderId) {
		$query = $this->db->select('*')
					->from('tbl_tender')
					->where("tender_id ='$tenderId'")
					 ->where("status != 'Trash'")
					->order_by('tender_id', 'desc')
					->get();
		$rowcount = $query->num_rows();
		if ($rowcount > 0) {
			$data="";
			foreach ($query->result() as $row) {
				$data= array(
					'ref_no' => $row->ref_no,
					'part_name' =>$row->part_name,
					'delivery_duration' => $row->delivery_duration,
					'delivery_date' => date("d/m/Y",strtotime($row->delivery_date)),
					'quantity' =>$row->quantity,
					'tender_title'=> $row->tender_title,
					'part_name' => $row->part_name,
					'part_price'  =>$row->part_price,
					'norm'  =>$row->norm,
					'tender_detail'  =>$row->tender_detail,
					'start_date' =>  date("d/m/Y",strtotime($row->start_date)),
					'start_time' =>  $row->start_time,
					'end_date'  =>  date("d/m/Y",strtotime($row->end_date)),
					'end_time' =>  $row->end_time,
					'base_price'=>$row->base_price,
					'rm_price' =>$row->rm_price,
					'drawing_doc'=>$row->drawing_doc,
				);
			}

			$docquery = $this->db->select('*')
					->from('tbl_tender_documents')
					->where("tender_id ='$tenderId'")
					->where("status ='Active'")
					->get();
			$rowcnt = $docquery->num_rows();
			if ($rowcnt > 0) {
				foreach ($docquery->result() as $docrow) {
					$doc_type = $docrow->t_doc_type;
					if (strtolower($doc_type) == "upload_terms") {
						$data["upload_terms"][] = array(
							'doc_name' => $docrow->t_doc_name,
						);
					}

					if (strtolower($doc_type) == "upload_helpdoc") {
						$data["upload_helpdoc"][] = array(
							'doc_name' => $docrow->t_doc_name,
						);
					}

					if (strtolower($doc_type) == "upload_tenderdetail") {
						$data["upload_tenderdetail"][] = array(
							'doc_name' => $docrow->t_doc_name,
						);
					}

					if (strtolower($doc_type) == "upload_tenderdoc") {
						$data["upload_tenderdoc"][] = array(
							'doc_name' => $docrow->t_doc_name,
						);
					}

					if (strtolower($doc_type) == "tender_docone") {
						$data["tender_docone"][] = array(
							'doc_name' => $docrow->t_doc_name,
						);
					}
					if (strtolower($doc_type) == "tender_doctwo") {
						$data["tender_doctwo"][] = array(
							'doc_name' => $docrow->t_doc_name,
						);
					}
				}
			}
			return $data;
		}
	}
}

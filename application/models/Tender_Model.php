<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class tender_model extends MY_Model {
	function __construct() {
		parent::__construct();
	}
	function add($data){
		$this->db->insert('tbl_tender', $data);
		$insert_id = $this->db->insert_id();
		return $insert_id;
	}
	function update_docs($data) {
		$tender_id = $data['id'];
		$keep_id = array();
		if(!empty($data['terms'])) {
			foreach ($data['terms'] as $term) {
				$query = $this->db->select('t_doc_id')
					->from('tbl_tender_documents')
					->where('tender_id',$tender_id)
					->where('t_doc_type','upload_terms')
					->where('t_doc_name',$term)
					->get();
				if ($query->num_rows() > 0) {
					foreach ($query->result() as $row) {
						$keep_id[] = $row->t_doc_id;
					}
				}
			}
		}
		if(!empty($data['helpdoc'])) {
			foreach ($data['helpdoc'] as $help_doc) {
				$helpquery = $this->db->select('t_doc_id')
					->from('tbl_tender_documents')
					->where('tender_id',$tender_id)
					->where('t_doc_type','upload_helpdoc')
					->where('t_doc_name',$help_doc)
					->get();
				if ($helpquery->num_rows() > 0) {
					foreach ($helpquery->result() as $helprow) {
						$keep_id[] = $helprow->t_doc_id;
					}
				}
			}
		}
        if(!empty($data['tenderdetail'])) {
	        foreach ($data['tenderdetail'] as $tender_detail) {
				$tenderdetailquery = $this->db->select('t_doc_id')
					->from('tbl_tender_documents')
					->where('tender_id',$tender_id)
					->where('t_doc_type','upload_tenderdetail')
					->where('t_doc_name',$tender_detail)
					->get();
				if ($tenderdetailquery->num_rows() > 0) {
					foreach ($tenderdetailquery->result() as $tenderdetailrow) {
						$keep_id[] = $tenderdetailrow->t_doc_id;
					}
				}
			}
		}
		if(!empty($data['tenderdoc'])) {
			foreach ($data['tenderdoc'] as $tenderdoc) {
				$tenderdocquery = $this->db->select('t_doc_id')
					->from('tbl_tender_documents')
					->where('tender_id',$tender_id)
					->where('t_doc_type','upload_tenderdoc')
					->where('t_doc_name',$tenderdoc)
					->get();
				if ($tenderdocquery->num_rows() > 0) {
					foreach ($tenderdocquery->result() as $tenderdocrow) {
						$keep_id[] = $tenderdocrow->t_doc_id;
					}
				}
			}
		}
		if(!empty($data['docone'])) {
			foreach ($data['docone'] as $docone) {
				$doconequery = $this->db->select('t_doc_id')
					->from('tbl_tender_documents')
					->where('tender_id',$tender_id)
					->where('t_doc_type','tender_docone')
					->where('t_doc_name',$docone)
					->get();
				if ($doconequery->num_rows() > 0) {
					foreach ($doconequery->result() as $doconerow) {
						$keep_id[] = $doconerow->t_doc_id;
					}
				}
			}
		}
		if(!empty($data['doctwo'])) {
			foreach ($data['doctwo'] as $doctwo) {
				$doctwoquery = $this->db->select('t_doc_id')
					->from('tbl_tender_documents')
					->where('tender_id',$tender_id)
					->where('t_doc_type','tender_doctwo')
					->where('t_doc_name',$doctwo)
					->get();
				if ($doctwoquery->num_rows() > 0) {
					foreach ($doctwoquery->result() as $doctworow) {
						$keep_id[] = $doctworow->t_doc_id;
					}
				}
			}
		}
        $Arrkeepid=implode("','",$keep_id);
		$deletequery = $this->db->where('tender_id',$tender_id)
		            ->from('tbl_tender_documents')
					->where("t_doc_id NOT IN('$Arrkeepid')")
					->delete();
	}
	function add_details($data){
		$this->db->insert('tbl_tender_documents', $data);
		$insert_id = $this->db->insert_id();
		return $insert_id;
	}
	function update($id,$data){
		$this->db->where('tender_id', $id);
		$this->db->update('tbl_tender', $data);
		return true;
	}
	function tender_list($searcharray=null){
		$this->db->select('*');
		$this->db->from('tbl_tender');
		if(!empty($searcharray)){
			foreach($searcharray as $key=>$value){
				$this->db->like($key,$value);
			}
		}
		$this->db->where('status!=','Trash');
		$query=$this->db->get()->result();
		return $query;
	}
	function supplier_list($cat_id){
		$this->db->select('*');
		$this->db->from('tbl_vendors');
		$this->db->where('status','Active');
		$this->db->where("vendor_category LIKE '%$cat_id%'");
		$query=$this->db->get()->result();
		$supplier_list = [];
		foreach ($query as $row) {
			$supplier['Name'] = $row->first_name. ' ' .$row->last_name;
			$supplier['ID'] = $row->vendor_id;
			$supplier['company_name'] = $row->company_name;
			$supplier_list[] = $supplier;
		}
		return $supplier_list;
	}
	function order_tender_list(){
		$this->db->select('*');
		$this->db->from('tbl_tender');
		$this->db->where('status!=','Trash');
		$this->db->order_by('FIELD (status,"Open","Pending","Awarded","Closed","Cancelled","Active")');
		$query=$this->db->get()->result();
		return $query;
	}

	function view_list($id){
		$this->db->select('*');
		$this->db->from('tbl_tender');
		$this->db->where('tender_id',$id);
		$this->db->where('status!=','Trash');
		$query['tender_list']=$this->db->get()->result();
		$query['tender_document']=$this->document_list($id);
		return $query;
	}
	function list_tender($id){
		$this->db->select('*');
		$this->db->from('tbl_tender');
		$this->db->where('tender_id',$id);
		$this->db->where('status!=','Trash');
		$query=$this->db->get()->result();
		return $query;
	}
	function check($id){
		$this->db->select('*');
		$this->db->from('tbl_tender');
		$this->db->where('tender_id',$id);
		$query=$this->db->get()->num_rows();
		if(!empty($query)){
			return "1";
		}
		else{
			return "0";
		}
	}
	function document_list($id){
		$this->db->select('*');
		$this->db->from('tbl_tender_documents');
		if(!empty($id)){
			$this->db->where('tender_id',$id);
		}
		$this->db->order_by('t_doc_type','Asc');
		$query=$this->db->get()->result();
		return $query;
	}

	function tenderAutoUpdate($getStatus) {
        $current_time = date("H:i");
        $current_date = date("Y-m-d");
		if ($getStatus == 'Pending') {
			$changeStatus = 'Open';
			$where = "DATE_FORMAT(start_date,'%Y-%m-%d') <='$current_date'";
		}
		else{
			$changeStatus = 'Closed';
			$where = "DATE_FORMAT(end_date,'%Y-%m-%d') <='$current_date'";
		}
		$this->db->select('*');
		$this->db->from('tbl_tender');
		$this->db->where("$where");
		$this->db->where("status ='$getStatus'");
		$query=$this->db->get();
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $rows) {
			   if ($rows->status != 'Cancelled') {
				   $tender_id = $rows->tender_id;
				   $start_date = $rows->start_date;
				   $start_time = date("H:i",strtotime($rows->start_time));
				   $end_date = $rows->end_date;
				   $end_time = date("H:i",strtotime($rows->end_time));
				   $status = array();
				  
				    if (strtolower($getStatus)=="pending") {
					    if ($start_date == $current_date) {
					   	  	if ($current_time >= $start_time) {
		                        $status[] = $this->tenderAutoStatusUpdate($tender_id,$changeStatus);
					      	}
					    } else {
		                    $status[] = $this->tenderAutoStatusUpdate($tender_id,$changeStatus);
					    }
					} 

					if (strtolower($getStatus)=="open")  {
					    if ($end_date == $current_date) {
							if ($current_time >= $end_time) {
								$status[] = $this->tenderAutoStatusUpdate($tender_id,$changeStatus);
							}
					    } else {
		                    $status[] = $this->tenderAutoStatusUpdate($tender_id,$changeStatus);
					    }
					}
					if (!empty($status))
						return true;
				}
			}
		}
	}
	function tenderAutoStatusUpdate($tenderID,$Status) {
		$data=array("status"=>$Status);
		$this->db->where('tender_id', $tenderID);
		$this->db->update('tbl_tender', $data);
		// echo $sql = $this->db->last_query();
		return $tenderID;
	}

	function tenderCancelUpdate($tenderid,$reason) {
		$data=array("status"=>"Cancelled","reason"=>$reason);
		$this->db->where('tender_id', $tenderid);
		$this->db->update('tbl_tender', $data);
		return $tenderid;

	}
}

<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Supplier_Model extends MY_Model {
    function __construct() {
        parent::__construct();
        $this->load->library('session');

        $this->load->model('Collective');

        $config = array (

            'mailtype'=>'html'

        );

        $this->load->library('email');
    }
    function get_categories($id) {
        $ArrCategory = [];
        if (!empty($id)) {
            $this->db->from('tbl_vendors');
            $this->db->where('vendor_id',$id);
            $query = $this->db->get();
            foreach ($query->result() as $row) {
                $vendor_category  = $row->vendor_category;
            }
            $ArrCategory = explode("##",$vendor_category);
        }
        return $ArrCategory;
    }

    function tender_list($tender_id=null) {
        if (!empty($_POST['equote'])) {
            $equote = $_POST['equote'];
            $searchrfq_no = $equote['rfq_no'];
            $searchtender_title = $equote['tender_title'];
            $searchpart_no = $equote['part_no'];
        } else {
            $searchrfq_no= $searchtender_title =  $searchpart_no="";
        }
        $vendor_id= $this->session->userdata("vendor_id");
        $today_date=date('Y-m-d');
        $current_time=date('H:i');
        $ArrTenderId= array();
        $qryTender= $this->db->select('*')
                          ->from('tbl_tender')
                          ->where("notification_setting !='1'")
                          ->where("suppliers LIKE '%$vendor_id%'")
                          ->where('status','Open')
                          ->get();
        $qry_rows = $qryTender->num_rows();
        if ($qry_rows > 0) {
            foreach ($qryTender->result() as $Tenderrow) {
                $ArrTenderId[]=$Tenderrow->tender_id;
            }
        }

        $allTender= $this->db->select('*')
                          ->from('tbl_tender')
                          ->where("notification_setting !='2'")
                          ->where('status','Open')
                          ->get();
        $all_rows = $allTender->num_rows();
        if ($all_rows > 0) {
            foreach ($allTender->result() as $Tenderrow) {
                $ArrTenderId[]=$Tenderrow->tender_id;
            }
        }

        $qrytenderInvite= $this->db->select('*')
                          ->from('tbl_tender_invitations')
                          ->where("vendor_id='$vendor_id'")
                          ->get();
        $inviterows = $qrytenderInvite->num_rows();
        if ($inviterows > 0) {
            foreach ($qrytenderInvite->result() as $Inviterow) {
                $ArrTenderId[]=$Inviterow->tender_id;
            }
        }
        //print_r($ArrTenderId); exit;OR (TIME_FORMAT( end_time,'%H:%i') >= '".$current_time."')
        if (count($ArrTenderId)>0) {
            $tenderids = implode("','",$ArrTenderId);
            $this->db->select("*");
            $this->db->from("tbl_tender");
            $this->db->where("(DATE_FORMAT(start_date,'%Y-%m-%d') <= '".$today_date."' AND DATE_FORMAT(end_date,'%Y-%m-%d') >= '".$today_date."')");
            //$this->db->where("(TIME_FORMAT( start_time,'%H:%i') <= '".$current_time."') ");
            $this->db->where("tender_id IN ('".$tenderids."')"); 
            $this->db->where('status','Open');
            $this->db->where("ref_no LIKE '%$searchrfq_no%'");
            $this->db->where("part_no LIKE '%$searchpart_no%'");
            $this->db->where("tender_title LIKE '%$searchtender_title%'");
            $query = $this->db->get();
            //echo $sql = $this->db->last_query();exit;
            $num_row = $query->num_rows();
            if ($num_row >0) {
                return $query->result();
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    function view_list($tender_id){
        //echo $tender_id;
        $this->db->select("*");
        $this->db->from("tbl_tender");
        if(!empty($tender_id)){
            $this->db->where('tender_id',$tender_id); 
        }
        $this->db->where('status','Open');
        $query['tender_list']=$this->db->get()->result();
        $query['tender_document']=$this->document_list($query['tender_list'][0]->tender_id);
        return $query;
    }
    
    function document_list($id){
        $this->db->select('*');
        $this->db->from('tbl_tender_documents');
        if(!empty($id)){
            $this->db->where('tender_id',$id);
        }
        $this->db->order_by('t_doc_type','Asc');
        $query=$this->db->get()->result();
        return $query;
    }
    function add($datas){
        $this->db->insert('tbl_tender_applications', $datas);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }
    function add_application($datas){
        $this->db->insert('tbl_apply_tender_audit', $datas);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }
    function add_tender_audit($datas){
        $this->db->insert('tbl_tender_audit', $datas);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }
   
    function update_invitation($invitation_id,$status){
        $this->db->where('id', $invitation_id);
        $this->db->update('tbl_tender_invitations', $status);
        return true;
    }
    function update_application($application_id,$status){
        $this->db->where('id', $application_id);
        $this->db->update('tbl_tender_applications', $status);
        return true;
    }
    function check($tender_id,$vendor_id){
        $this->db->select("*");
        $this->db->from("tbl_tender_applications");
        $this->db->where("tender_id",$tender_id);
        $this->db->where("vendor_id",$vendor_id);
        $this->db->where('status','Active');
        $query = $this->db->get()->result();
        if(!empty($query)){
            return $query[0]->id;
        }
        else{
            return '';
        }
    }
    function view_tenders_list($id=null){
        $this->db->select("*,app.price as prices,app.id as invitation_id");
        $this->db->from("tbl_tender_applications as app");
        $this->db->join('tbl_tender as tender','app.tender_id=tender.tender_id','inner');
        //$this->db->join('tbl_tender_invitations as invitation','tender.tender_id=invitation.tender_id','inner');
        if(!empty($id)){
            $this->db->where('app.id',$id);
        }
        $this->db->where('tender.status', 'Open');
        $this->db->where('app.status', 'Active');
        $query = $this->db->get();
        $num_rows = $query->num_rows();
        if($num_rows >0) {
            return $query->result();
        }else {
            return false;
        }
    }
    function applied_tenders_list($id=null) {
        $this->db->select("*,app.price as prices,app.id as invitation_id");
        $this->db->from("tbl_tender_applications as app");
        $this->db->join('tbl_tender as tender','app.tender_id=tender.tender_id','inner');
        //$this->db->join('tbl_tender_invitations as invitation','tender.tender_id=invitation.tender_id','inner');
        if(!empty($id)){
            $this->db->where('app.tender_id',$id);
        }
        $this->db->where('tender.status !=', 'Trash');
        $this->db->where('app.status', 'Active');
        $query = $this->db->get();
        $num_rows = $query->num_rows();
        if($num_rows >0) {
            return $query->result();
        }else {
            return false;
        }
    }
    
    function applied_vendortotender_list($vendorId) {
        if (!empty($_POST['equotes'])) {
            $equote = $_POST['equotes'];
            $searchrfq_no = $equote['rfq_no'];
            $searchtender_title = $equote['tender_title'];
            $searchpart_no = $equote['part_no'];
        } else {
            $searchrfq_no= $searchtender_title =  $searchpart_no="";
        }
        $this->db->select("*,app.price as prices,app.id as invitation_id,app.tender_id as applytenderid");
        $this->db->from("tbl_tender_applications as app");
        $this->db->join('tbl_tender as tender','app.tender_id=tender.tender_id','inner');
        $this->db->where('app.vendor_id',$vendorId);
        $this->db->where("tender.ref_no LIKE '%$searchrfq_no%'");
        $this->db->where("tender.part_no LIKE '%$searchpart_no%'");
        $this->db->where("tender.tender_title LIKE '%$searchtender_title%'");
        $this->db->where('tender.status !=', 'Awarded');
        // $this->db->where('app.status','Active');
        $query = $this->db->get();
        $num_rows = $query->num_rows();
        if($num_rows >0) {
            return $query->result();
        } else {
            return false;
        }
    }

    function SupplierLogin() {
        $email = $this->input->post("email");
        $password = $this->input->post("password");
        $passwords=$this->Collective->encode($password);
        if(!empty($email) && !empty($passwords)) {
            $stmt = $this->db->query("select * from tbl_vendors where email='" . $email . "' and password='" .$passwords."'");
            $rowcount = $stmt->num_rows();
            if ($rowcount == 1) {
                $data="";
                foreach ($stmt->result() as $row) {
                    $data = array(
                        'vendor_id'  =>$row->vendor_id,
                        'vendor_name' => $row->company_name,
                        'vendor_email'=>  $row->email,
                        'vendor_type'=> "Supplier",
                        'status'=>$row->status,
                    );
                    $this->session->set_userdata($data);
                    $vendorid=$row->vendor_id;
                    if(!empty($vendorid)) {
                        $ip=$this->input->ip_address();
                        $data_log = array(
                            'vendor_id' => $vendorid,
                            'logged_in' => date("Y-m-d H:i:s"),
                            'ip_address'=> $ip,
                        );
                        $this->db->insert('tbl_vendor_login_audit',$data_log);
                        $vendor_logid=$this->db->insert_id();
                        $data_log = array("vendorlogid"=>$vendor_logid);
                    }
                    $this->session->set_userdata($data_log);
                    return true;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    function logout($vendorlogid) {
        $data=array("logged_out"=>date("Y-m-d H:i:s"));
        $this->db->where('veendor_login_id',$vendorlogid);
        $this->db->update('tbl_vendor_login_audit', $data);
        return true;
    }
    function ForgotPassword() {
        $email = $this->input->post("email");
        if (!empty($email)) {
            $stmt = $this->db->query("select * from tbl_vendors where email='". $email ."'");
            $rowcount = $stmt->num_rows();
            if ($rowcount > 0) {
                $data="";
                foreach ($stmt->result() as $row) {
                    $vendor_id = $row->vendor_id;
                    $vendorid  =  $this->Collective->encode($vendor_id);
                    $first_name = $row->first_name;
                    $last_name = $row->last_name;
                    $email = $row->email;
                    $vendorid=$this->Collective->encode($vendor_id);
                    $HttpPath=base_url();
                    $EmailMessage = '
                        <html>
                            <style>
                                @media screen and (min-width: 320px) {
                                    .container1 {
                                        width: 100%!important;
                                    }
                                }  
                            </style>
                        <table class="container1" width="100%" cellpadding="0" cellspacing="0"  width="600px" style="font-family: verdana;font-size:13px;max-width: 600px;">
                            <tr>
                            <td>Hello '.$first_name.',</td>
                            </tr>
                            <tr style="height:10px"><td></td></tr>
                            <tr>
                            <td>As requested, your E-Quote Management Reset details are below:</td>
                            </tr>
                            <tr style="height:10px"><td></td></tr>
                            <tr>
                                <td>Full Name : '.$first_name." ".$last_name.'</td>
                            </tr>
                            <tr style="height:2px"><td></td></tr>
                            <tr>
                                <td>User Email : '.$email.'</td>
                            </tr>
                            <tr style="height:2px"><td></td></tr>
                            <tr>
                                <td><a href = "'.$HttpPath.'supplier/login/reset_password/'.$vendorid.'">Click Here To Reset Password</a></td>
                            </tr>
                            <tr style="height:10px"><td></td></tr>
                            <tr>
                            <td>Thank you,<br>E-Quote Management</td>
                            </tr>
                        </table>
                        </body>
                        </html>
                        ';
                    $this->email->set_mailtype("html");
                    $this->email->from('karuna.shenll@gmail.com', 'E-Quote');
                    $this->email->to($email);
                    $this->email->subject('Your E-Quote Management Login Details');
                    $this->email->message($EmailMessage);
                    if ($this->email->send()) {
                        return "Success";
                    } else {
                        //echo $this->email->print_debugger();
                        return "Notsend";
                    }

                }
            } else {
                return "Invaild";
            }
        }
    }
    function ResetPassword ($vendorId) {
        $password = $this->input->post("password");
        if (!empty($vendorId) && !empty($password)) {
            $vendor_id=$this->Collective->decode($vendorId);
            $newpassword=$this->Collective->encode($password);
            $data=array(
                "password"=>$newpassword,
                "modified_date"=>date("Y-m-d H:i:s"),
            );
            $this->db->where('vendor_id',$vendor_id);
            $this->db->update('tbl_vendors', $data);
            return true;
        } else {
            return false;
        }
    }
    function cronVendorMail() {
        $date_cur_minus=date('Y-m-d', strtotime('-5 days'));
        if (!empty($date_cur_minus)) {
            $stmt = $this->db->query("select * from tbl_vendors where DATE_FORMAT(sent_date,'%Y-%m-%d') ='".$date_cur_minus."' and (supplier_doc ='' OR supplier_doc IS NULL OR supplier_doc !='') and status='Pending'");
            $rowcount = $stmt->num_rows();
            if ($rowcount > 0) {
                $data="";
                foreach ($stmt->result() as $row) {
                    $vendor_id = $row->vendor_id;
                    $vendorid  =  $this->Collective->encode($vendor_id);
                    $last_name = $row->last_name;
                    $first_name = $row->first_name;
                    $email = $row->email;
                    $vendorid=$this->Collective->encode($vendor_id);
                    $HttpPath=base_url();
                    $this->email->from('karuna.shenll@gmail.com', 'E-Quote Notification');
                    $this->email->to($email);
                    $this->email->subject('Registration for Notification E Quote');
                    $this->email->set_mailtype("html");
                    $this->email->message(
                        '<div>
                            <p>Hi '.$first_name.',</p><br>
                            <p>Please note, the following registration forms are incomplete. You can login Please click <a href="'.base_url().'supplier/login">here</a> to complete them. You registered with the following email: '.$email.' Thankyou.</p>
                            <p>Below are the forms you have yet to complete. Please complete registration no later than registration with in 5 days.</p>
                            <p>Supplier upload document not completed</p><br><br>
                            <p>Best Regards,<br>Support Team<br>E-Quote Management<br></p>
                        </div>'
                    );
                    if ($this->email->send()) {
                        //return "Email sent successfully";
                    } else {
                        //echo $this->email->print_debugger();
                        //return "Email not sent";
                    }
                    $data_vendor=array("sent_date"=>date("Y-m-d"));
                    $this->db->where('vendor_id',$vendor_id);
                    $this->db->update('tbl_vendors', $data_vendor);
                }
                return true;
            } else{
                return "Email not sent";
            }

        }
    }
}
<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Bidding_Model extends MY_Model {
    function __construct() {
        parent::__construct();
    }

    function getbiddingList() {

        if (!empty($_POST['bidding'])) {
            $bidding = $_POST['bidding'];
            $condition = $searchenddate = $searchstartdate="";
            if (!empty($bidding['start_date']) && empty($bidding['end_date'])) {
                $start_date       = str_replace("/", "-",$bidding['start_date']);
                $searchstartdate = date("Y-m-d",strtotime($start_date));
                $condition.=" AND tender.start_date ='".$searchstartdate."'";
            }
            if (empty($bidding['start_date']) && !empty($bidding['end_date'])) {
                $end_date       = str_replace("/", "-",$bidding['end_date']);
                $searchenddate = date("Y-m-d",strtotime($end_date));
                $condition.=" AND tender.end_date ='".$searchenddate."'";
            }
            if (!empty($bidding['start_date']) && !empty($bidding['end_date'])) {
                $start_date       = str_replace("/", "-",$bidding['start_date']);
                $end_date       = str_replace("/", "-",$bidding['end_date']);
                $searchstartdate = date("Y-m-d",strtotime($start_date));
                $searchenddate  = date("Y-m-d",strtotime($end_date));
               $condition.=" AND tender.start_date >= '".$searchstartdate. "' and tender.end_date <= '".$searchenddate. "'";
            }
            $searchrfq_no = $bidding['rfq_no'];
            $searchtender_title = $bidding['tender_title'];
            $searchpart_name = $bidding['part_name'];
            $searchpart_no = $bidding['part_no'];
        } else {
            $condition = $searchstartdate= $searchenddate=$searchrfq_no= $searchtender_title = $searchpart_name = $searchpart_no="";
        }
        $currentdate=date("Y-m-d");
        $sessionVendorid=$this->session->userdata('vendor_id');
        $query = $this->db->select('applytender.tender_id, applytender.vendor_id, applytender.price as applyprice, tender.tender_title,tender.tender_title,tender.part_name,tender.part_no,tender.ref_no,tender.base_price,tender.part_price,tender.start_date,tender.end_date,tender.price_edit')
            ->from('tbl_tender_applications as applytender')
            ->join('tbl_tender as tender', 'applytender.tender_id = tender.tender_id', 'INNER JOIN')
            ->where("tender.ref_no LIKE '%$searchrfq_no%'")
            ->where("tender.tender_title LIKE '%$searchtender_title%'")
            ->where("tender.part_no LIKE '%$searchpart_no%'")
            ->where("tender.part_name LIKE '%$searchpart_name%'".$condition)
            ->where('applytender.vendor_id', $sessionVendorid)
            ->where("tender.status='Open'")
            ->get();
        if ($query->num_rows() > 0)
        {
            $data="";

            foreach ($query->result() as $row) {
                $tender_id = $row->tender_id;
                $vendor_id = $row->vendor_id;
                $applyprice = $row->applyprice;
                $tender_title = $row->tender_title;
                $part_name = $row->part_name;
                $ref_no = $row->ref_no;
                $part_no = $row->part_no;
                $base_price = $row->base_price;
                $part_price = $row->part_price;
                $start_date = $row->start_date;
                $end_date = $row->end_date;
                $price_edit = $row->price_edit;
                $currentdate=date("Y-m-d");
                //echo $start_date."==>".$end_date;echo"<br>";
                if (strtolower($price_edit) =="no") {
                   $viewbtn="hide";
                } else {
                    /* case
                        startdate tomorrow 
                        enddate today or previous days
                    */
                    if (($currentdate < $start_date) && ($currentdate < $end_date)) {
                        $viewbtn="hide";                        
                    } elseif($currentdate > $end_date) {
                        $viewbtn="hide";
                    } else { 
                        $stmtaward = $this->db->query("select * from tbl_awarded_tenders where tender_id='" .$tender_id. "'");
                        $countAward= $stmtaward->num_rows();
                        if ($countAward>0) {
                            $awardData = $stmtaward->row();
                            $viewbtn="hide";
                        } else {
                            $viewbtn="show";
                        }
                    }
                }
                
                $stmtcurrent = $this->db->query("select MIN(price) as currentPrice from tbl_tender_applications where tender_id='" .$tender_id. "'");
                $currentCount= $stmtcurrent->num_rows();
                $currentPriceData = $stmtcurrent->row();
                $currentPrice=$currentPriceData->currentPrice;
                
                $data[]= array(
                    'tender_id' =>$row->tender_id,
                    'vendor_id' =>$row->vendor_id,
                    'applyprice'=> $row->applyprice,
                    'tender_title'=> $row->tender_title,
                    'part_name' => $row->part_name,
                    'ref_no'  =>$row->ref_no,
                    'part_no'  =>$row->part_no,
                    'base_price' => $row->base_price,
                    'part_price' => $row->part_price,
                    'start_date' => $row->start_date,
                    'end_date'  => $row->end_date,
                    'currentPrice'=>$currentPrice,
                    'viewbtn' =>$viewbtn,
                );
                
            }
            return $data;
        }
    }

   // function applied_tenders_list($user_id) {
   //      $this->db->select("*,app.price as prices,app.id as invitation_id");
   //      $this->db->from("tbl_tender_applications as app");
   //      $this->db->join('tbl_tender as tender','app.tender_id=tender.tender_id','inner');
   //      $this->db->where('tender.status', 'Open');
   //      $this->db->where('app.status', 'Active');
   //      $this->db->where('app.vendor_id', "$user_id");
   //      $query = $this->db->get();
   //      $num_rows = $query->num_rows();
   //      if($num_rows >0) {
   //         return $query->result();
   //      }else {
   //         return false;
   //      }
   //  }
    function change_bid_tenders($tenderId,$vendorId) {
        $query = $this->db->select('*')
        ->from('tbl_tender')
        ->where("tender_id= '$tenderId'")
        ->get();
        if ($query->num_rows() > 0) {
            $data="";
            foreach ($query->result() as $row) {
                $stmtcurrent = $this->db->query("select * from tbl_tender_applications where vendor_id='" .$vendorId. "' and tender_id='".$tenderId."'");
                $applicationsCount= $stmtcurrent->num_rows();
                if ($applicationsCount>0) {
                    $applicationsPriceData = $stmtcurrent->row();
                    $vendor_id=$applicationsPriceData->vendor_id;
                    $tender_id=$applicationsPriceData->tender_id;
                    $applyprice=$applicationsPriceData->price;
                } else {
                   $applyprice="";
                   $vendor_id =$vendorId;
                }
                $data= array(
                    'tender_id' =>$row->tender_id,
                    'part_no' =>$row->part_no,
                    'price_edit' =>$row->price_edit,
                    'part_name'=> $row->part_name,
                    'tender_title'=> $row->tender_title,
                    'part_name' => $row->part_name,
                    'part_price'  =>$row->part_price,
                    'ref_no' => $row->ref_no,
                    'drawing_doc' => $row->drawing_doc,
                    'delivery_duration' => $row->delivery_duration,
                    'start_date' => $row->start_date,
                    'end_date'  => $row->end_date,
                    'start_time' => $row->start_time,
                    'end_time'  => $row->end_time,
                    'base_price'=>$row->base_price,
                    'quantity' =>$row->quantity,
                    'description' =>$row->tender_detail,
                    'applyprice' =>$applyprice,
                    'vendor_id'=>$vendor_id,
                );
            }
            $docquery = $this->db->select('*')
                    ->from('tbl_tender_documents')
                    ->where("tender_id ='$tenderId'")
                    ->where("status ='Active'")
                    ->get();
            $rowcnt = $docquery->num_rows();
            if ($rowcnt > 0) {
                foreach ($docquery->result() as $docrow) {
                    $doc_type = $docrow->t_doc_type;
                    if (strtolower($doc_type) == "upload_terms") {
                        $data["upload_terms"][] = array(
                            'doc_name' => $docrow->t_doc_name,
                        );
                    }

                    if (strtolower($doc_type) == "upload_helpdoc") {
                        $data["upload_helpdoc"][] = array(
                            'doc_name' => $docrow->t_doc_name,
                        );
                    }

                    if (strtolower($doc_type) == "upload_tenderdetail") {
                        $data["upload_tenderdetail"][] = array(
                            'doc_name' => $docrow->t_doc_name,
                        );
                    }

                    if (strtolower($doc_type) == "upload_tenderdoc") {
                        $data["upload_tenderdoc"][] = array(
                            'doc_name' => $docrow->t_doc_name,
                        );
                    }

                    if (strtolower($doc_type) == "tender_docone") {
                        $data["tender_docone"][] = array(
                            'doc_name' => $docrow->t_doc_name,
                            
                        );
                    }

                    if (strtolower($doc_type) == "tender_doctwo") {
                        $data["tender_doctwo"][] = array(
                            'doc_name' => $docrow->t_doc_name,
                            
                        );
                    }
                    
                }
            }
        }
        return $data;
    }
    function upadtebidding($vendor_id,$tender_id) {
        $your_price=$this->input->post("your_price");
        if (!empty($your_price)) {
            $stmtchecapplytender = $this->db->query("select * from tbl_tender_applications where tender_id='".$tender_id."' and vendor_id='".$vendor_id."'");
            $applicationCount= $stmtchecapplytender->num_rows();
            $datetimedb=date("Y-m-d H:i:s");
            if ($applicationCount>0) {
                $stmtcurrent = $this->db->query("select MIN(price) as currentPrice from tbl_tender_applications where tender_id='".$tender_id."'");
                $currentCount= $stmtcurrent->num_rows();
                $currentPriceData = $stmtcurrent->row();
                $currentPrice=$currentPriceData->currentPrice;
               
                
                if ($currentPrice > $your_price) {
                    $data_vendor=array("modified_date"=>$datetimedb,"price"=>$your_price,"created_by"=>$vendor_id,"modified_by"=>$vendor_id);
                    $this->db->where('vendor_id',$vendor_id);
                    $this->db->where('tender_id',$tender_id);
                    $this->db->update('tbl_tender_applications', $data_vendor);

                    $data_apply = array (
                        "apply_id"=>$tender_id, 
                        "tender_id"=>$tender_id,
                        "vendor_id"=>$vendor_id,
                        "created_on"=>$datetimedb,
                        "created_by"=>$vendor_id,
                        "price"=>$your_price,
                    );
                    $this->db->insert('tbl_apply_tender_audit',$data_apply);
                    return "success";
                } else {
                    $data_vendor=array("modified_date"=>$datetimedb,"price"=>$your_price,"created_by"=>$vendor_id,"modified_by"=>$vendor_id);
                    $this->db->where('vendor_id',$vendor_id);
                    $this->db->where('tender_id',$tender_id);
                    $this->db->update('tbl_tender_applications', $data_vendor);

                    $data_apply = array (
                        "apply_id"=>$tender_id, 
                        "tender_id"=>$tender_id,
                        "vendor_id"=>$vendor_id,
                        "created_on"=>$datetimedb,
                        "created_by"=>$vendor_id,
                        "price"=>$your_price,
                    );
                    $this->db->insert('tbl_apply_tender_audit',$data_apply);
                    return "success";
                }
            } else {
                     
                    $data_vendor= array (
                        "tender_id"=>$tender_id,
                        "vendor_id"=>$vendor_id,
                        "price"=>$your_price,
                        "status"=>"Active",
                        "created_by"=>$vendor_id,
                        "modified_by"=>$vendor_id,
                        "created_date"=>$datetimedb,
                        "modified_date"=>$datetimedb,
                    );
                    $this->db->insert('tbl_tender_applications',$data_vendor);

                    $data_apply = array (
                        "apply_id"=>$tender_id, 
                        "tender_id"=>$tender_id,
                        "vendor_id"=>$vendor_id,
                        "created_on"=>$datetimedb,
                        "created_by"=>$vendor_id,
                        "price"=>$your_price,
                    );
                    $this->db->insert('tbl_apply_tender_audit',$data_apply);
                    return "success";

            }
        }
    }
}
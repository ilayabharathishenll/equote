<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Applytender_Model extends MY_Model {

	function __construct() {
		parent::__construct();
		$this->load->library('session');
	}
	function appliedtender_list() {
		if (!empty($_POST['applied'])) {
			$applied = $_POST['applied'];
			$condition = $searchenddate = $searchstartdate="";
			if (!empty($applied['start_date']) && empty($applied['end_date'])) {
				$start_date       = str_replace("/", "-", $applied['start_date']);
				$searchstartdate = date("Y-m-d",strtotime($start_date));
				$condition.=" AND start_date ='".$searchstartdate."'";
			}
			if (empty($applied['start_date']) && !empty($applied['end_date'])) {
				$end_date       = str_replace("/", "-", $applied['end_date']);
				$searchenddate = date("Y-m-d",strtotime($end_date));
				$condition.=" AND end_date ='".$searchenddate."'";
			}
			if (!empty($applied['start_date']) && !empty($applied['end_date'])) {
				$start_date       = str_replace("/", "-", $applied['start_date']);
				$end_date       = str_replace("/", "-", $applied['end_date']);

				$searchstartdate = date("Y-m-d",strtotime($start_date));
				$searchenddate  = date("Y-m-d",strtotime($end_date));
				$condition.=" AND start_date >= '".$searchstartdate. "' and end_date <= '".$searchenddate. "'";
			}
			$searchrfq_no = $applied['rfq_no'];
			$searchtender_title = $applied['tender_title'];
			$searchpart_name = $applied['part_name'];
			$searchpart_no = $applied['part_no'];
		} else {
			$condition = $searchstartdate= $searchenddate=$searchrfq_no= $searchtender_title = $searchpart_name = $searchpart_no="";
		}
		$currentdate=date("Y-m-d");
		// $query = $this->db->select('*')
		// 	->from('tbl_tender')
		// 	->where("ref_no LIKE '%$searchrfq_no%'")
		// 	->where("tender_title LIKE '%$searchtender_title%'")
		// 	->where("part_no LIKE '%$searchpart_no%'")
		// 	->where("part_name LIKE '%$searchpart_name%'".$condition)
		// 	->where("start_date <= '$currentdate'")
		// 	->where("status <> 'Awarded'")
		// 	->where("status <> 'Cancelled'")
		// 	->order_by('created_on', 'desc')
		// 	->get();
		$query = $this->db->select('*')
			->from('tbl_tender')
			->where("ref_no LIKE '%$searchrfq_no%'")
			->where("tender_title LIKE '%$searchtender_title%'")
			->where("part_no LIKE '%$searchpart_no%'")
			->where("part_name LIKE '%$searchpart_name%'".$condition)
			->where("start_date <= '$currentdate'")
			->where("status <> 'Awarded'")
			->where("status <> 'Cancelled'")
			->order_by('created_on', 'desc')
			->get();
		if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
            	$tender_id = $row->tender_id;
            	$stmtcurrent = $this->db->query("select count(id) as currentcount from tbl_tender_applications where tender_id='" .$tender_id. "'");
                $currentapplyCount= $stmtcurrent->num_rows();
                $currentCountData = $stmtcurrent->row();
                $currentCount=$currentCountData->currentcount;
            	$data[]= array(
					'tender_id' =>$row->tender_id,
					'tender_title'=> $row->tender_title,
					'part_name' => $row->part_name,
					'ref_no'  =>$row->ref_no,
					'part_no'  =>$row->part_no,
					'applycount'  =>$currentCount,
				);
            }
		}
		//print_r($data);
		return $data;
	}
	
	function appliedvendor_list($tenderID) {
		if (!empty($_POST['appliedvendor'])) {
			$appliedvendor = $_POST['appliedvendor'];
			$searchrfq_no = $appliedvendor['rfq_no'];
			$search_code = $appliedvendor['vendor_code'];
			$search_name = $appliedvendor['vendor_name'];
			$search_partno = $appliedvendor['part_no'];
		} else {
			$searchrfq_no = $search_code = $search_name = $search_partno ="" ;
		}
		$query = $this->db->select('applytender.tender_id, applytender.vendor_id, applytender.price as applyprice, tender.tender_title,tender.tender_title,tender.part_name,tender.ref_no,tender.base_price,tender.part_price,tender.start_date,tender.end_date,tender.start_time,tender.end_time,tender.price_edit,vendor.vendor_code,vendor.first_name,applytender.id as application_id,vendor.rating,tender.part_no')
			->from('tbl_tender as tender')
			->join('tbl_tender_applications as applytender', 'tender.tender_id = applytender.tender_id', 'JOIN')
			->join('tbl_vendors as vendor', 'vendor.vendor_id = applytender.vendor_id', 'JOIN')
			->where("tender.ref_no LIKE '%$searchrfq_no%'")
			->where("tender.part_no LIKE '%$search_partno%'")
			->where("vendor.vendor_code LIKE '%$search_code%'")
			->where("vendor.first_name LIKE '%$search_name%'")
			->where('applytender.tender_id',$tenderID)
			->order_by("applytender.price","ASC")
			->get();
		if ($query->num_rows() > 0) {
			$data=$bidding="";
			foreach ($query->result() as $row) {
				$count_query = $this->db->select('price, created_on')
					->from('tbl_apply_tender_audit')
					//->limit(5)
					->order_by("created_on","desc")
					->where("tender_id = '$row->tender_id'")
					->where("vendor_id = '$row->vendor_id'")
					->get();
				if ($count_query->num_rows() > 0) {
					foreach ($count_query->result() as $count_row) {
						$bidding[] = array(
							'price' => $count_row->price,
							'date' => $count_row->created_on,
						);
					}
				}

				$currentdate=date("Y-m-d");
				$data[]= array(
					'tender_id' =>$row->tender_id,
					'apply_audit' =>$row->vendor_id,
					'applyprice'=> $row->applyprice,
					'tender_title'=> $row->tender_title,
					'part_name' => $row->part_name,
					'ref_no'  =>$row->ref_no,
					'part_no'  =>$row->part_no,
					'rating'  =>$row->rating,
					'base_price' => $row->base_price,
					'part_price' => $row->part_price,
					'start_date' => $row->start_date,
					'end_date'  => $row->end_date,
					'start_time' => $row->start_time,
					'end_time'  => $row->end_time,
					'vendor_code' =>$row->vendor_code,
					'vendor_name' =>$row->first_name,
					'application_id' =>$row->application_id,
					'bidding_count' =>$count_query->num_rows(),
					'bidding_log' => $bidding
				);
				$bidding = ''; 
			}
			return $data;
		}
	}
	function awardsAdded($tenderID,$application_id) {
		$query = $this->db->select('*')
			->from('tbl_tender_applications')
			->where('tender_id',$tenderID)
			->where('id',$application_id)
			->get();
		if ($query->num_rows() > 0) {
			$data=$price="";
			foreach ($query->result() as $row) {
				$price=$row->price;
				$data = array(
					'tender_id' =>$row->tender_id,
					'vendor_id' =>$row->vendor_id,
					'awarded_price'=> $row->price,
					'awarded_by'=> $this->session->userdata('admin_id'),
					'created_on' => date("Y-m-d H:i:s"),
					'status'  =>"Awarded",
					'details'=>"Null",
					'modified_on' => date("Y-m-d H:i:s"),
				);
				$this->db->insert('tbl_awarded_tenders',$data);
			}
			$data=array("price"=>$price,"modified_on"=>date("Y-m-d H:i:s"),"status"=>"Awarded");
			$this->db->where('tender_id',$tenderID);
			$this->db->update('tbl_tender', $data);
			$data=array("modified_date"=>date("Y-m-d H:i:s"),"status"=>"Awarded");
			$this->db->where('id',$application_id);
			$this->db->update('tbl_tender_applications', $data);
			$user_id = $this->session->userdata('admin_id');
			$admin_name = $this->session->userdata('admin_name');
			$dataInsert=array("tender_id"=>$tenderID,"admin_id"=>$user_id,"created_by"=>$admin_name,"status"=>"Awarded","created_on"=>date('Y-m-d H:i:s'));
			$this->db->insert('tbl_tender_activity',$dataInsert);
			return true;
		} else {
			return flase;
		}
	}
}